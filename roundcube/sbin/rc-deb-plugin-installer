#!/usr/bin/env bash
################################################################################
# Copyright 2023 Marco Emilio Poleggi
#
# rc-deb-plugin-installer is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rc-deb-plugin-installer is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rc-deb-plugin-installer.  If not, see <http://www.gnu.org/licenses/>.
################################################################################
# More info in `usage_msg()` below.
################################################################################
# meta
__version=0.9.4
__dependencies=(
    [dpkg_query]='>=1.21.22'
    [git]='>=2.39.2'
    [tar]='>=1.34'
    [p7zip]='>=16.02'
)
################################################################################
peval_lib_path=/usr/local/share/bash-utilz/lib/peval.sh
source $peval_lib_path || {
    echo "[fatal] unable to load shell library '${peval_lib_path}'" >&2
    exit -1
}
# think of these as a sort of "import as" ;-)
for aname in log_debug log_info log_warn log_error log_fatal; do
    alias ${aname}=pvl__${aname}
done
alias peval=pvl__eval

myself=$(basename $0)
my_sandbox=
status=0

################################################################################

# (( $EUID == 0 )) || \
#     log_fatal 'Please run as root'

################################################################################

usage_msg=$(cat <<EOF
                                                     ${myself} v${__version}

  $myself [OPTIONS] ACTION PLUGIN RELEASE_URL|VERSION

rc-deb-plugin-installer is an out-of-tree Roundcube's plugin installer for
Debian.  It downloads and installs a plugin ~tarball file in a distro's RC
instance.

Arguments
---------

  ACTION    (str) [install|remove].

    'install' is idempotent => alias 'reinstall'. 3rd positional arg must be a
        RELEASE_URL string. Supported archive formats: tarballs (.tar.*),
        zipballs (.zip).

    'remove' won't uninstall a plugin provided by distro's packages, unless
        you '--force' the run. Configuration files and downloaded packages are
        left untouched. 3rd positional arg must be a VERSION string.

  PLUGIN    (str) the plugin's _release_ name. This may differ from the
    'PLUGIN_REPO' mentioned in RELEASE_URL but must match the donwload file and
    RC's expected plugin's name.

  RELEASE_URL | VERSION  (str)

    RELEASE_URL is something like "http[s]://REPOSITORY/USER/PLUGIN_REPO/RELEASE/PATH...",
        e.g.  "https://github.com/mstilkerich/rcmcarddav/..." -- see section
        Examples. Notice that 'PLUGIN_REPO' may differ from arg 'PLUGIN'. The
        supported 'RELEASE/PATH' forms are:

            (GitHub release) releases/download/vVERSION/PLUGIN.FEXT
            (GitHub tag)     archive/refs/tags/vVERSION.FEXT

        The supported release file extensions (FEXT) are:

            tar.gz, tgz

    VERSION is expected to be in the form 'X.Y.Z' or similar.


Examples
--------

    $myself install carddav https://github.com/mstilkerich/rcmcarddav/releases/download/v5.1.0/carddav-v5.1.0.tar.gz

    $myself remove carddav 5.1.0

    $myself install twofactor_gauthenticator https://github.com/camilord/twofactor_gauthenticator/archive/refs/heads/master.zip


Testing
-------

A fake-root sandbox will be created with:

    PVL_SYS_ROOT=\${rc_sandbox} $myself ...

Since 'remove' uses  Debian's 'dpkg-query' to probe distro's packages, testing in the
bsence of 'dpkg' other package managers can be _simulated_ by forcibly make
the probe either fail

    PVL_DPKG_QUERY=true $myself ...

or succeed

    PVL_DPKG_QUERY=false $myself


Bugs
----

* Only github repos and tarball head/tag/release are supported.
* Pure git repo URLs are not supported.
* 'dpkg-query' is used to probe system pcks before removing.

To-do
-----

* Support more Git/other repo platforms: GitLab, etc.

Options
-------
EOF
)

# my defaults
force=$PVL_FALSE
download_dir="/srv/pkgs/roundcube/plugins"
install_dir="/opt/roundcube/plugins"
etc_plugin_dir="/etc/roundcube/plugins"
usr_plugin_dir="/usr/share/roundcube/plugins"
var_plugin_dir="/var/lib/roundcube/plugins"
user=root
group=root
php_config_name='config.inc.php'

dpkg_query=${PVL_DPKG_QUERY:-'dpkg-query'}

# link common (archive) file extension to the correct downloader and
# decompressor functions
declare -A downloaders=(
    [tgz]='curl_dl'
    [tar.Z]='curl_dl'
    [tar.gz]='curl_dl'
    [tar.bz]='curl_dl'
    [zip]='curl_dl'
    [git]='git_dl'      # not a real file extension, usually git URLs
                        # terminate with '.git'
)
declare -A decompressors=(
    [tgz]='untar -z'
    [tar.Z]='untar -Z'
    [tar.gz]='untar -z'
    [tar.bz]='untar -j'
    [zip]='un7z'
    [git]=':'           # no-op ;-)
)

# let's introduce ourselves
declare -A caller_data=(
    [name]=$myself
    [path]=$0
    [usage_msg]=${usage_msg}
    [short_opts]='D:E:fi:u:v:'
    [long_opts]='download-dir:,etc-plugin-dir:,force,install-dir:,usr-plugin-dir:,var-plugin-dir:'
)

# $caller_env is passed as nameref to `pvl__init()`
declare -A caller_env=(
    [cl_opts]=''
    [lib_env]=''
)
# Namerefs (!) of directories that shall be sandboxed when PVL_SYS_ROOT is
# overridden. After init the original vars will hold the modified paths.
declare -a caller_sandbox=(
    download_dir
    install_dir
    etc_plugin_dir
    usr_plugin_dir
    var_plugin_dir
)

pvl__init caller_env caller_data || {
    pvl__usage
    exit $PVL_OS_ERR
}
[[ ${caller_env[lib_env]} ]] && eval export ${caller_env[lib_env]}
[[ ${caller_env[cl_opts]} ]] && eval set -- ${caller_env[cl_opts]}

if my_sandbox=$(pvl__is_sandboxed); then
    log_info "Running in sandbox '${my_sandbox}'"
fi


# Our mandatory arguments will be instantiated at run time
declare -a argsn=(
    action
    plugin
    releaseurl_or_version   # yes, two for the price of one ;-)
)

while true; do
    case "$1" in
        -D|--download-dir)      #@USAGE: str (${download_dir}). Where to download the plugin pkg
            download_dir=$2
            shift 2
            ;;
        -E|--etc-plugin-dir)    #@USAGE: str (${etc_plugin_dir}). RC's etc plugin dir
            etc_plugin_dir=$2
            shift 2
            ;;
        -f|--force)             #@USAGE: bool (${force}). Force dangerous actions, like removals
            force=$PVL_TRUE
            shift
            ;;
        -i|--install-dir)       #@USAGE: str (${install_dir}). Where to install the plugin
            install_dir=$2
            shift 2
            ;;
        -u|--usr-plugin-dir)    #@USAGE: str (${usr_plugin_dir}). RC's usr plugin dir
            usr_plugin_dir=$2
            shift 2
            ;;
        -v|--var-plugin-dir)    #@USAGE: str (${var_plugin_dir}). RC's var plugin dir
            var_plugin_dir=$2
            shift 2
            ;;
        --)
            pvl__ingest_posargs argsn $@ || {
                log_error "Can't ingest my positional arguments"
                pvl__usage
                exit $PVL_OS_ERR
            }

            [[ ${action} =~ ^(install)|(remove)$ ]] || {
                log_error "${action}: unsupported action. Must be { 'install' | 'remove' }"
                pvl__usage
                exit $PVL_OS_ERR
            }
            break
            ;;
        *)
            log_fatal "[BUG] $1: unexpected command line option" >&2
            exit $PVL_OS_ERR
    esac
done

# handle sandboxing here because some paths might have been modified by CL options
pvl__make_sandbox caller_sandbox || log_fatal "Sandboxing failed."


################################################################################
# Functions
################################################################################

# Match a release URL, as given as 2nd command line arg, to the supported
# patterns and sets the required callers' variables passed as
# namerefs. Parameters:
#   $1: release URL
#   $2: &str(). The plugin's package file name
#   $3: &str(). The plugin's package version string
#   $4: &str(). The plugin's package file extension
#   $5: &str(). The plugin's package archive name -- UNUSED!
# *Return* $PVL_OS_ERR on failure.
function match_release_url() {
    local user=${1:?'arg #1 missing: release URL'}
    local -n plug_pf=${2:?"arg #2 missing: plugin's package file nameref"}
    local -n plug_pv=${3:?"arg #3 missing: plugin's package plug_pv nameref"}
    local -n plug_pe=${4:?"arg #4 missing: plugin's package file extension nameref"}
    # local -n plug_pa=${5:?"arg #5 missing: plugin's package archive nameref"}

    # only one token in the regex capture group shall match!
    local -A platforms=(
        [github]='^https?://github\.com/.+?/(releases|tags|heads)/'
        [git]='^https?://[^/]+/.+?/[^/]+\.(git)'
    )
    local -A platform_variants=(
        # https://github.com/github/codeql/archive/refs/heads/main.tar.gz
        # 3 capture groups:
        # ....................................plug_pn..plug_pv.................plug_pe
        [github_releases]='/releases/download/[^/]+/([^/]+)-v([[:digit:].]+)+\.(.+)$'
        # GitHub tags packages have no name, just version ~ 'vX.Y.Z.tar.gz',
        # however, the archive extracts to '<project>-X.Y.Z.' so
        # plugin-name == project-name
        #...............plug_pn....................plug_pv............plug_pe
        [github_tags]='/([^/]+)/archive/refs/tags/v?([[:digit:].]+)+\.(.+)$'
        #...............plug_pn.......................plug_pv....plug_pe
        [github_heads]='/([^/]+)/archive/refs/heads/v?([^/]+?)\.(zip|tar\..+)$'
        # .................plug_pn..plug_pe
        [git_git]='/[^/]+/([^/]+)\.(git)$'
    )

    # select a platform
    for platform in ${!platforms[*]}; do
        log_debug "trying platform: ${platform}"
        ppttrn=${platforms[$platform]}
        if [[ ${release_url} =~ ${ppttrn} ]]; then
            variant=${BASH_REMATCH[1]}
            log_debug "${platform}: detected platform variant '${variant}'"
            # apply the specific platform pattern variant
            pvariant="${platform}_${variant}"
            [[ ${release_url} =~ ${platform_variants[${pvariant}]} ]] || {
                log_error "[BUG] ${pvariant}: no match. Possibly broken regex"
                return $PVL_OS_ERR
            }

            plug_pf=${BASH_REMATCH[1]}
            plug_pv=${BASH_REMATCH[2]}
            plug_pe=${BASH_REMATCH[3]}
            # plug_pa=$(echo $(eval "echo ${archive_names[${pvariant}]}"))
            return $PVL_OS_OK
        fi
    done
    log_error "Unsupported platform."
    return $PVL_OS_ERR
} #match_release_url


# Handle *-compressed tarballs
function untar() {
    local cpr_type=${1:?'arg #1 missing: compression option, e.g. "-z"'}
    local arc_name=${2:?"arg #2 missing: archive name"}
    local dst_dir=${3?"arg #3 missing: destination directory"}

    peval "tar --strip-components=1 --directory ${dst_dir} ${cpr_type} -xf ${arc_name}"
}

# Handle zipballs
function un7z() {
    local arc_name=${1:?"arg #1 missing: archive name"}
    local dst_dir=${2?"arg #2 missing: destination directory"}

    peval "7z e -o${dst_dir} ${arc_name}"
}

# Handle curl downloads
function curl_dl() {
    local dst_dir=${1?"arg #1 missing: destination directory"}
    local dst_fname=${2:?"arg #2 missing: destination file name"}
    local url=${3:?'arg #3 missing: download URL'}

    peval "curl --output-dir ${dst_dir} --output ${dst_fname} --location ${url}"
}

# Handle plain git repos
function git_dl() {
    local arc_name=${1:?"arg #1 missing: archive name"}
    local dst_dir=${2?"arg #2 missing: destination directory"}

    log_error "Not implemented yet"
    return $PVL_OS_ERR
    git clone --depth=1
}

function install() {
    local release_url=${1:?'arg #1 missing: release URL'}

    cmd="mkdir -p ${download_dir} ${install_dir} \
    ${etc_plugin_dir}/${plugin} ${usr_plugin_dir} ${var_plugin_dir}"
    peval "${cmd}" || {
        log_error "Can't make needed directories. Failing command: '${cmd}'"
        return $PVL_OS_ERR
    }

    match_release_url $release_url plug_pn version fext || {
        log_error "Can't extract plugin package name, version, file extension from release URL."
        return $PVL_OS_ERR
    }

    log_debug "plug_pn=${plug_pn}, version=${version}, fext=${fext}"

    [[ "${plug_pn}" == "${plugin}" ]] || {
        log_warn "${plug_pn:-[none!?]}: extracted plugin package name doesn't match provided name '${plugin}'"
        # return $PVL_OS_ERR
    }

    log_info "Downloading '${plugin}' @ 'v${version}'..."
    # Notice that the downloaded file gets rename to a canonical one
    dload_prog=${downloaders[${fext}]}
    plug_an=${plugin}-v${version}.${fext}
    # cmd="curl --output-dir ${download_dir} --output ${plug_an} --location ${release_url}"
    ${dload_prog} ${download_dir} ${plug_an} ${release_url} || {
        log_error "Can't download the plugin."
        return $PVL_OS_ERR
    }

    log_info "Installing the plugin..."
    unarch_prog=${decompressors[${fext}]}
    # strip-components + directory => ~mv PLUGIN PLUGIN-VERSION ;-)
    install_dir=${install_dir}/${plugin}-${version}
    cmd="mkdir -p ${install_dir}"
    peval "${cmd}" || {
        log_error "Can't create the plugin's install dir. Failing command: '${cmd}'"
        return $PVL_OS_ERR
    }
    ${unarch_prog} ${download_dir}/${plug_an} ${install_dir} || {
        log_error "Can't extract the plugin archive"
        return $PVL_OS_ERR
    }

    <<SKIP_THIS
cmd="chown -R ${user}:${group} ${install_dir}"
peval "${cmd}" || {
    log_error "Can't chown install dir. Failing command: '${cmd}'"
    return $PVL_OS_ERR
}
SKIP_THIS

    log_info "Installing an empty config file..."
    # this part doesn't need peval
    tmp_config_file=$(mktemp --tmpdir=/tmp -t "${myself}-XXXXXX") || {
        log_error "Can't create tmp config file. Failing command: '${cmd}'"
        return $PVL_OS_ERR
    }

    {
        cat >${tmp_config_file} <<EOF
<?php
    // your ${plugin} config here
EOF
    } || {
        log_error "Can't write tmp config file. Failing command: '${cmd}'"
        return $PVL_OS_ERR
    }
    log_debug "tmp config file written: ${tmp_config_file}"

    config_file=${etc_plugin_dir}/${plugin}/${php_config_name}
    cmd="mv -b --suffix=.loc-old ${tmp_config_file} ${config_file} && chmod +r ${config_file}"
    peval "${cmd}" || {
        log_error "Can't install plugin's config file. Failing command: '${cmd}'"
        return $PVL_OS_ERR
    }

    log_info "Installing application symlinks..."
    cmd="ln -sf --no-dereference ${install_dir} ${usr_plugin_dir}/${plugin}"
    peval "${cmd}" || {
        log_error "Can't symlink usr plugin dir to install dir. Failing command: '${cmd}'"
        return $PVL_OS_ERR
    }

    cmd="ln -sf --no-dereference ${usr_plugin_dir}/${plugin} ${var_plugin_dir}"
    peval "${cmd}" || {
        log_error "Can't symlink usr plugin dir to install dir. Failing command: '${cmd}'"
        return $PVL_OS_ERR
    }

    cmd="ln -sf --no-dereference ${config_file} ${usr_plugin_dir}/${plugin}/${php_config_name}"
    peval "${cmd}" || {
        log_error "Can't symlink config file into usr plugin dir. Failing command: '${cmd}'"
        return $PVL_OS_ERR
    }

    return $PVL_OS_OK
}


function remove() {
    local version=${1:?'arg #1 missing: plugin version string'}

    install_dir=${install_dir}/${plugin}-${version}

    log_info "Checking distro's packages..."
    # check if plugin is provided by a distro package -- dpkg query fails when
    # no pkg matches
    if dpkg_match=$(${dpkg_query} -S ${plugin} | cut -d: -f1 | sort -u); then
        err_msg="${plugin}: the following distro's packages provides this plugin:\n${dpkg_match}\n=> "

        [[ ${force} ]] || {
            # bail out
            log_error "${err_msg}Better off removing it with your distro's package manager."
            return $PVL_OS_ERR
        }
        log_warn "${err_msg}*** Forcing removal! ***"
        read -n1 -p 'Please confirm (y|N): ' answer
        answer=${answer:-'no'}
        echo
        shopt -s nocasematch
        [[ ${answer} == 'y' ]] || {
            log_info "Bail out..."
            return $PVL_OS_OK
        }
        log_info "Going on..."
        shopt -u nocasematch
    fi

    [[ -d ${install_dir} ]] || log_fatal "${install_dir}: install dir not found. Possible version mismatch?"

    # proceed in reverse order of install commands
    log_info "Removing symlinks..."
    cmd="rm ${usr_plugin_dir}/${plugin}/${php_config_name} \
            ${var_plugin_dir}/${plugin} ${usr_plugin_dir}/${plugin}"
    peval "${cmd}" || {
        log_error "Can't remove symlinks. Failing command: '${cmd}'"
        return $PVL_OS_ERR
    }

    log_info "Removing plugin files..."
    cmd="rm -r ${install_dir}"
    peval "${cmd}" || {
        log_error "Can't remove plugins' files. Failing command: '${cmd}'"
        return $PVL_OS_ERR
    }

    log_info "Config files (possibly) left in '${etc_plugin_dir}/${plugin}'."

    return $PVL_OS_OK
}


################################################################################
# main
################################################################################

# it's as simple as this ;-)
$action $releaseurl_or_version || log_fatal "${action}: there were errors..."

exit 0
