#!/usr/bin/env bats
# -*- mode: bats -*-
################################################################################
# Copyright 2024 Marco Emilio Poleggi
#
# This file is part of bash-utilz.
#
# bash-utilz is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License Foundation version 3 as
# published by the Free Software Foundation.
#
# bash-utilz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with bash-utilz.  If not, see <http://www.gnu.org/licenses/>.
################################################################################

setup_file() {
    source peval.sh || {
        echo "[fatal] unable to load peval shell library" >&2
        exit -1
    }
}

setup() {
    bats_require_minimum_version 1.5.0
    load '/usr/share/bats-support/load'
    load '/usr/share/bats-assert/load'

    local_conf_file=$(mktemp -q)
    user_conf_file=$(mktemp -q)
    sys_conf_file=$(mktemp -q)
    output_file=$(mktemp -q)

    short_opts='a:b'
    long_opts='opt-a:,opt-b'
}

teardown() {
    rm -f $local_conf_file $user_conf_file $output_file
}


# bats test_tags=ingest
@test 'ingest_posargs() -- no args' {
    source test/fixture.sh
    declare -a argsn=()
    run pvl__ingest_posargs argsn
}

# bats test_tags=ingest
@test 'ingest_posargs() -- unexpected args' {
    source test/fixture.sh
    declare -a argsn=()
    run ! pvl__ingest_posargs argsn -- foo bar
    assert_output --partial "foo bar: unexpected caller's arguments."
}

# bats test_tags=ingest,args
@test 'ingest_posargs() -- 1 mandatory arg missing' {
    source test/fixture.sh
    declare -a argsn=(foo)
    run ! pvl__ingest_posargs argsn
    assert_output --partial "foo: mandatory caller's argument missing."
}

# bats test_tags=ingest,args
@test 'ingest_posargs() -- 1 mandatory args' {
    source test/fixture.sh
    declare -a argsn=(foo)
    foo=''
    # can't use 'run' because we're expecting globally declared vars
    pvl__ingest_posargs argsn -- baz
    assert_equal "$foo" baz
}

# bats test_tags=ingest,args
@test 'ingest_posargs() -- 2 mandatory args, 1 missing' {
    source test/fixture.sh
    declare -a argsn=(foo bar)
    foo=''
    bar=''

    # can't use 'run' because we're expecting globally declared vars
    # can't use $() for output because variables would be set in a subshell
    ! pvl__ingest_posargs argsn -- baz >$output_file 2>&1

    expected="bar: mandatory caller's argument missing."
    [[ $(<$output_file) =~ $expected ]]

    assert_equal "$foo" baz
    assert_equal "$bar" ''
}

# bats test_tags=ingest,args,def
@test 'ingest_posargs() -- 2 mandatory args, 1 default' {
    source test/fixture.sh
    declare -a argsn=(foo bar:'quux')
    foo=''
    bar=''
    pvl__ingest_posargs argsn -- baz
    assert_equal "$foo" baz
    assert_equal "$bar" quux
}

# bats test_tags=ingest,args,def
@test 'ingest_posargs() -- 2 mandatory args, 1 default overridden' {
    source test/fixture.sh
    declare -a argsn=(foo bar:'quux')
    foo=''
    bar=''
    pvl__ingest_posargs argsn -- baz gee
    assert_equal "$foo" baz
    assert_equal "$bar" gee
}

# bats test_tags=ingest,args,opt
@test 'ingest_posargs() -- 2 mandatory args, 1 optional' {
    source test/fixture.sh
    declare -a argsn=(foo bar:)
    foo=''
    bar=''
    pvl__ingest_posargs argsn -- baz
    assert_equal "$foo" baz
    assert_equal "$bar" ''
}

# bats test_tags=init,two-opts
@test 'init() -- opts: 1 bool unset, 1 non-bool unset' {
    opt_a=''
    opt_b=''

    source test/fixture.sh

    assert_equal ${caller_env[cl_opts]} '--'

    while true; do
        case "$1" in
            -a|--opt-a)
                opt_a=$2
                shift 2
                ;;
            -b|--opt-b)
                opt_b=$PVL_TRUE
                shift
                ;;
            --)
                break
                ;;
        esac
    done

    assert_equal "${opt_a}" "${PVL_FALSE}"
    assert_equal "${opt_b}" ""
}

# bats test_tags=init,two-opts
@test 'init() -- opts: 1 bool set, 1 non-bool unset' {
    opt_a=''
    opt_b=''

    set -- -b

    source test/fixture.sh

    assert_equal "${caller_env[cl_opts]}" '-b --'

    while true; do
        case "$1" in
            -a|--opt-a)
                opt_a=$2
                shift 2
                ;;
            -b|--opt-b)
                opt_b=$PVL_TRUE
                shift
                ;;
            --)
                break
                ;;
        esac
    done

    assert_equal "${opt_b}" "${PVL_TRUE}"
    assert_equal "${opt_a}" ""
}

# bats test_tags=init,two-opts
@test 'init() -- opts: 1 bool set long, 1 non-bool unset' {
    opt_a=''
    opt_b=''

    set -- --opt-b

    source test/fixture.sh

    assert_equal "${caller_env[cl_opts]}" '--opt-b --'

    while true; do
        case "$1" in
            -a|--opt-a)
                opt_a=$2
                shift 2
                ;;
            -b|--opt-b)
                opt_b=$PVL_TRUE
                shift
                ;;
            --)
                break
                ;;
        esac
    done

    assert_equal "${opt_b}" "${PVL_TRUE}"
    assert_equal "${opt_a}" ""
}

# bats test_tags=init,two-opts
@test 'init() -- opts: 1 bool unset, 1 non-bool set' {
    opt_a=''
    opt_b=''

    set -- -a foo

    source test/fixture.sh

    # long options are converted to short ones
    assert_equal "${caller_env[cl_opts]}" '-a foo --'

    while true; do
        case "$1" in
            -a|--opt-a)
                opt_a=$2
                shift 2
                ;;
            -b|--opt-b)
                opt_b=$PVL_TRUE
                shift
                ;;
            --)
                break
                ;;
        esac
    done

    assert_equal "${opt_b}" "${PVL_FALSE}"
    assert_equal "${opt_a}" "foo"
}

# bats test_tags=init,two-opts
@test 'init() -- opts: 1 bool set, 1 non-bool long set' {
    opt_a=''
    opt_b=''

    # short variation without space
    set -- -afoo --opt-b

    source test/fixture.sh

    assert_equal "${caller_env[cl_opts]}" '-a foo --opt-b --'

    while true; do
        case "$1" in
            -a|--opt-a)
                opt_a=$2
                shift 2
                ;;
            -b|--opt-b)
                opt_b=$PVL_TRUE
                shift
                ;;
            --)
                break
                ;;
        esac
    done

    assert_equal "${opt_b}" "${PVL_TRUE}"
    assert_equal "${opt_a}" "foo"
}

# bats test_tags=init,two-opts
@test 'init() -- opts: 1 bool long set, 1 non-bool set' {
    opt_a=''
    opt_b=''

    # = signal is removed
    set -- --opt-a=foo -b

    source test/fixture.sh

    assert_equal "${caller_env[cl_opts]}" '--opt-a foo -b --'

    while true; do
        case "$1" in
            -a|--opt-a)
                opt_a=$2
                shift 2
                ;;
            -b|--opt-b)
                opt_b=$PVL_TRUE
                shift
                ;;
            --)
                break
                ;;
        esac
    done

    assert_equal "${opt_b}" "${PVL_TRUE}"
    assert_equal "${opt_a}" "foo"
}

# bats test_tags=init,invalid
@test 'init() -- opts: invalid short' {
    set -- -c

    source test/fixture-no-init.sh

    run ! pvl__init caller_env caller_data
    assert_output --partial "invalid option -- 'c'"
}

# bats test_tags=init,invalid
@test 'init() -- opts: invalid long' {
    set -- --opt-c

    source test/fixture-no-init.sh

    run ! pvl__init caller_env caller_data
    # don't ask me why the message is different wrt the '-c' case!?
    assert_output --partial "unrecognized option '--opt-c'"
}
