#!/usr/bin/env bats
# -*- mode: bats -*-
################################################################################
# Copyright 2024 Marco Emilio Poleggi
#
# This file is part of bash-utilz.
#
# bash-utilz is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License Foundation version 3 as
# published by the Free Software Foundation.
#
# bash-utilz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with bash-utilz.  If not, see <http://www.gnu.org/licenses/>.
################################################################################

shopt -s expand_aliases

# peval's getopt test would return 4, so ignore it
alias load_peval='source peval.sh || true'

setup_file() {
    # disable ERR trap to avoid interference
    PVL_TRAP_ERR=
    export  PVL_TRAP_ERR

    source peval.sh || {
        echo "[fatal] unable to load peval shell library" >&2
        exit -1
    }
}

setup() {
    bats_require_minimum_version 1.5.0
    load '/usr/share/bats-support/load'
    load '/usr/share/bats-assert/load'

    output_file=$(mktemp -q)
}

teardown() {
    rm -f $output_file
}

# bats test_tags=expand_text_tpl
@test 'pvl__expand_text_tpl()' {
    load_peval

    declare -A bar=(
        [baz]='123'
    )
    quux='gee'

    # mind the single quotes to avoid expansion
    text_in=$(cat <<'EOF'
Foo ${bar[baz]}
and
  ${quux}
EOF
)

    text_out=$(cat <<EOF
Foo ${bar[baz]}
and
  ${quux}
EOF
)

    run pvl__expand_text_tpl "$text_in"
    assert_output "$text_out"
}

# bats test_tags=pvl__unquote
@test 'pvl__unquote()' {
    load_peval

    text_in=$(cat <<EOF
Foo "bar"
and
  'quux'
EOF
)

    text_out=$(cat <<EOF
Foo bar
and
  quux
EOF
)

    run pvl__unquote "$text_in"
    assert_output "$text_out"
}

# bats test_tags=pvl__sed
@test 'pvl__sed() -- subshell forbidden' {
    load_peval

    expected="This function can't be called within a subshell (BASH_SUBSHELL=1)!"

    # can't use 'run' either
    ! msg=$(pvl__sed 2>&1)

    [[ "$msg" =~ "$expected" ]]
}

# bats test_tags=pvl__sed
@test 'pvl__sed() -- no match' {
    load_peval

    text_in=$(cat <<EOF
Foo bar
bar quux
baz gee
EOF
)

    declare -A match=()

    ! pvl__sed "$text_in" 'no match' match

    assert_equal "${#match[@]}" 0
}

# bats test_tags=pvl__sed
@test 'pvl__sed() -- matches without capture' {
    load_peval

    text_in=$(cat <<EOF
Foo bar
bar quux
baz gee
EOF
)

    declare -A match=()

    pvl__sed "$text_in" '^.*baz.*$' match

    assert_equal "${#match[@]}" 1
    assert_equal "${match[0,0]}" "baz gee"
}


# bats test_tags=pvl__sed
@test 'pvl__sed() -- matches with one capture' {
    load_peval

    text_in=$(cat <<EOF
Foo 123
4567 quux
no match here
gee 89 bar
EOF
)

    declare -A match=()

    # non-greedy patterns are not supported in Bash
    pvl__sed "$text_in" '^[^[:digit:]]*([[:digit:]]+)[^[:digit:]]*$' match

    assert_equal "${#match[@]}" 6
    assert_equal "${match[0,0]}" 'Foo 123'
    assert_equal "${match[0,1]}" 123
    assert_equal "${match[1,0]}" '4567 quux'
    assert_equal "${match[1,1]}" 4567
    assert_equal "${match[2,0]}" 'gee 89 bar'
    assert_equal "${match[2,1]}" 89
}


# bats test_tags=pvl__sed
@test 'pvl__sed() -- matches with more captures' {
    load_peval

    text_in=$(cat <<EOF
Foo 123 quux 4567
no match here
89 bar 943 baz
nothing here
EOF
)

    declare -A match=()

    # non-greedy patterns are not supported in Bash
    pvl__sed "$text_in" '([[:digit:]]+)[^[:digit:]]*([[:digit:]]+)' match

    assert_equal "${#match[@]}" 6
    assert_equal "${match[0,0]}" '123 quux 4567'
    assert_equal "${match[0,1]}" 123
    assert_equal "${match[0,2]}" 4567
    assert_equal "${match[1,0]}" '89 bar 943'
    assert_equal "${match[1,1]}" 89
    assert_equal "${match[1,2]}" 943
}
