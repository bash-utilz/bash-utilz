#!/usr/bin/env bats
# -*- mode: bats -*-
################################################################################
# Copyright 2024 Marco Emilio Poleggi
#
# This file is part of bash-utilz.
#
# bash-utilz is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License Foundation version 3 as
# published by the Free Software Foundation.
#
# bash-utilz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with bash-utilz.  If not, see <http://www.gnu.org/licenses/>.
################################################################################

setup_file() {
    # disable ERR trap to avoid interference
    PVL_TRAP_ERR=

    export PVL_TRAP_ERR

    source peval.sh || {
        echo "[fatal] unable to load peval shell library" >&2
        exit -1
    }
}

setup() {
    bats_require_minimum_version 1.5.0
    load '/usr/share/bats-support/load'
    load '/usr/share/bats-assert/load'

    local_conf_file=$(mktemp -q)
    user_conf_file=$(mktemp -q)
    sys_conf_file=$(mktemp -q)

    short_opts='a:b'
    long_opts='opt-a:,opt-b'

    source test/fixture.sh
}

teardown() {
    rm -f $local_conf_file $user_conf_file $sys_conf_file
}


# bats test_tags=ingest
@test 'ingest_conf_file() -- nonexistent file' {
    declare -A caller_defaults=()
    run ! pvl__ingest_conf_file non_exitent_file caller_defaults
    assert_output --partial "No such file or directory"
}

# bats test_tags=ingest
@test 'ingest_conf_file() -- empty file' {
    declare -A caller_defaults=()
    run pvl__ingest_conf_file $local_conf_file caller_defaults
}

# bats test_tags=ingest
@test 'ingest_conf_file() -- only comments and empty lines' {
    declare -A caller_defaults=()
    cat >$local_conf_file <<'EOF'
# foo bar

# quux

EOF
    run pvl__ingest_conf_file $local_conf_file caller_defaults
}

# bats test_tags=ingest,unmatch
@test 'ingest_conf_file() -- unmatched default var' {
    declare -A caller_defaults=(
        [bar]=''
    )
    cat >$local_conf_file <<'EOF'
foo=bar
EOF
    run ! pvl__ingest_conf_file $local_conf_file caller_defaults
    assert_output --partial "not in caller's defaults"
    assert_output --partial "1 error(s)"
}

# bats test_tags=ingest,invalid
@test 'ingest_conf_file() -- invalid line command' {
    declare -A caller_defaults=(
        [foo]=quux
    )
    cat >$local_conf_file <<'EOF'
touch evil
EOF
    run ! pvl__ingest_conf_file $local_conf_file caller_defaults
    assert_output --partial "no equal sign"
    assert_output --partial "1 error(s)"
}

# bats test_tags=ingest,invalid
@test 'ingest_conf_file() -- invalid line \$()' {
    declare -A caller_defaults=(
        [foo]=quux
    )
    cat >$local_conf_file <<'EOF'
#
foo=$(might screw me)
EOF
    run ! pvl__ingest_conf_file $local_conf_file caller_defaults
    assert_output --partial "fail declare"
    assert_output --partial "1 error(s)"
}

# bats test_tags=ingest,invalid
@test 'ingest_conf_file() -- invalid line \`\`' {
    declare -A caller_defaults=(
        [foo]=quux
    )
    cat >$local_conf_file <<'EOF'
foo=`might screw me`
EOF
    run ! pvl__ingest_conf_file $local_conf_file caller_defaults
    assert_output --partial "fail declare"
    assert_output --partial "1 error(s)"
}

# bats test_tags=ingest,valid,indirect
@test 'ingest_conf_file() -- valid line -- indirect assignment from empty var' {
    declare -A caller_defaults=(
        [foo]=quux
        [bar]=baz
    )
    # this is just to avoid unbound variables enforced by peval....
    foo=''
    cat >$local_conf_file <<'EOF'
bar=$foo
EOF
    pvl__ingest_conf_file $local_conf_file caller_defaults
    assert_equal "${caller_defaults[bar]}" ''
}

# bats test_tags=ingest,valid,indirect
@test 'ingest_conf_file() -- valid line -- indirect assignment from good var' {
    declare -A caller_defaults=(
        [foo]=quux
        [bar]=baz
    )
    # this is just to avoid unbound variables enforced by peval....
    foo=''
    cat >$local_conf_file <<'EOF'
foo=baz
bar=$foo
EOF
    pvl__ingest_conf_file $local_conf_file caller_defaults
    assert_equal "${caller_defaults[bar]}" 'baz'
}

# bats test_tags=ingest,valid,unbound
@test 'ingest_conf_file() -- valid line -- unbound var' {
    declare -A caller_defaults=(
        [foo]=quux
    )
    foo=''
    cat >$local_conf_file <<'EOF'
# even though valid, expands to nothing => unbound var
foo=()
EOF
    run ! pvl__ingest_conf_file $local_conf_file caller_defaults
    assert_output --partial "varv: unbound variable"
}

# bats test_tags=ingest,valid,empty
@test 'ingest_conf_file() -- valid line -- empty var' {
    declare -A caller_defaults=(
        [foo]=quux
    )
    foo=''
    cat >$local_conf_file <<'EOF'
foo=
EOF
    pvl__ingest_conf_file $local_conf_file caller_defaults
    # quoted => avoid unbound
    assert_equal "${caller_defaults[foo]}" ''
}

# bats test_tags=ingest,invalid,fishy
@test 'ingest_conf_file() -- invalid lines -- fishy chars' {
    declare -A caller_defaults=(
        [foo]=quux
    )
    foo=''
    cat >$local_conf_file <<'EOF'
# these are KO
foo=&
foo=|
foo=*
foo=?
# these are OK
foo=$
foo={
foo=}
foo={}
foo=[
foo=]
foo=[]
EOF
    run ! pvl__ingest_conf_file $local_conf_file caller_defaults
    assert_output --partial "fishy chars"
    assert_output --partial "4 error(s)"
}

# bats test_tags=ingest,invalid,strict,fishy
@test 'ingest_conf_file() -- invalid lines -- fishy chars (strict)' {
    declare -A caller_defaults=(
        [foo]=quux
    )
    foo=''
    cat >$local_conf_file <<'EOF'
# these are KO
foo=&
foo=|
foo=*
foo=?
foo=$
# these are OK
foo={
foo=}
foo={}
foo=[
foo=]
foo=[]
foo=(
foo=)
foo=()
EOF
    run ! pvl__ingest_conf_file $local_conf_file caller_defaults $PVL_TRUE
    assert_output --partial "fishy chars"
    assert_output --partial "5 error(s)"
}

# bats test_tags=ingest,invalid,eval
@test 'ingest_conf_file() -- invalid line -- fail eval' {
    declare -A caller_defaults=(
        [foo]=quux
    )
    foo=''
    cat >$local_conf_file <<'EOF'
# this is allowed but breaks because of eval in non-strict mode:
foo=)
EOF
    run ! pvl__ingest_conf_file $local_conf_file caller_defaults
    assert_output --partial "fail eval"
    assert_output --partial "1 error(s)"
}

# bats test_tags=ingest,valid,quoted
@test 'ingest_conf_file() -- valid line -- quoted )' {
    declare -A caller_defaults=(
        [foo]=''
    )
    cat >$local_conf_file <<'EOF'
foo=')'
EOF
    pvl__ingest_conf_file $local_conf_file caller_defaults
    assert_equal ${caller_defaults[foo]} ')'
}

# bats test_tags=ingest,invalid,eval
@test 'ingest_conf_file() -- invalid line -- EOF' {
    declare -A caller_defaults=(
        [foo]=quux
    )
    foo=''
    cat >$local_conf_file <<'EOF'
# these is allowed but breaks because of eval in non-strict mode:
foo=(
EOF
    run ! pvl__ingest_conf_file $local_conf_file caller_defaults
    echo "output: '$output'"
    assert_output --partial "unexpected EOF while looking for matching"
}

# bats test_tags=ingest,valid,quoted
@test 'ingest_conf_file() -- valid line -- quoted ()' {
    declare -A caller_defaults=(
        [foo]=''
    )
    cat >$local_conf_file <<'EOF'
foo='('
EOF
    pvl__ingest_conf_file $local_conf_file caller_defaults
    assert_equal ${caller_defaults[foo]} '('
}

# bats test_tags=ingest,valid,assign
@test 'ingest_conf_file() -- valid simple assignments' {
    declare -A caller_defaults=(
        [foo]=quux
        [bar]=baz
    )
    cat >$local_conf_file <<'EOF'
foo=bar
bar=might-have-equal-sign=0,--quux-gee
EOF
    # can't use 'run' because that would change the nameref scope
    pvl__ingest_conf_file $local_conf_file caller_defaults
    assert_equal ${caller_defaults[foo]} 'bar'
    assert_equal ${caller_defaults[bar]} 'might-have-equal-sign=0,--quux-gee'
}

# bats test_tags=ingest,valid,expand
@test 'ingest_conf_file() -- valid expanded assignments' {
    declare -A caller_defaults=(
        [foo]=quux
        [bar]=baz
    )
    foo=''
    bar=''
    cat >$local_conf_file <<'EOF'
foo=gee
bar=${foo}
EOF
    # can't use 'run' because that would change the nameref scope
    pvl__ingest_conf_file $local_conf_file caller_defaults
    assert_equal ${caller_defaults[foo]} 'gee'
    assert_equal ${caller_defaults[bar]} 'gee'
}

# bats test_tags=load,missing
@test 'load_conf_files() -- default path, missing files' {
    declare -A caller_defaults=(
        [foo]=quux
        [bar]=baz
    )

	local_conf_file='local-not-there'
    user_conf_file='user-not-there'
    sys_conf_file='sys-not-there'

    # yep, need to reload the fixture as setup() chokes on 'declare -A'!?
    source test/fixture.sh

	declare -A cfiles=(
		[local]=$local_conf_file
		[user]=$user_conf_file
		[system]=$sys_conf_file
	)

    run pvl__load_caller_conf caller_defaults

	for scope in ${!cfiles[@]}; do
		f=${cfiles[$scope]}
		assert_output --partial "${f}: ignoring ${scope} configuration file -- requested but missing or non readable"
	done

    assert_equal ${caller_defaults[foo]} 'quux'
    assert_equal ${caller_defaults[bar]} 'baz'
}


# bats test_tags=load,local,empty
@test 'load_caller_conf() -- local path, empty file' {
    declare -A caller_defaults=(
        [foo]=quux
        [bar]=baz
    )

    local_conf_file='.empty-conf'
	touch $local_conf_file

	source test/fixture.sh

    run pvl__load_caller_conf caller_defaults

    assert_equal ${caller_defaults[foo]} 'quux'
    assert_equal ${caller_defaults[bar]} 'baz'

	rm $local_conf_file
}


# bats test_tags=load,empty
@test 'load_conf_files() -- default path, empty files' {
    declare -A caller_defaults=(
        [foo]=quux
        [bar]=baz
    )
    source test/fixture.sh

    pvl__load_caller_conf caller_defaults

    assert_equal ${caller_defaults[foo]} 'quux'
    assert_equal ${caller_defaults[bar]} 'baz'
}

# bats test_tags=load,emptyarg
@test 'load_caller_conf() -- empty file args' {
    declare -A caller_defaults=(
        [foo]=quux
        [bar]=baz
    )

    local_conf_file=''
    user_conf_file=''
    sys_conf_file=''

    source test/fixture.sh

    run pvl__load_caller_conf caller_defaults

    assert_equal ${caller_defaults[foo]} 'quux'
    assert_equal ${caller_defaults[bar]} 'baz'
}

# bats test_tags=load,user
@test 'load_caller_conf() -- user override' {
    declare -A caller_defaults=(
        [foo]=quux
    )
    cat >$user_conf_file <<'EOF'
foo=bar
EOF
    source test/fixture.sh

    pvl__load_caller_conf caller_defaults

    assert_equal ${caller_defaults[foo]} 'bar'
}

# bats test_tags=load,user,local
@test 'load_caller_conf() -- user and local (override) default files' {
    declare -A caller_defaults=(
        [foo]=quux
        [bar]=baz
    )
    cat >$user_conf_file <<'EOF'
foo=bar
bar=quux
EOF
    cat >$local_conf_file <<'EOF'
foo=baaz
EOF
    source test/fixture.sh

    pvl__load_caller_conf caller_defaults

    assert_equal ${caller_defaults[foo]} 'baaz'
    assert_equal ${caller_defaults[bar]} 'quux'
}

# bats test_tags=load,sys,user,local
@test 'load_caller_conf() -- sys, user and local (override) default files' {
    declare -A caller_defaults=(
        [foo]=quux
        [bar]=baz
        [gee]=bah
    )
    cat >$sys_conf_file <<'EOF'
foo=bar
gee=boo
EOF
    cat >$user_conf_file <<'EOF'
foo=bar
bar=quux
EOF
    cat >$local_conf_file <<'EOF'
foo=baaz
EOF
    source test/fixture.sh

    pvl__load_caller_conf caller_defaults

    assert_equal ${caller_defaults[foo]} 'baaz'
    assert_equal ${caller_defaults[bar]} 'quux'
    assert_equal ${caller_defaults[gee]} 'boo'
}

# bats test_tags=load,sys,user,local,sandbox
@test 'load_caller_conf() -- sandbox: sys, user and local (override) default files' {
    PVL_SYS_ROOT=$(mktemp -q -d)

    # original path to be sandboxed
    cfg_dir='/etc'
    sys_conf_file="${cfg_dir}/sys.conf"
    user_conf_file="${cfg_dir}/user.conf"
    local_conf_file="${cfg_dir}/local.conf"

    declare -a caller_sandbox=(
        cfg_dir
    )

    declare -A caller_defaults=(
        [foo]=quux
        [bar]=baz
        [gee]=bah
    )

    source test/fixture.sh

    pvl__is_sandboxed

    pvl__make_sandbox caller_sandbox

    # path is now prefixed
    assert_equal $cfg_dir "${PVL_SYS_ROOT}/etc"

    mkdir -p $cfg_dir

    # these would already exist in an actual program
    cat >${PVL_SYS_ROOT}/${sys_conf_file} <<'EOF'
foo=bar
gee=boo
EOF

    cat >${PVL_SYS_ROOT}/${user_conf_file} <<'EOF'
foo=bar
bar=quux
EOF

    cat >${PVL_SYS_ROOT}/${local_conf_file} <<'EOF'
foo=baaz
EOF

    # will load conf files from sandbox
    pvl__load_caller_conf caller_defaults

    assert_equal ${caller_defaults[foo]} 'baaz'
    assert_equal ${caller_defaults[bar]} 'quux'
    assert_equal ${caller_defaults[gee]} 'boo'

    # you never know...
    if [[ "${PVL_SYS_ROOT}" ]] && [[ ${PVL_SYS_ROOT} =~ ^/tmp/.+$ ]]; then
        rm -rf ${PVL_SYS_ROOT}
    else
        fail "*** WARNING *** Unexpected dangerous value for PVL_SYS_ROOT: ${PVL_SYS_ROOT}"
    fi
}
