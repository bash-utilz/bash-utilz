#!/usr/bin/env bats
# -*- mode: bats -*-
################################################################################
# Copyright 2024 Marco Emilio Poleggi
#
# This file is part of bash-utilz.
#
# bash-utilz is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License Foundation version 3 as
# published by the Free Software Foundation.
#
# bash-utilz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with bash-utilz.  If not, see <http://www.gnu.org/licenses/>.
################################################################################

shopt -s expand_aliases

# peval's getopt test would return 4, so ignore it
alias load_peval='source peval.sh || true'

setup_file() {
    source peval.sh || {
        echo "[fatal] unable to load peval shell library" >&2
        exit -1
    }
}

setup() {
    bats_require_minimum_version 1.5.0
    load '/usr/share/bats-support/load'
    load '/usr/share/bats-assert/load'

    tmp_file=$(mktemp -q)
}

teardown() {
    rm -f $tmp_file
}

# bats test_tags=peval,dummy,dry-run
@test 'pvl__eval() -- dummy, dry-run' {
    load_peval

    PVL_PRETEND=$PVL_TRUE
    cmd="echo foo bar"
    run pvl__eval "${cmd}"
    assert_output --partial "[pretend] ${cmd}"
}

# bats test_tags=peval,dummy
@test 'pvl__eval() -- dummy' {
    load_peval

    cmd="echo foo bar"
    run pvl__eval "${cmd}"
    assert_output --partial "foo bar"
}

# bats test_tags=peval,true
@test 'pvl__eval() -- true' {
    load_peval

    cmd="true"
    run pvl__eval "${cmd}"
}

# bats test_tags=peval,false
@test 'pvl__eval() -- false' {
    load_peval

    cmd="false"
    run ! pvl__eval "${cmd}"
}

# bats test_tags=peval,catch
@test 'pvl__eval() -- catch' {
    load_peval

    cmd="echo 'got some problem'"
    run ! pvl__eval "${cmd}" 'catch:problem'
    assert_output --partial "got some problem"
}

# bats test_tags=peval,catch,regex
@test 'pvl__eval() -- catch, regex' {
    load_peval

    cmd="echo 'got some WeIrD (TM) things going on'"
    run ! pvl__eval "${cmd}" 'catch:problem|weird\s+.+?things'
    assert_output --partial "got some WeIrD (TM) things going on"
}

# bats test_tags=peval,bad-catch
@test 'pvl__eval() -- bad catch' {
    load_peval

    cmd="echo 'got some problem'"
    # match != catch => token not recognized
    run pvl__eval "${cmd}" 'match:problem'
    assert_output --partial "got some problem"
}

# bats test_tags=peval,ppass
@test 'pvl__eval() -- ppass' {
    load_peval

    cmd="echo 'got some problem'"
    PVL_PRETEND=1
    run pvl__eval "${cmd}" 'whatever'
    assert_output --partial "got some problem"
}

# bats test_tags=peval,pipe
@test 'pvl__eval() -- pipe' {
    load_peval

    cmd="echo 'foo bar' | sed -r 's/bar/quux/'"
    run pvl__eval "${cmd}"
    assert_output --partial 'foo quux'
}

# bats test_tags=peval,redirect
@test 'pvl__eval() -- redirect' {
    load_peval

    cmd="echo 'foo bar' > ${tmp_file}"
    run pvl__eval "${cmd}"

    [[ 'foo bar' == $(<${tmp_file}) ]]
}

# bats test_tags=peval,pipe,redirect
@test 'pvl__eval() -- pipe, redirect' {
    load_peval

    cmd="echo 'foo bar' | sed -r 's/bar/quux/' > ${tmp_file}"
    run pvl__eval "${cmd}"

    [[ 'foo quux' == $(<${tmp_file}) ]]
}
