#!/usr/bin/env bats
# -*- mode: bats -*-
################################################################################
# Copyright 2024, 2025 Marco Emilio Poleggi
#
# This file is part of bash-utilz.
#
# bash-utilz is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License Foundation version 3 as
# published by the Free Software Foundation.
#
# bash-utilz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with bash-utilz.  If not, see <http://www.gnu.org/licenses/>.
################################################################################

shopt -s expand_aliases

# peval's getopt test would return 4, so ignore it
alias load_peval='source peval.sh || true'
alias run-sse='run --separate-stderr'

setup_file() {
    PVL_SYS_ROOT=$(mktemp -q -d)
    # disable ERR trap to avoid interference
    PVL_TRAP_ERR=

    export PVL_SYS_ROOT PVL_TRAP_ERR

    source peval.sh || {
        echo "[fatal] unable to load peval shell library" >&2
        exit -1
    }
}

setup() {
    bats_require_minimum_version 1.5.0
    load '/usr/share/bats-support/load'
    load '/usr/share/bats-assert/load'
}

teardown_file() {
    # you never know...
    if [[ "${PVL_SYS_ROOT}" ]] && [[ ${PVL_SYS_ROOT} =~ ^/tmp/.+$ ]]; then
        rm -rf ${PVL_SYS_ROOT}
    else
        fail "*** WARNING *** Unexpected dangerous value for PVL_SYS_ROOT: ${PVL_SYS_ROOT}"
    fi
}

# bats test_tags=pvl__is_sandboxed
@test 'pvl__is_sandboxed' {
    source test/fixture.sandbox.bash

    # be explicit here -- forbidden characters are not checked here
    PVL_SYS_ROOT=my-sandbox

    run-sse -$PVL_OS_TRUE pvl__is_sandboxed
    assert_output $PVL_SYS_ROOT

    unset PVL_SYS_ROOT
    run-sse -$PVL_OS_FALSE pvl__is_sandboxed
    assert_output ''
}

# bats test_tags=pvl__make_sandbox
@test 'pvl__make_sandbox -- forbidden chars in root' {
    source test/fixture.sandbox.bash

    declare -a caller_sandbox=(foo)
    # generate invalid printable chars in ASCII range [32-127] decimal
    re="[^${PVL_PATH_CHARS}]"
    for i in {32..127}; do
        c=$(pvl__char_d2a $i)
        [[ $c =~ $re ]] && {
            PVL_SYS_ROOT="/tmp/root-${c}-invalid"
            run ! pvl__make_sandbox caller_sandbox
            assert_output --partial "$PVL_SYS_ROOT: path has forbidden characters"
        }
    done
}

# bats test_tags=pvl__make_sandbox
@test 'pvl__make_sandbox -- forbidden chars in name' {
    source test/fixture.sandbox.bash

    # generate invalid printable chars in ASCII range [32-127] decimal
    re="[^${PVL_PATH_CHARS}]"
    for i in {32..127}; do
        c=$(pvl__char_d2a $i)
        [[ $c =~ $re ]] && {
            name="name-${c}-invalid"
            declare -a caller_sandbox=("$name")
            run ! pvl__make_sandbox caller_sandbox
            assert_output --partial "$name: invalid variable name"
        }
    done
}

# bats test_tags=pvl__make_sandbox
@test 'pvl__make_sandbox -- forbidden chars in path' {
    source test/fixture.sandbox.bash

    # generate invalid printable chars in ASCII range [32-127] decimal
    re="[^${PVL_PATH_CHARS}]"
    for i in {32..127}; do
        c=$(pvl__char_d2a $i)
        [[ $c =~ $re ]] && {
            path="path-${c}-invalid"
            declare -a caller_sandbox=(path)
            run ! pvl__make_sandbox caller_sandbox
            assert_output --partial "$path: path has forbidden characters"
        }
    done
}

# bats test_tags=pvl__make_sandbox
@test 'pvl__make_sandbox' {
    source test/fixture.sandbox.bash

    my_sandbox=$(pvl__is_sandboxed)

    path_1=/foo/bar
    path_2=/foo/baz

    declare -a caller_sandbox=(
        path_1
        path_2
    )

    pvl__make_sandbox caller_sandbox

    assert_equal $path_1 "${PVL_SYS_ROOT}/foo/bar"
    assert_equal $path_2 "${PVL_SYS_ROOT}/foo/baz"

    # paths are not created
    assert [ ! -d $path_1 ]
    assert [ ! -d $path_2 ]
}
