################################################################################
# Copyright 2024,2025 Marco Emilio Poleggi
#
# This file is part of bash-utilz.
#
# bash-utilz is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License Foundation version 3 as
# published by the Free Software Foundation.
#
# bash-utilz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with bash-utilz.  If not, see <http://www.gnu.org/licenses/>.
################################################################################

source peval.sh || {
    # getopt test would return 4, so ignore it
    echo "don't worry ;-)" >&3
}

myself=$(basename $0)

declare -A caller_data=(
    [name]=$myself
    [path]=$0
    [usage_msg]="Blah, blah"
    # [short_opts]=$short_opts
    # [long_opts]=$long_opts
)

declare -A caller_env=(
    [cl_opts]=''
    [lib_env]=''
)

pvl__init caller_env caller_data || exit $PVL_OS_ERR

# [[ ${caller_env[lib_env]} ]] && eval export ${caller_env[lib_env]}
# [[ ${caller_env[cl_opts]} ]] && eval set -- ${caller_env[cl_opts]}
