#!/usr/bin/env bats
# -*- mode: bats -*-
################################################################################
# Copyright 2024, 2025 Marco Emilio Poleggi
#
# This file is part of bash-utilz.
#
# bash-utilz is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License Foundation version 3 as
# published by the Free Software Foundation.
#
# bash-utilz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with bash-utilz.  If not, see <http://www.gnu.org/licenses/>.
################################################################################

shopt -s expand_aliases

# peval's getopt test would return 4, so ignore it
alias load_peval='source peval.sh || true'

setup_file() {
    source peval.sh || {
        echo "[fatal] unable to load peval shell library" >&2
        exit -1
    }
}

setup() {
    bats_require_minimum_version 1.5.0
    load '/usr/share/bats-support/load'
    load '/usr/share/bats-assert/load'

    dummy_caller=$(mktemp -q)

    name='dummy_caller'
    version='1.0'
    short_opts='a:b'
    long_opts='opt-a:,opt-b'
    # force parsing the dummy script
    path=$dummy_caller

    # disable ERR trap to avoid interference
    PVL_TRAP_ERR=
}

teardown() {
    rm -f $dummy_caller
}


# bats test_tags=pvl__usage
@test 'pvl__usage() -- no arguments expected' {
    # Cant' use bats' load() because of associative arrays :-(
    source test/fixture.usage.bash

    run ! pvl__usage foo

    assert_output --partial "This function doesn't take arguments"
}

@test 'pvl__usage() -- malformed name' {
    source test/fixture.usage.bash

    cat >$dummy_caller <<'EOF'
  -a|--opt-a)  #@USAGE: set @{mal[formed}
EOF

    run ! pvl__usage

    assert_output --partial "mal[formed': invalid variable name for name reference"
}

# bats test_tags=pvl__usage
@test 'pvl__usage() -- no expansion requested' {
    # Cant' use bats' load() because of associative arrays :-(
    source test/fixture.usage.bash

    # mind the single quotes to avoid expansion
    cat >$dummy_caller <<'EOF'
  -a|--opt-a)  #@USAGE: set this to something -- default: foo
EOF

    run pvl__usage

    assert_output --regexp '-a, --opt-a\s+set this to something -- default: foo'
}

# bats test_tags=pvl__usage
@test 'pvl__usage() -- expansion requested, failure' {
    source test/fixture.usage.bash

    cat >$dummy_caller <<'EOF'
  -a|--opt-a)  #@USAGE: set $this to something -- default: @{this}
EOF

    run ! pvl__usage

    assert_output --partial 'name: unbound variable'
}

# bats test_tags=pvl__usage
@test 'pvl__usage() -- one expansion requested' {
    source test/fixture.usage.bash

    this='foo'

    cat >$dummy_caller <<'EOF'
  -a|--opt-a)  #@USAGE: set $this to something -- default: @{this}
  -b|--opt-b)  #@USAGE: set $that to anything -- no default
EOF

    run pvl__usage

    # note that only @{...} tokens are expanded as simple vars
    assert_output --regexp '-a, --opt-a\s+set \$this to something -- default: '${this}
    assert_output --regexp '-b, --opt-b\s+set \$that to anything -- no default'
}

# bats test_tags=pvl__usage
@test 'pvl__usage() -- more expansions requested' {
    source test/fixture.usage.bash

    this='foo'
    that='bar'

    cat >$dummy_caller <<'EOF'
  -a|--opt-a)  #@USAGE: set $this (@{this}) and $that (@{that})
EOF

    run pvl__usage

    # note that only @{...} tokens are expanded as simple vars
    regex='-a, --opt-a\s+set $this ('$this') and $that ('${that}')'
    assert_output --regexp $regex
}

@test 'pvl__usage() -- array expansion' {
    source test/fixture.usage.bash

    declare -A this=([foo]='one' [bar]='two')

    cat >$dummy_caller <<'EOF'
  -a|--opt-a)  #@USAGE: set $this (@{this[foo]}) and $that (@{this[bar]})
EOF

    run pvl__usage

    regex='-a, --opt-a\s+set $this ('${this[foo]}') and $that ('${this[bar]}')'
    assert_output --regexp $regex
}
