################################################################################
# Copyright 2024 Marco Emilio Poleggi
#
# This file is part of bash-utilz.
#
# bash-utilz is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License Foundation version 3 as
# published by the Free Software Foundation.
#
# bash-utilz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with bash-utilz.  If not, see <http://www.gnu.org/licenses/>.
################################################################################

.ONESHELL:

# meta
_name = peval.sh
_version = 0.3.14

# vars
prefix :=
install_sdir := lib/bash-utilz
install_dir_user := $(prefix)$(HOME)/.local/$(install_sdir)
install_dir_sys := $(prefix)/usr/local/$(install_sdir)


define _help_msg :=
Usage:

  make [target]

Targets:

  help          ...guess what ;-)
  sys-install   install all files in $(install_dir_sys)
  user-install  install all files in $(install_dir_user)
  version       update the version string (currently '$(version)') in all files
  test			run unit tests

$(_name) v$(_version)
endef

.PHONY: clean sys-install user-install version test

all: help

_install:
	install -d $(install_dir)
	install -t $(install_dir) peval.sh
	$(info Export 'PEVAL_LIB_PATH=$(install_dir)/peval.sh' for use in your scripts)

user-install:
	$(MAKE) install_dir=$(install_dir_user) _install

sys-install:
	$(MAKE) install_dir=$(install_dir_sys) _install

version:
	sed -ri 's/^(__pvl_version=)(.+)$$/\1$(_version)/' peval.sh

test:
	bats --tap test/*.bats

# the shell no-op silences 'nothing to be done...' messages
help:
	@: $(info $(_help_msg))
