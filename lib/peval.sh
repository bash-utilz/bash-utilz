################################################################################
# peval.sh -- bash library for logging end dry-run command execution
################################################################################
# Copyright 2018 - 2024 Marco Emilio "sphakka" Poleggi
#
# This file is part of bash-utilz.
#
# bash-utilz is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License Foundation version 3 as
# published by the Free Software Foundation.
#
# bash-utilz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with bash-utilz.  If not, see <http://www.gnu.org/licenses/>.
################################################################################
# meta
__pvl_version=0.3.14
################################################################################
shopt -s expand_aliases
set -o nounset -o pipefail -o errtrace

################################################################################
# globals
################################################################################

# caller should rewrite these two
myself='peval.sh'

timestamp=$(date '+%Y-%m-%d_%s')

PVL_DEBUG=${PVL_DEBUG:-}
PVL_TRACE=${PVL_TRACE:-}
PVL_PRETEND=${PVL_PRETEND:-}
PVL_TRAP_ERR=${PVL_TRAP_ERR-1}

# exit statuses in order of severity
PVL_OS_OK=0
PVL_OS_WRN=1
PVL_OS_ERR=2

# booleans
PVL_TRUE=1
PVL_FALSE=
PVL_OS_TRUE=0
PVL_OS_FALSE=1

# Compatibility checks are enforced by default
PVL_SKIP_CHECK=${PVL_SKIP_CHECK:-${PVL_FALSE}}

# Global status
pvl__gstatus=$PVL_OS_OK

# Max number of iterations for (some) loops
PVL_MAX_ITERATIONS=${PVL_MAX_ITERATIONS:-100}

# system root and other canonical paths
PVL_SYS_ROOT=${PVL_SYS_ROOT:-}
PVL_ETC_DIR=${PVL_ETC_DIR:-"${PVL_SYS_ROOT}/etc"}
PVL_RUN_DIR=${PVL_RUN_DIR:-"${PVL_SYS_ROOT}/run"}
PVL_VAR_DIR=${PVL_VAR_DIR:-"${PVL_SYS_ROOT}/var"}

# OS RC service control
PVL_OS_RC=${PVL_OS_RC:-openrc}

PVL_AUTODOC_MARK='@USAGE'

# Hex code for some problematic characters
# single & double quote chars for sed replacements
_dq=$(printf '\u022')
_sq=$(printf '\u027')
PVL_QUOTE_D='\x22'
PVL_QUOTE_S='\x27'
PVL_QUOTES="${PVL_QUOTE_D}${PVL_QUOTE_S}"

# Regex sequence of allowed characters in paths
PVL_PATH_CHARS='/[:alnum:]._~@+-'

declare -A _lib_opts_def=(
    [d]="[l]='debug'   [t]='bool' [d]=''                [v]='' \
         [h]='Enable logging of debug statements'"
    [e]="[l]='etc-dir' [t]='str'  [d]='${PVL_ETC_DIR}'  [v]='(dir)' \
         [h]='Dir where caller'\''s conf files are expected to be found'"
    [h]="[l]='help'    [t]='bool' [d]=''                [v]='' \
         [h]='obvious... and the library *stops* loading when called directly'"
    [m]="[l]='manual'  [t]='bool' [d]=''                [v]='' \
         [h]='show the help message and the manual text'"
    [o]="[l]='os-rc'   [t]='str'  [d]='openrc'          [v]='openrc systemv systemd' \
         [h]='OS service RC style'"
    [p]="[l]='pretend' [t]='bool' [d]=''                [v]='' \
         [h]='Pretend (dry-run) execution of \"sensitive\" commands'"
    [r]="[l]='run-dir' [t]='str'  [d]='${PVL_RUN_DIR}'  [v]='(dir)' \
         [h]='Dir for caller'\''s  runtime files'"
    [t]="[l]='trace'   [t]='bool' [d]=''       [v]='' \
         [h]='Trace execution (`set -x`)'"
)

# dynamically filled by `_pvl__getopt()`
declare -A _lib_usage=()

_lib_help=$(cat <<EOF
                                                     ${myself} v${__pvl_version}
Usage:

  sh $myself -h
  [ENV OPTIONS] source $myself [OPTIONS]

Adjust your env variable PEVAL_LIB_PATH.

In caller:

  peval_lib_path=${PEVAL_LIB_PATH:-/usr/local/share/bash-utilz/lib/peval.sh}
  source \$peval_lib_path || {
      echo "[fatal] unable to load shell library '\${peval_lib_path}'" >&2
      exit -1
  }

  # these are just informational for now...
  declare -A caller_dependencies=(
      [foo]='>=1.2.3'
      [bar]='<=4.5.6'
  )

  # let's introduce ourselves
  declare -A caller_data=(
      [name]='the_caller'
      [version]=1.2.3
      [path]=\$0
      [dependencies]='caller_dependencies'  # intended as nameref
      [usage_msg]="concise blah blah"
      [man_text]="very long and complementary usage text"
      [opt_sep_msg]='My options'    # to distinguish lib's from caller's options
      [short_opts]='a:b'            # getopt...
      [long_opts]='an-opt:,boo-hoo' # ...style
  )

  # prepare a container for CL options and lib's ENV. \$caller_env is passed
  # as a _nameref_ to \`pvl__init()\`
  declare -A caller_env=(
      [cl_opts]=''
      [lib_env]=''
  )

  # Optionally, provide a container of var _names_ for directories that shall
  # be sandboxed when PVL_SYS_ROOT is overridden. After init the original
  # vars will hold the modified paths.
  path_1=/foo/bar
  path_1=/foo/bar
  declare -a caller_sandbox=(
      path_1
      path_2
  )

  # do it
  pvl__init caller_env caller_data || {
      pvl__usage
      exit \$PVL_OS_ERR
  }
  [[ \${caller_env[lib_env]} ]] && eval export \${caller_env[lib_env]}
  [[ \${caller_env[cl_opts]} ]] && eval set -- \${caller_env[cl_opts]}

  # check sandboxing
  my_sandbox=
  if my_sandbox=\$(pvl__is_sandboxed); then
      log_info "Running in sandbox '\${my_sandbox}'"
  fi

  # Our arguments will be instantiated at run time using the
  # following var names.
  declare -a argsn=(
      first_mandatory_arg
      second_optaional_arg_with_default:foo-bar
  )

  # Handle *my* CL options and args. Autodoc tags can be used here. See
  # Section "Autodoc"
  while true; do
      case "\$1" in
        -a|--an-option)  #@USAGE: set @{this} to 'frob' some "bits"
            an_option=\$2
            shift 2
            ;;
         ...
         --)
            pvl__ingest_posargs argsn \$@ || {
                pvl__log_error "Can't ingest my mandatory arguments"
                pvl__usage
                exit \$PVL_OS_ERR
            }
            break
            ;;
        *)
            pvl__log_fatal "[BUG] \$1: unexpected command line option" >&2
            exit \$PVL_OS_ERR
      esac
  done

  # handle sandboxing here because some paths might have been modified by CL options
  # path_1, path_2 are now prefixed with \${my_sandbox}
  pvl__make_sandbox caller_sandbox || pvl__log_fatal "Sandboxing failed."

  pvl__log_info "Hey, I'm running!"
  pvl__eval "command --may-screw things" || {
      log_error "Whoa! Failing command..."
      exit \$PVL_OS_ERR
  }


At command line:

   \$ caller --pretend --debug -a foo...


Options
=======

  -h, --help
    obvious... and the library *stops* loading when called directly

  -m, --manual
    show the help message and the manual text

  -d, --debug
    enable 'log_debug()' statements

  -e, --etc-dir=ETC_DIR
   str (/etc). Dir where caller's conf files are expected to be found

  -o, --os-rc=OS_RC_STYLE
    str (openrc). OS service RC style [openrc | systemv | systemd]

  -p, --pretend
    pretend execution of commands called via 'peval()'

  -r, --run-dir=RUN_DIR
   str (/run). Dir for caller\'s  runtime files

  -t, --trace
    enable tracing (\`set -x\`)

These are getopt-style and provided for use by the *caller* via env setting of
contents of nameref '\${cl_env_out[cl_opts]}' -- see Section "Initialization".
Of course, the caller shouldn't use the _same_ name for any its option.

Any configured system path listed above can be sandboxed by changing the ENV
var \$PVL_SYS_ROOT. See help section "Sandboxing".


Initialization
==============

The caller *must* initialize the lib to enjoy its full features. Because of
the shell's limitations... (to be completed)


Autodoc
=======

Minimal self-documenting features are available to program's option lines.
E.g., the following snippet with a case line (notice the special token
'@{this}'):

  this=foo
  ...
    -a|--opt-a)  #$PVL_AUTODOC_MARK: set \$this to something -- default: @{this}

would be rendered as:

  -a|--opt-a)  #$PVL_AUTODOC_MARK: set \$this to something -- default: foo

Thus, special tokens '@{<var-name>}' are expanded to the corresponding value
of '<var-name>', which MUST be defined, else you'll get an 'unbound variable'
error.

Limitations:

- The autodoc comment for an option must be a single line. Support for
  multiple lines is not planned.


Environment
===========

Switches are def-based str booleans:

''            : false
'whatever'    : true

Command line options bearing the same suffix as name take precedence and are
effective after calling \`pvl__init()\`.

  PVL_DEBUG
    str (undef). Switch on 'log_debug()' calls

  PVL_OS_RC
    str ('openrc'). OS service control style [openrc, systemv, systemd]

  PVL_PRETEND
    str (undef). Pretend (dry-run) execution of commands wrapped in 'peval()'
    calls


Exported symbols
================

Apart from functions, stuff that the caller can make use of. See them all by
calling:

  # ( source /usr/local/share/bash-utilz/lib/peval.sh; env | grep -i pvl_ | sort; )


Sandboxing
==========

Minimal sandboxing with a simple fake root can be achieved by setting the ENV variable

    PVL_SYS_ROOT=/path/to/your/sandbox/

peval.sh will create the sandbox dir at lib init time, i.e., in
\`pvl__init()\`. No chrooting is done; it is up to the caller to provide the
paths to be prefixed with \${PVL_SYS_ROOT} and then to create them.


Development notes
=================

Coding conventions
------------------

Pseudo-namespacing is achieved by prefixing symbols with:

* '__pvl_'  : meta variables
* '_[pvl__]': internal use
* 'pvl__'   : "exported" (not in the sense of env-exported) things

Env-exported variable names:

* 'PVL_*" : R/O (constants)
* 'pvl__*': R/W

TO-DO
-----

* convert help message to POD
* document functions with POD
* switch to log4sh for logging?
* package with bash-utilz, install in /usr/local/share/bash-utilz/lib/
* expose structured CL options to the caller (instead of relying on autodoc
  tags)

                                                ${myself} v${__pvl_version}
EOF
)


# stuff to be provided by the caller. * == mandatory, else empty or default
declare -A _caller_data=(
    [name]='*'
    [version]=''
    [path]='*'
    [dependencies]=''
    [usage_msg]='*'
    [man_text]=''
    [opt_sep_msg]='Options (for this program)'
    [short_opts]=''
    [long_opts]=''
    [conf_file_opt]=''
    [local_conf_file]=''
    [user_conf_file]=''
    [sys_conf_file]=''
)


export \
    PVL_PATH_CHARS \
    PVL_AUTODOC_MARK \
    PVL_DEBUG \
    PVL_ETC_DIR \
    PVL_FALSE \
    PVL_OS_ERR \
    PVL_OS_OK \
    PVL_OS_RC \
    PVL_OS_WRN \
    PVL_PRETEND \
    PVL_RUN_DIR \
    PVL_SKIP_CHECK \
    PVL_SYS_ROOT \
    PVL_TRACE \
    PVL_TRUE \
    pvl__gstatus


################################################################################
################################################################################

# save CL args (C-style) for later
# declare -a args=
args=("$@")

# Lib's CL getopt-style options, initialized later by `_pvl__getopt()`
declare -A _pvl__clopts=(
    [-o]=''
    [-l]=''
)

if [ ${BASH_SOURCE[0]} == $0 ]; then
    # we're called directly. Only allow help and bail out. Options handling
    # for both lib and caller is done in `pvl__init()`
    getopt -o h -l help -n ${myself} -- "${@}" >/dev/null
    echo -e "$_lib_help"
fi


################################################################################
################################################################################

alias ls='ls --color=none'
alias bexp_off='set +o braceexpand'
alias bexp_on='set -o braceexpand'
alias gexp_off='set -o noglob'
alias gexp_on='set +o noglob'
alias exp_off='set -o noglob +o braceexpand'
alias exp_on='set +o noglob -o braceexpand'
# "failglob" causes [sub-]shells to terminate, thus use 'eval' when dealing
# with globbing patterns
alias failglob_on='shopt -s failglob'
alias failglob_off='shopt -u failglob'

# Standard message for a function's missing argument tested by ${n:?...}
function pvl__msg_arg_missing() {
    local caller=${FUNCNAME[1]:-${myself}}

    echo "@function ${caller}(): arg missing: ${*}"
}

alias now='date "+%Y-%m-%d %H:%M"'

# Split strings (all of $@) into characters
function pvl__s2a() {
    local re=${1:?$(pvl__msg_arg_missing 'split pattern regex')}
    shift
    echo "$@" | grep --color=never -Eo $re
}

# Logging goes to STDERR. No level control is implemented, just a debug filter...
alias pvl__log_debug=':' # no-op
[ "$PVL_DEBUG" ] && alias pvl__log_debug='pvl__log debug'
alias pvl__log_fatal='pvl__log fatal'
alias pvl__log_error='pvl__log error'
alias pvl__log_info='pvl__log info'
alias pvl__log_warn='pvl__log warn'
function pvl__log() {
    local tag=${1:-'info'}
    shift 1
    local msg="$*"

    local caller=${FUNCNAME[1]:-${myself}}
    echo -e "[${tag}] ${caller}> ${msg}" >&2

    [[ "${tag}" == 'fatal' ]] && exit $PVL_OS_ERR

    return $PVL_OS_OK
}


# compatibility checks
function _pvl__check_compat() {
    getopt --test >/dev/null
    [[ $? -ne 4 ]] && pvl__log_fatal "Your getopt is too old, sorry!"
}

alias pvl__check_compat='_pvl__check_compat'
[[ $PVL_SKIP_CHECK ]] || {
    alias pvl__check_compat=':' # no-op
}

pvl__check_compat


# Crash landing <https://gist.github.com/akostadinov/33bb2606afe1b334169dfbf202991d36?permalink_comment_id=4962266#gistcomment-4962266>
# Print a backtrace.
# Parameters:
#   $1: int(). Program's status code
function pvl__stack_trace() {
    local -i status_code="${1}"

    trap ERR

    local -a stack=("*** Crash landing with error code '${status_code}': ***")
    local -i stack_size=${#FUNCNAME[@]}
    local -i i
    local indent="  "
    # start with 1 to skip *this* stack function
    for ((i = 1; i < stack_size; i++)); do
        local func="${FUNCNAME[$i]:-(top level)}"
        local -i line="${BASH_LINENO[$(( i - 1 ))]}"
        local src="${BASH_SOURCE[$i]:-(no file)}"
        stack+=("${indent} └ ${src}:${line} (${func})")
        indent="${indent}  "
    done
    (IFS=$'\n'; pvl__log_error "${stack[*]}")

    exit $PVL_OS_ERR
}

[[ "${PVL_TRAP_ERR}" ]] && {
    pvl__log_debug "ERR trap is on"
    trap 'pvl__stack_trace $?' ERR
}


# Print the current sandbox root.
# *Return* $PVL_OS_TRUE is a sandbox is set, $PVL_OS_FALSE otherwise.
function pvl__is_sandboxed() {
    [[ ${PVL_SYS_ROOT} ]] && {
        echo ${PVL_SYS_ROOT}
        return $PVL_OS_TRUE
    }
    return $PVL_OS_FALSE
}


# Parse lib options from global defs $_lib_opts_def and store:
# - getopt-sytle defs in $_pvl_clopts
# - usage message strings in $lib_usage
# For the moment, internal use only.
function _pvl__getopt() {
    local err=0
    local -A odef=() # ([m]='' [o]='' [l]='' [t]='' [d]='' [h]='' [v]='')
    local sopts='' lopts=''
    for opt in "${!_lib_opts_def[@]}"; do
        local sbj='' cmt=''
        eval "odef=([o]=${opt} ${_lib_opts_def[$opt]})" || {
            log_error "${opt}: can't eval in option def"
            ((++err))
            continue
        }

        sbj=$(echo "-${odef[o]}, --${odef[l]}")
        cmt=$(echo "${odef[t]} (${odef[d]}). ${odef[h]}")
        if [[ ${odef[v]} ]]; then
            sopts+="${odef[o]}:"
            lopts+="${odef[l]}:,"
            sbj+='=VALUE'
            cmt+=" [${odef[v]}]"
        else
            sopts+="${odef[o]}"
            lopts+="${odef[l]},"
        fi
        # set global usage msg
        _lib_usage[$opt]=$(printf "  %-24s %s" "${sbj}" "${cmt}")
    done
    _pvl__clopts[-o]="$sopts"
    _pvl__clopts[-l]="$lopts"

    return $err
}

# yes, we do it here!
_pvl__getopt || {
    pvl__log_fatal "Can't parse lib options"
}


# Provide minimal self-documenting via comment tags beginning with
# $PVL_AUTODOC_MARK. Typical use is for CLI options, f.i. (suppose
# PVL_AUTODOC_MARK=@USAGE):
#
#   -a|--an-option)  #@USAGE: set @{this} to 'frob' some "bits"
#
# Minimal var (scalar and array) expansion is supported: the token '@{this}'
# would be expanded to the value of global var '$this', which must be set by
# the caller.
# Lib's options usage are globally stored before by `_pvl__getopt()`.
#
# *Return* $PVL_OS_OK on success, else $PVL_OS_ERR.
# *Echo* the complete usage message.
# *See also* examples in file 'test/usage.bats'.
function pvl__usage() {
    [[ $# -gt 0 ]] && {
        pvl__log_error "This function doesn't take arguments"
        return $PVL_OS_ERR
    }

    local c_name=${_caller_data[name]}
    local c_vers=${_caller_data[version]}
    local c_path=${_caller_data[path]}
    local c_umsg=${_caller_data[usage_msg]}
    local c_ospm=${_caller_data[opt_sep_msg]}

    local _header=$(printf "%80s" "${c_name} v${c_vers}")
    local _footer="${_header}"

    echo -e "\n${_header}"
    echo -e "Usage:\n\n${c_umsg}\n\nOptions (from peval lib v${__pvl_version}):\n"

    # lib's opt usage is already formatted
    for o in "${!_lib_usage[@]}"; do
        echo -e "${_lib_usage[$o]}"
    done

    # process autodoc lines. Get a list of <subject${sep}comment> pairs
    local sep=$'\001'
    local re_filter="^[[:blank:]]*([^#]+)#${PVL_AUTODOC_MARK}:(.+)[[:blank:]]*$"
    local dlines=$(
        sed -nr "/^[[:blank:]]*#/ d; s/${re_filter}/\1${sep}\2/ p" $c_path
    ) || pvl__log_fatal "can't parse autodoc"

    local status=$PVL_OS_OK

    [[ "${dlines}" ]] && {
        echo -e "\n${c_ospm}:\n"

        (
            local -i errs=0
            IFS=$'\n'
            # quote to preserve newlines
            for l in $dlines; do
                pvl__log_debug "line: '${l}'"
                [[ "$l" =~ ^([^[:blank:]]+)[[:blank:]]*${sep}[[:blank:]]*(.+)$ ]]
                # subjet trasformation: '-a|--an-option)' => '-a, --an-option'
                local sbj=${BASH_REMATCH[1]}
                sbj=${sbj//|/, }; sbj=${sbj//\)/}
                # apparently BASH_REMATCH contents stay single-quoted => not
                # expanded, thus we need to sanitize it :-(
                local cmt=$(echo ${BASH_REMATCH[2]} | sed -r "s/[${PVL_QUOTES}]/\\\\&/g")

                # extract option names, if any -- emulate global regex rematch
                # (<https://unix.stackexchange.com/questions/251013/bash-regex-capture-group>)
                local ocmt=$cmt
                re_filter='@\{([][_[:alnum:]]+)\}'
                # if the cmt head trimming fails, the loop might be infinite
                local -i max_iterations=$PVL_MAX_ITERATIONS
                while [[ "$cmt" =~ $re_filter ]]; do
                    local match=${BASH_REMATCH[0]}
                    # matched name must be defined, else you get an unbound
                    # variable error
                    local -n name=${BASH_REMATCH[1]} || {
                        ((++errs));
                        break
                    }
                    # sanitize, else special chars would derail the prefix removal
                    match=$(echo $match | sed -r 's/[][{}]/\\&/g')
                    cmt=${cmt##*${match}}
                    declare val=${name} || {
                        ((++errs));
                        break
                    }
                    ocmt=${ocmt/${match}/${name}}
                    ((--max_iterations))
                    [[ $max_iterations -gt 0 ]] || {
                        pvl__log_error "[BUG] Max iterations (${max_iterations}) exhausted! Bailing out..."
                        ((++errs));
                        break
                    }
                done
                printf "  %-24s %s\n" ${sbj} ${ocmt}
            done
            return $errs
        ) || ((status+=$?))
    }

    echo -e "\n${_footer}"
    return $status
}


# print caller's usage via `pvl__usage` plus the caller's 'man_text'
function pvl__manual() {
    local c_name=${_caller_data[name]}
    local c_vers=${_caller_data[version]}
    local c_mtxt=${_caller_data[man_text]}
    # expected nameref pointing to an associative array
    local -n c_deps=${_caller_data[dependencies]}

    local -A cfiles=(
        [local]=${_caller_data[local_conf_file]}
        [user]=${_caller_data[user_conf_file]}
        [system]=${_caller_data[sys_conf_file]}
    )

    local _footer=$(printf "%80s" "${c_name} v${c_vers}")

    pvl__usage

    echo -e "\n${c_mtxt}"

    cat <<EOF

# Configuration files

This program declares support of the following configuration files:

EOF

    ncf=3
    for scope in 'system' 'user' 'local'; do
        f=${cfiles[$scope]}
        [[ -z $f ]] && {
            f='(none)'
            ((--ncf))
        }
        printf "  %-12s: %s\n" ${scope} ${f}
    done

    # don't bother if no cfiles are declared
    [[ $ncf -gt 0 ]] && cat <<EOF

Any command line (CL) option may appear in any ot the above configuration
files with the convention that dashes (in CL) be converted to
underscores. F.i.:

  --foo-bar=... => foo_bar=...
EOF

    cat <<EOF

# Dependencies

This program declares the following dependencies:

EOF

    if [[ "${c_deps[*]}" ]]; then
        for i in "${!c_deps[@]}"; do
            printf "  %-12s: %s\n" ${i} ${c_deps[$i]}
        done
    else
        echo -e "  (none)"
    fi

    echo -e "\n${_footer}"
}


# Ingest a configuration file wherein only comment lines and basic direct variable
# assignments are allowed --
# <https://unix.stackexchange.com/questions/175648/use-config-file-for-my-shell-script>
# Parameters:
#   $1: string(). Config file path
#   $2: &array(). Caller's defaults output nameref to write in. On return, it
#       will have possibly updated values for the keys matching conf file's variables.
#   $3: bool($PVL_FALSE). Restrict line's allowed right-side characters, thus
#   preventing var interpolation through '$'.
#
# *Raise error* for any invalid lines.
# *Return* $PVL_OS_ERR on failure.
function pvl__ingest_conf_file() {
    local cfile=${1:?$(pvl__msg_arg_missing 'configuration file path')}
    local -n cl_defs=${2:?$(pvl__msg_arg_missing 'defaults output nameref')}
    local strict=${3:-$PVL_FALSE}

    local -i err=0
    local -i cnt=0
    local line
    while read line; do
        ((++cnt))

        [[ "$line" =~ (^[[:blank:]]*#+)|(^[[:blank:]]*$) ]] && continue
        [[ "$line" =~ = ]] || {
            pvl__log_error "${cfile}:${cnt}: invalid line: ${line} -- no equal sign"
            ((++err))
            continue
        }

        # local xline=${line//$/\\$}
        # pvl__log_debug "line: $line, xline: $xline"
        # test by declare (don't quote!), this has only local scope
        declare $line || {
            pvl__log_error "${cfile}:${cnt}: invalid line: ${line} -- fail declare"
            ((++err))
            continue
        };

        # name: remove longest suffix after first =
        local varn=${line%%=*}
        # value: remove shortest prefix before first =
        local varv=${line#*=}

        pvl__log_debug "varn=$varn, varv=$varv"

        [[ -z ${cl_defs[$varn]+x} ]] && {
            pvl__log_error "${cfile}:${cnt}: unexpected var: ${varn} -- not in caller's defaults"
            ((++err))
            continue
        }
        # filter residual potential issues
        forbidden='|*?&'
        [[ $strict ]] && forbidden+='$'
        [[ "${varv}" ]] && [[ "'${varv}'" =~ [${forbidden}] ]] && {
            pvl__log_error "${cfile}:${cnt}: invalid line: ${line} -- fishy chars"
            ((++err))
            continue
        }

        [[ $strict ]] || {
            # re-eval to perform $-interpolation
            eval "varv=${varv}" || {
                pvl__log_error "${cfile}:${cnt}: invalid line: ${line} -- fail eval"
                ((++err))
                continue
            }
        }
        cl_defs[$varn]=$varv
        pvl__log_debug "cl_defs[$varn]: ${cl_defs[$varn]}"
        # clean up
        unset varn varv
    done < $cfile || return $PVL_OS_ERR

    [[ $err -gt 0 ]] && {
        pvl__log_error "${cfile}: malformed configuration file -- ${err} error(s)"
        return $PVL_OS_ERR
    }

    return $PVL_OS_OK
}


# Load caller's configuration from files whose paths are passed via the
# caller's data keys 'sys_conf_file', 'user_conf_file' and 'local_conf_file'
# -- in decreasing order of priority (the following one overrides the
# preceding one). The caller shall call this function before processing its CL
# options. The files, which may not exist (log warning), are processed by
# `pvl__ingest_conf_file()`.
# Parameters:
#   $1: &array(). Caller's defaults output nameref to write in. On return, it
#       will have possibly updated values for the keys matching conf file's variables.
#   $2: bool($PVL_FALSE). Restrict line's allowed right-side characters, thus
#       preventing var interpolation through '$'.
# *Raise error* when file ingestion fails. See `pvl__ingest_conf_file()`.
# *Return* $PVL_OS_ERR on failure.
function pvl__load_caller_conf() {
    # declared without '-n' as we'll pass it downstream
    local cl_ndefs=${1:?$(pvl__msg_arg_missing 'caller defaults output nameref')}
    local strict=${2:-$PVL_FALSE}

    local -A cfiles=(
        [local]=${_caller_data[local_conf_file]}
        [user]=${_caller_data[user_conf_file]}
        [system]=${_caller_data[sys_conf_file]}
    )

    local errs=$PVL_OS_OK
    for scope in 'system' 'user' 'local'; do
        f=${cfiles[$scope]}
        [[ $f ]] && {
            # honor sandboxing
            f=$(realpath "${PVL_SYS_ROOT}/${f}")
            if [[ -r $f ]]; then
                pvl__log_debug "${f}: processing file..."
                pvl__ingest_conf_file $f $cl_ndefs $strict || {
                    pvl__log_error "${f}: can't ingest ${scope} configuration file"
                    ((++errs))
                }
            else
                pvl__log_info "${f}: ignoring ${scope} configuration file -- requested but missing or non readable"
            fi
        }
    done

    return $errs
}

# Merge caller's CL getopt-style options with lib's $_pvl__clopts and set them for
# use. CL args come from global $@. Parameters:
#   $1: &array(). Caller's env output nameref to write in. On return, it will have keys:
#         cl_opts: str(). Command line options as returned by getopts and
#           ready to be set by the caller with
#             `eval set -- ${caller_env[cl_opts]}`
#         lib_env: str(). Updated lib's env vars depending of options ready to
#           be set by the caller with (for now `PVL_DEBUG` and `PVL_PRETEND` only)
#             `eval export ${caller_env[lib_env]}`
#   $2: &array(). Caller's data nameref with at least the mandatory keys
#       listed in lib's `$_caller_data`.
# *Raise error* for any conflicting options between caller and lib.
# *Return* $PVL_OS_ERR on failure.
function pvl__init() {
    local -n cl_env_out=${1:?$(pvl__msg_arg_missing 'caller env output nameref')}
    local -n cl_data=${2:?$(pvl__msg_arg_missing 'caller data nameref')}

    shift 2
    [[ "$*" ]] && {
        pvl__log_error "Too many arguments: $*"
        return $PVL_OS_ERR
    }

    # ensure all caller data symbols are defined
    for _cd in "${!_caller_data[@]}"; do
        if [[ ${_caller_data[$_cd]} == '*' ]]; then
            # mandatory check
            # <https://stackoverflow.com/questions/3601515/how-to-check-if-a-variable-is-set-in-bash>
            #  unset test                     empty test
            [[ -z ${cl_data[$_cd]+x} ]] || [[ -z ${cl_data[$_cd]} ]] && {
                pvl__log_error "'${_cd}': caller data symbol is undefined or empty"
                return $PVL_OS_ERR
            }
            _caller_data[$_cd]=${cl_data[$_cd]}
            continue
        fi
        [[ -z ${cl_data[$_cd]+x} ]] || {
            # override default (even if empty)
            _caller_data[$_cd]=${cl_data[$_cd]}
        }
    done

    # from globals
    local cl_sopts=${_caller_data[short_opts]}
    local cl_lopts=${_caller_data[long_opts]}

    local sopts=''
    local err=0
    for co in $(pvl__s2a '.:?' $cl_sopts); do
        [[ $co == ':' ]] && continue
        local so=${co/:/ }
        [[ ${_pvl__clopts[-o]} =~ $so ]] && {
            pvl__log_error "${so}: caller's short option conflicts with lib's"
            ((++err))
            continue
        }
        sopts+="${co}"
    done
    sopts+="${_pvl__clopts[-o]}"

    local lopts=''
    for co in ${cl_lopts//,/ }; do
        local lo=${co/:/ }
        [[ ${_pvl__clopts[-l]} =~ $lo ]] && {
            pvl__log_error "${lo}: caller's long option conflicts with lib's"
            ((++err))
            continue
        }
        lopts+="${co},"
    done
    [[ $err -gt 0 ]] && {
        pvl__log_error "invalid specification of caller's options"
        return $PVL_OS_ERR
    }
    if [[ $lopts ]]; then
        lopts+="${_pvl__clopts[-l]}"
    else
        lopts=${_pvl__clopts[-l]}
    fi

    local opts_merged=''
    opts_merged=$(getopt -o ${sopts} -l ${lopts} -n ${myself} -- "${args[@]}") || {
        pvl__log_error "can't merge caller's and lib's options"
        return $PVL_OS_ERR
    }

    eval set -- ${opts_merged}

    # process this lib's options
    while [[ $# -gt 0 ]]; do
        case "$1" in
            -d|--debug)
                cl_env_out[lib_env]+=" PVL_DEBUG=1"
                BASH_ALIASES[pvl__log_debug]='pvl__log debug'
                shift
                ;;
            -e|--etc-dir)
                PVL_ETC_DIR=$2
                cl_env_out[lib_env]+=" PVL_ETC_DIR=$PVL_ETC_DIR"
                shift 2
                ;;
            -h|--help)
                pvl__usage
                exit
                ;;
            -m|--manual)
                pvl__manual
                exit
                ;;
            -o|--os-rc)
                PVL_OS_RC=$2
                cl_env_out[lib_env]+=" PVL_OS_RC=$PVL_OS_RC"
                case $PVL_OS_RC in
                    openrc)
                        acmd=_pvl__service_openrc
                        ;;
                    systemv)
                        acmd=_pvl__service_systemv
                        ;;
                    systemd)
                        acmd=_pvl__service_systemd
                        ;;
                    *)
                        pvl__log_error "${PVL_OS_RC}: unsupported OS RC style"
                        return $PVL_OS_ERR
                esac
                BASH_ALIASES[pvl__service]=$acmd
                shift 2
                ;;
            -p|--pretend)
                cl_env_out[lib_env]+=" PVL_PRETEND=1"
                shift
                ;;
            -r|--run-dir)
                PVL_RUN_DIR=$2
                cl_env_out[lib_env]+=" PVL_RUN_DIR=$PVL_RUN_DIR"
                shift 2
                ;;
            -t|--trace)
                cl_env_out[lib_env]+=" PVL_TRACE=1"
                set -x
                shift
                ;;
            *)
                # the remainder for the caller
                cl_env_out[cl_opts]+=" $1"
                shift
        esac
    done
    cl_env_out[cl_opts]=$(pvl__trim "${cl_env_out[cl_opts]}")
}


# Make a fake-root sandbox.
#   $1: &array(). Caller's sandbox nameref with a list of var names for
#       directories to be sandboxed. Each corresponding var is expected, at
#       input, to hold a path to be prefixed with the sandbox path
#       ($PVL_SYS_ROOT); the final value is stored back in the var. These
#       paths are not created here: that's the caller's duty.
# *Return* $PVL_OS_OK, else the number of errors. Log warning if no paths
# are provided.
function pvl__make_sandbox() {
    local -n cl_sandbox=${1:-''}

    [[ ${#cl_sandbox[@]} -gt 0 ]] || {
        pvl__log_warn "No paths provided for sandboxing"
        return $PVL_OS_OK
    }

    local -i errors=0
    local invalid_chars_re="[^${PVL_PATH_CHARS}]"
    if sandbox_root=$(pvl__is_sandboxed); then
        [[ "$sandbox_root" =~ $invalid_chars_re ]] && {
            pvl__log_error "$sandbox_root: path has forbidden characters. Valid ones are: ${PVL_PATH_CHARS}"
            return $PVL_OS_ERR
        }

        mkdir -p ${sandbox_root} || pvl__log_fatal "Can't create the sandbox root."
        pvl__log_debug "${sandbox_root}: sandbox root created."

        for ((i=0 ; i < ${#cl_sandbox[@]}; ++i)); do
            # ! => indirection
            dirn=${cl_sandbox[$i]}
            dir=${!dirn}
            [[ "$dir" =~ $invalid_chars_re ]] && {
                pvl__log_error "${dir}: path has forbidden characters. Valid ones are: ${PVL_PATH_CHARS}"
                ((++errors))
                continue
            }
            sdir=$(echo -n ${sandbox_root}/${dir} | sed -r 's%/+%/%g')
            # side effect is intentional: the caller will get the modified path
            pvl__log_debug "dirn: $dirn => ${dir}"
            # eval "${dirn}"=${sdir}
            declare -g "${dirn}"=${sdir}
        done
        pvl__log_debug "cl_sandbox: ${cl_sandbox[*]}"
    fi
    return $errors
}


# Ingest caller's positional args, handling unexpected ones.
#   $1: &array(). Caller's array nameref with a list of var names, one for
#       each argument, to be filled in. Optional args shall be passed as '<arg>:?'
#   $2: list(). Remaining caller's arg list, usually $@
# *Return* $PVL_OS_ERR on failure.
function pvl__ingest_posargs() {
    local -n cl_argn=${1:?$(pvl__msg_arg_missing "caller's arg names (array nameref)")}
    shift

    for argn in "${cl_argn[@]}"; do
        local def='' sep=''
        [[ "${argn}" =~ ^(.+?)([:?])(.*)$ ]] && {
            # optional arg (has default)
            argn=${BASH_REMATCH[1]}
            sep=${BASH_REMATCH[2]}
            def=${BASH_REMATCH[3]}
        }

        local val=${def}

        # with getops, usually the first arg is '--', so grab the second one
        [[ "${2:-}" ]] && {
            val=${2:-}
            shift
        }

        # get us a global var named as the key
        declare -g "${argn}"=${val}

        pvl__log_debug "argn=${argn}, val=${val}, sep=${sep}"

        [[ "${!argn}" ]] || [[ "$sep" == ':' ]] || {
            pvl__log_error "${argn}: mandatory caller's argument missing."
            return $PVL_OS_ERR
        }
    done

    shift
    [[ $# -gt 0 ]] && {
        pvl__log_error "$*: unexpected caller's arguments."
        return  $PVL_OS_ERR
    }
    return $PVL_OS_OK
}


# pretend executing command $1, unless optional "pretend pass" ($2) is defined
# -- this allows exceptions, f.i. when cmd support natively dry-run... umh
# Parameters:
#   $1: string(). Command to run
#   $2: mixed(). If string(), then it shall be 'catch:<token>' to force
#   returning $PVL_OS_ERR if the command's std+err output matches regex
#   '<token>'. If bool($PVL_FALSE), then it is interpreted as 'ppass' -- **not
#   implemented yet**
# Examples:
#   peval "command with proper exit code"
#   peval "echo 'Got an error but wrongly return 0'" catch:error
# TO-DO
# * support native cmd dry-run... really!
function pvl__eval() {
    local cmd=${1:?$(pvl__msg_arg_missing 'command to run')}
    local ppass_or_catch=${2:-}

    local token='' ppass=''
    [[ "$ppass_or_catch" ]] && {
        token=${ppass_or_catch##catch:}

        [[ "$token" ]] || ppass=$PVL_TRUE
    }

    [[ "$PVL_PRETEND" && ! "$ppass" ]] && {
            # escape bash-sensitive stuff
            cmd=$(echo $cmd | sed -r 's/[<>]/\\&/g')
            # redirect to avoid output mixup...
            cmd="echo '[pretend] $cmd' >&2"
        }

    pvl__log_debug "going to run: '$cmd'"

    if [[ "$token" ]]; then
        local output=$(eval "$cmd" 2>&1)
        local status=$?
        echo $output >&2

        shopt -s nocasematch
        [[ "$output" =~ ${token} && $status -eq 0 ]] && status=$PVL_OS_ERR
        shopt -u nocasematch

        return $status
    fi

    eval "$cmd"
}


# convert exit status code to a human-readable value
function pvl__status2str() {
    local status=${1:?$(pvl__msg_arg_missing 'numeric status')}

    local hstatus
    case $status in
        $PVL_OS_OK) hstatus='OK';;
        $PVL_OS_ERR) hstatus='ERROR';;
        $PVL_OS_WRN) hstatus='WARNING';;
        *)
            log_warn "unknown numeric status: $status -- interpreting as ERROR"
            hstatus='ERROR';;
    esac
    echo $hstatus
}


# Update the global status based on $status. Return the result.
function pvl__update_gstatus() {
    local status=${1:?$(pvl__msg_arg_missing 'numeric status')}

    case $pvl__gstatus in
        $PVL_OS_OK)
            # we were good...
            pvl__gstatus=$status
            ;;
        $PVL_OS_WRN)
            # take the higher
            [ $status -gt $pvl__gstatus ] && pvl__gstatus=$status
            ;;
        *)
            # we were too bad
            ;;
    esac

    return $pvl__gstatus
}

# Handle OS service start, stop, reload, etc. Use peval(), of course
function _pvl__service_openrc() {
    local service=${1:?$(pvl__msg_arg_missing 'service name')}
    local action=${2:?$(pvl__msg_arg_missing 'action')}
    local options=${3:-}

    local cmd="rc-service $options $service $action"
    pvl__eval "$cmd"
}
function _pvl__service_systemv() {
    local service=${1:?$(pvl__msg_arg_missing 'service name')}
    local action=${2:?$(pvl__msg_arg_missing 'action')}
    local options=${3:-}

    local cmd="service $service $action $options"
    pvl__eval "$cmd"
}
function _pvl__service_systemd() {
    local service=${1:?$(pvl__msg_arg_missing 'service name')}
    local action=${2:?$(pvl__msg_arg_missing 'action')}
    local options=${3:-}

    local cmd="systemctl $options $action $service"
    pvl__eval "$cmd"
}
# default
alias pvl__service=_pvl__service_openrc

################################################################################
# Miscellanea

# Expand variables in a text template and echo the result. See
# <https://unix.stackexchange.com/questions/9784/how-can-i-read-line-by-line-from-a-variable-in-bash>. Parameters:
#   $1: string(). Text to process
function pvl__expand_text_tpl() {
    local text=${1:?$(pvl__msg_arg_missing 'template text')}

    local result
    while IFS= read -r line; do
        # quote to preserve all blanks
        line=$(eval "echo \"$line\"")
        result+="${line}\n"
    done < <(printf '%s\n' "$text")

    echo -e "$result"
}


# Emulate sed-matching line by line with bash-native =~ operator. Parameters:
#   $1: string(). Text to process
#   $2: string(). Regex pattern supporting any number of capture groups
#   $3: &array(). Caller's output nameref for an associative array that will
#                 get a 2D-simulated array holding the content of the original
#                 $BASH_REMATCH array for any matching lines:
#                   rematch[i,0] <= whole match at i-th matching line
#                   rematch[i,j] <= j-th captured group at the i-th matching line
# *Return* $PVL_OS_OK or >0 on failure.
# *See also* examples in file 'test/misc.bats'.
# *Caveats*
#   - Because of the implied nameref argument, this function shouldn't be
#     called within a command substitution.
#   - The nameref '_pvl__rematch' can't be used by the caller, else you get a
#     circular reference error.
#   - Global matching is not supported.
function pvl__sed() {
    if [[ $BASH_SUBSHELL -eq 1 ]]; then
        pvl__log_error "This function can't be called within a subshell (BASH_SUBSHELL=${BASH_SUBSHELL})!"
        return $PVL_OS_ERR
    fi

    local text=${1:?$(pvl__msg_arg_missing 'text to process')}
    local regex=${2:?$(pvl__msg_arg_missing 'regex pattern')}
    local -n _pvl__rematch=${3:?$(pvl__msg_arg_missing 'result nameref')}

    local -i i=0 j=0
    local l
    old_IFS=$IFS
    IFS=$'\n';
    for l in $text; do
        if [[ "${l}" =~ ${regex} ]]; then
            (IFS=${old_IFS}; pvl__log_debug "line #${i}: matches: ${BASH_REMATCH[*]}")
            _pvl__rematch[${i},0]=${BASH_REMATCH[0]}
            pvl__log_debug "match[${i},0]: ${_pvl__rematch[${i},0]}"
            for ((j = 1; j < ${#BASH_REMATCH[@]}; j++)); do
                _pvl__rematch[${i},${j}]=${BASH_REMATCH[${j}]}
                pvl__log_debug "match[${i},${j}]: ${_pvl__rematch[${i},${j}]}"
            done
            ((++i))
        fi
    done
    IFS=${old_IFS}

    return $PVL_OS_OK
}


# Print a blinking text.
#   $1: string(). Text to print
#   $2: bool($PVL_FALSE). Wrap tput output with escape sequences
function pvl__tblink() {
    local text=${1:?$(pvl__msg_arg_missing 'text to print')}
    local escape=${3:-$PVL_FALSE}

    local tbln=$(tput blink)
    local trst=$(tput sgr0)

    [[ $escape ]] && {
        tbln="\[${tbln}\]"
        trst="\[${trst}\]"
    }

    printf '%s%s%s' "${tbln}" "${text}" "${trst}"
}


# Print a colored text. Parameters:
#   $1: string(). Text to print
#   $2: uint(). Color code
#   $3: bool($PVL_FALSE). Wrap tput output with escape sequences
function pvl__tcolor() {
    local text=${1:?$(pvl__msg_arg_missing 'text to print')}
    local color=${2:?$(pvl__msg_arg_missing 'color code')}
    local escape=${3:-$PVL_FALSE}

    local tcol=$(tput setaf ${color})
    local trst=$(tput sgr0)

    [[ $escape ]] && {
        tcol="\[${tcol}\]"
        trst="\[${trst}\]"
    }
    printf '%s%s%s' "${tcol}" "${text}" "${trst}"
}


# Trim blanks at the left end of a single-line input text. Parameters:
#   $1: string(). Text to trim
function pvl__ltrim() {
    local text=${1:?$(pvl__msg_arg_missing 'text to trim')}
    echo  ${text##+([[:blank:]])}
}

# Trim blanks at the right end of a single-line input text. Parameters:
#   $1: string(). Text to trim
function pvl__rtrim() {
    local text=${1:?$(pvl__msg_arg_missing 'text to trim')}
    echo ${text%%+([[:blank:]])}
}

# Trim blanks at both ends of a single-line input text. Parameters:
#   $1: string(). Text to trim
function pvl__trim() {
    local text=${1:?$(pvl__msg_arg_missing 'text to trim')}
    text=${text%%+([[:blank:]])}
    echo ${text##+([[:blank:]])}
}


# Print the current sandbox root. Parameters:
#   $1: uint(). PID to check
# *Return* $PVL_OS_TRUE is a sandbox is set, $PVL_OS_FALSE otherwise.
function pvl__is_running() {
    local -i pid=${1:?$(pvl__msg_arg_missing 'PID to check')}

    kill -0 $pid >/dev/null 2>&1 &&
        return $PVL_OS_TRUE

    return $PVL_OS_FALSE
}


# Remove quotes from input text. Quote input to preserve newlines.
#   $@: string(). Text to trim
function pvl__unquote() {
    local text=${@:?$(pvl__msg_arg_missing 'text to unquote')}
    # No need of sed or xchars ;-)
    echo -e "${text//[\"\']/}"
}


# Convert a decimal character into ASCII equivalent
function pvl__char_d2a() {
    local char=${1:?$(pvl__msg_arg_missing 'decimal char to convert')}

    printf "\x$(printf %x $char)"
}


################################################################################
# pre-init. no other functions below this line!

if [ "$PVL_TRACE" ]; then
    set -x
fi

pvl__log_debug "library loaded"
