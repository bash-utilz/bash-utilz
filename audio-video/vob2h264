#!/bin/bash
# <http://en.gentoo-wiki.com/wiki/Ripping_DVD_to_Matroska_and_H.264>

dbg=1

TITLE=${1:?'Need title argument [#1: string]'}
TARGETSIZE=${2:?'Need target size argument (MB) [#2: int]'}

# default values
VOBFILE="${TITLE}.vob"
AUDFILE="${TITLE}.aud"
AVIFILE="${TITLE}.avi"
INFFILE="${TITLE}.inf"
TRACK=1
RIPPATH='/scratch'
AVIPATH=$RIPPATH

echo -e "[INFO] ripping video..."
[ -f ${RIPPATH}/${VOBFILE} ] || \
    mplayer dvd://${TRACK} -dumpstream -dumpfile ${RIPPATH}/${VOBFILE}

echo -e "[INFO] analyzing..."
# dump any useful info in a tmp file (5" scan, no audio/video output)
[ -f ${RIPPATH}/${INFFILE} ] || \
    mplayer -msglevel identify=6 -ss 10 -endpos 5 -vf cropdetect -vo null -ao null \
    ${RIPPATH}/${VOBFILE} > ${RIPPATH}/${INFFILE}

# extract info
CROP=$(cat  ${RIPPATH}/${INFFILE} | sed -nr '/^\[CROP\]/ s/^.+crop=(.+)\)\.$/\1/ p' | sed -nr '$ p')
AID=$(sed -nr 's/^ID_AUDIO_ID=([[:digit:]]+)$/\1/ p' ${RIPPATH}/${INFFILE})
DURATION=$(sed -nr 's/^ID_LENGTH=([[:digit:].]+)$/\1/ p' ${RIPPATH}/${INFFILE})
[ "$CROP" -a "$AID" -a "$DURATION" ] || {
    echo -e "[ERROR] can't extract clip info. Please check info file: ${RIPPATH}/${INFFILE}"
    exit 1
}
DURATION=$((${DURATION%.*}+1)) # round it up

# audio ripping...
echo -e "[INFO] ripping audio..."
[ -f ${RIPPATH}/${AUDFILE} ] || \
    mplayer -aid ${AID} -dumpaudio -dumpfile ${RIPPATH}/${AUDFILE} ${RIPPATH}/${VOBFILE}

BITRATE=$(bitrate -b 256 -o 1.2 -t ${TARGETSIZE} ${DURATION} ${RIPPATH}/${AUDFILE} | \
    sed -nr 's|^Target.+@\s+([[:digit:].]+?)\s+kbit/s$|\1| p')
BITRATE=$((${BITRATE%.*}+1)) # round it up

[ "$dbg" ] && echo "[DBG] CROP=$CROP, AID=$AID, DURATION=$DURATION, BITRATE=$BITRATE"

echo -e "[INFO] encoding -- 1st pass..."
# 'turbo' deprecated, now default
mencoder -really-quiet -nosub -vf "pullup,softskip,crop=${CROP},harddup" \
    -oac lavc -ovc x264 \
    -x264encopts "bitrate=${BITRATE}:subq=5:bframes=3:b_pyramid=normal:weight_b:threads=auto:pass=1" \
    -o /dev/null ${RIPPATH}/${VOBFILE} || {
    echo -e "[ERROR] encoding 1st pass failed :-("
    exit 1;
}

echo -e "[INFO] encoding -- 2nd pass..."
mencoder -really-quiet -nosub -vf "pullup,softskip,crop=${CROP},harddup" \
    -oac lavc -ovc x264 \
    -x264encopts "bitrate=${BITRATE}:subq=5:8x8dct:frameref=2:bframes=3:b_pyramid=normal:weight_b:threads=auto:pass=2" \
    -o ${RIPPATH}/${AVIFILE} ${RIPPATH}/${VOBFILE}
