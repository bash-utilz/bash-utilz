# -*- mode: sh -*-
################################################################################
alias ls='ls -h --color'
alias l='ls'
alias lt='l -t'
alias la='l -a'
alias ll='l -l'
alias llp='ll | more'
alias lld='llp | grep "^d"'
alias lla='l -la'
alias llt='ll -t'
alias llat='lla -t'
alias llap='lla | more'
alias d='l -d */'
alias mv='mv -i'

DOMAIN='<FQDN>'
WEBMASTER="webmaster@${DOMAIN}"

unset PS1
PS1="\[\033[01;36m\]<\u@\h\[\033[01;34m\]::${DOMAIN}:\w>\n\[\033[01;31m\]$ \[\033[00m\]"

PATH="${HOME}/.local/bin${PATH:+:${PATH}}"
# @Plesk
# PATH="/opt/plesk/php/X.Y/bin${PATH:+:${PATH}}"

# @WP -- please ajust. Examples:
HOSTING_ROOT=
#   @Infomaniak: /home/clients/<CLIENT_ID>/sites/<FQDN>/...
#   @Plesk:      /var/www/vhosts/<CLIENT_ID|FQDN>/httpdocs/wp/
WEBROOT_STG=${HOSTING_ROOT}/stage
WEBROOT_PRD=${HOSTING_ROOT}/prod

export PS1 PATH HOSTING_ROOT WEBROOT_STG WEBROOT_PRD DOMAIN WEBMASTER

# @WP
alias wps="wp --path=$WEBROOT_STG"
alias wpp="wp --path=$WEBROOT_PRD"
