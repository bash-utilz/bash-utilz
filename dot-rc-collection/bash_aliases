# -*- mode: shell-script; -*-
################################################################################
# Copyright 2024 Marco Emilio Poleggi
#
# This file is part of bash-utilz.
#
# bash-utilz is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License Foundation version 3 as
# published by the Free Software Foundation.
#
# bash-utilz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with bash-utilz.  If not, see <http://www.gnu.org/licenses/>.
################################################################################
shopt -s expand_aliases

# source some aliases
if [ -e "/etc/shell-aliases" ]; then
    source /etc/shell-aliases
fi

if [ -e "${HOME}/.bash_variables" ]; then
    source ${HOME}/.bash_variables
fi

alias log_error='echo >&2 "[error] ${FUNCNAME}>"'
alias log_info='echo >&2 "[info] ${FUNCNAME}>"'
alias log_warn='echo >&2 "[warn] ${FUNCNAME}>"'

# User specific aliases
alias lnc='ls --color=none'
alias ls='ls -h --color'
alias l='ls'
alias lt='l -t'
alias la='l -a'
alias ll='l -l'
alias llp='ll | more'
alias lld='llp | grep "^d"'
alias lla='l -la'
alias llt='ll -t'
alias llat='lla -t'
alias llap='lla | more'
alias d='l -d */'
alias mv='mv -i'
alias sshc='ssh -c none'
alias eng2ita='wordtrans -d babylon'
alias vi='vim'
# alias inject='eject -t'
alias eject='eject /dev/sr0'
alias creset='clear; reset'
alias cp='cp -i'
alias ff=firefox
alias firefox-noxinput='MOZ_USE_XINPUT2=0 ff'
alias ffnxin='firefox-noxinput 2>/dev/null'
alias fftor='ffnxin -P tor'
alias tb='thunderbird 2>/dev/null'
alias tbnxin='MOZ_USE_XINPUT2=0 tb'
# alias fftb='thunderbird-bin & firefox-bin &'
# alias tbff=fftb
alias suda='sudo '
alias zap=':> '
alias histgrep='history | grep'

function _random_pw {
    local plngth=${1:?'arg #1 missing: password length'}
    LC_ALL=C
    tr -dc 'A-Za-z0-9!"#$%&'\''()*+,-./:;<=>?@[\]^_`{|}~' </dev/urandom | head -c $plngth
    echo
}
alias rpw='_random_pw 13'

# Bad BTRFS support: <https://github.com/andreafrancia/trash-cli/issues/311>
# alias rm='trash'
# alias trash-restore='restore-trash'
# alias undorm='trash-restore'

# dict stuff
alias dicte2e='dict -h dict.org'
alias dicte2f='dict -h dict.uni-leipzig.de -d eng-fra'
alias dicte2i='dict -h dict.uni-leipzig.de -d eng-ita'
alias dicti2e='dict -h dict.uni-leipzig.de -d ita-eng'
# alias dicti2f='dict -h dict.uni-leipzig.de -d ita-fra'
# alias dictf2i='dict -h dict.uni-leipzig.de -d fra-ita'
alias dictf2e='dict -h dict.uni-leipzig.de -d fra-eng'

# find tricks
alias ffN='find -name'
alias ffn='find -iname'
alias fdN='find -type d -name'
alias fdn='find -type d -iname'
function fpfn { path=$1; shift; find $path -iname $@; }
function fpfN { path=$1; shift; find $path -name $@; }
function fpdn { path=$1; shift; find $path -type d -iname $@; }
function fpdN { path=$1; shift; find $path -type d -name $@; }

# @latex tricks
# whenever a double run is needed
function run_twice {
    local cmd=${1:?"don't know what to run! Give me a command name"}
    shift
    eval "$cmd $@ && $cmd $@"
}
alias latex2='run_twice latex'
alias pdflatex2='run_twice pdflatex'

alias makeq='make --quiet'
alias smake=makeq
alias mkprxmyroot='make PREFIX=~/myroot'

# quick TeX artifacts clean-up
function tex_clean_artifacts {
  local dir=${1:-'.'}
  local bnre=${2:-'.*?\.'}

  dir="$dir/"
  dir=$(echo $dir | sed -r 's|/+|/|g')
  local findre="${dir}${bnre}(log|aux|bbl|blg|out|dvi|toc|nav|snm)$"
  find $dir -regextype posix-egrep \
    -regex $findre \
      -print0 | xargs -0 rm -f
}
alias texclean=tex_clean_artifacts
alias txc=tex_clean_artifacts

# get a "clean" file list (rsync-C-style) in a given dir
function clflist {
    local dir=${1:-'.'}

    # need a '/' to get the contents
    rsync -C --list-only  $dir/ | awk '{print $5}' | sed -nr '/^\.$/ !p'
}


# @BT
function obexftp_put {
  local thost=${1:?'missing target host BT MAC'}
  local tdir=${2:?'missing target host directory'}
  local files=${3:?'no file to send!?'}

  obexftp -v -b $thost -c $tdir --put $files
}
function obexftp_push {
  local thost=${1:?'missing target host BT MAC'}
  local pchnl=${2:?'missing push channel'}
  local srcdir=${3:?'missing source directory'}
  local tgtdir=$fp2_BT_tdir

  local obopts="-v --nopath --noconn --uuid none -b $thost -B $pchnl"
  local files=$(clflist ${srcdir})

  # default trgtdir is $fp2_BT_tdir -- don't know if one can specify a
  # different one in push mode...
  ## delete doesn't work!?
  ##   obexftp -v -b $thost --delete $tgtdir/*.txt && \
  # don't know why this exit !=0 even when OK!?
  obexftp $obopts --put $files && \
    echo "[INFO] Trasfer OK. Check your target dir '$fp2_BT_tdir' on device"
}
alias btsync-n95="obexftp_put $n95_BT_mac $n95_BT_tdir"
alias btsync-share-n95="obexftp_put $n95_BT_mac $n95_BT_tdir ${btshare_dir}/\*"
alias btpush-fp2="obexftp_push $fp2_BT_mac 12"
alias btpush-share-fp2="obexftp_push $fp2_BT_mac 12 ${btshare_dir}"
alias btput-share-fp2="obexftp_put $fp2_BT_mac $fp2_BT_tdir ${btshare_dir}/*"

alias srsync="rsync --rsync-path='sudo rsync'"

function _rsync_any {
    local srcdir=${1:?'missing source dir'}
    local dstdir=${2:?'missing destination dir'}
    local xropts=$3
    shift 3

    local opts1="-CrtLh --prune-empty-dirs --modify-window=2 --info=stats2,progress2"

    [ "$RSYNC_DELETE" ] && opts1+=' --delete-during'

    local opts2="-rh --existing --ignore-existing --delete --prune-empty-dirs"

    # remaining ones are extra opts to rsync -- must escape '-' to pass the 1st test...
    if [ "${xropts/-/\\-}" ]; then
       if [ -r "$xropts" ]; then
           opts1+=" --files-from=${xropts} $@"
           opts2+=" $@"
       else
           opts1+=" ${xropts} $@"
           opts2+=" ${xropts} $@"
       fi
    fi

    # first pass to refresh new contents at dest
    echo "[INFO] refreshing $srcdir => $dstdir"
    rsync $opts1 $srcdir $dstdir

    [ "$RSYNC_CLEANUP" ] && {
        # second pass to remove stale contents at dest. Must force '/' on src...
        echo -e "\n[INFO] cleaning up $dstdir"
        rsync $opts2 $srcdir/ $dstdir
    }
}

function _adbsync_any {
    local srcdir=${1:?'missing source dir'}
    local dstdir=${2:?'missing destination dir'}
    local xropts=$3
    shift 3

    local opts1="--no-clobber"

    [ "$RSYNC_DELETE" ] && opts1+=' --delete'

    # remaining ones are extra opts to adbync -- must escape '-' to pass the 1st test...
    [ "${xropts/-/\\-}" ] && opts1+=" ${xropts} $@"

    # first pass to refresh new contents at dest
    echo "[INFO] refreshing $srcdir => $dstdir..."
    adb-sync $opts1 $srcdir $dstdir
}

alias adload-picz-fp2="unset RSYNC_CLEANUP RSYNC_DELETE; _adbsync_any ${fp2_px_sdir}/ ./ -R"

alias rsync-share-fp2="_rsync_any ${btshare_dir}/ shell@mefp2:${fp2_sy_tdir}"
alias rsync-share-rmfp2="_rsync_any ${btshare_dir}/ shell@rmfp2:${fp2_sy_tdir}"
alias rsync-dload-fp2="unset RSYNC_CLEANUP RSYNC_DELETE;\
    _rsync_any ${fp2_dl_sdir}/ shell@mefp2:${mefp2_rsync_trgt}"
alias rsync-music-fp2='_rsync_any ~/music ${fp2_trgt_root}/music/ ~/music/sync.list'
alias rsync-photos-fp2="unset RSYNC_CLEANUP RSYNC_DELETE;\
    _rsync_any shell@mefp2:${fp2_px_sdir}/ ./"
alias rsync-music-pumpkin='_rsync_any ~/music ${pumpkin_trgt_root}/music/ ~/music/sync.list'
alias rsync-share-smyoung="_rsync_any ${btshare_dir}/ smyoung:${smyoung_sy_tdir} --no-times"
alias rsync-dload-smyoung="unset RSYNC_CLEANUP RSYNC_DELETE;\
    _rsync_any ${smyoung_dl_sdir}/ smyoung:${smyoung_rsync_trgt} --no-times"

# gpsbabel
function gpx_to_lmx {
  local ifile=${1:?'missing input file'}
  local bname=$(basename $ifile .gpx)
  local ofile=${2:-"${bname}.lmx"}
  local maxp=${3:-'500'}

  gpsbabel -i gpx -f ${ifile} -x simplify,count=${maxp} -x transform,wpt=trk -c UTF-8 -o lmx,binary=0 -F${ofile}
}
alias gpx2lmx=gpx_to_lmx

alias ganttp='/home/utilz/ganttproject/ganttproject-2.5.5-r1256/ganttproject'

function gdb_get_backtrace() {
    local exe=${1:?'arg #1 missing: path to executable'}
    local core=${2:?'arg #2 missing: path to core dump'}

    gdb ${exe} \
        --core ${core} \
        --batch \
        --quiet \
        -ex "thread apply all bt full" \
        -ex "quit"
}

# iso images
function _mnt_iso {
    local img=${1:?'missing image file'}
    local mpt=${2:-/mnt/iso}

    sudo mount -vo loop $img $mpt
}
function _umnt_iso {
    local mpt=${2:-/mnt/iso}

    sudo umount -v $mpt
}
alias isomount='_mnt_iso'
alias isoumount='_umnt_iso'

# mozilla logging & debugging
alias smimapdbg='nsprlog "imap:5,timestamp" seamonkey'
alias smsmtpdbg='nsprlog "smtp:5,timestamp" seamonkey'
alias smbsmtpdbg='nsprlog "smtp:5,timestamp" seamonkey-bin'
# seamonkey fiddles with LC_TIME if LANG is set...
alias seamonkeyl='export LANG=; seamonkey'
# need a subshell!?
alias smdbg='(ulimit -Sc 2097152 && seamonkey)'

# realtime stuff
alias vnice='ionice -c3 nice -n 15'
alias timeg='/usr/bin/time'

alias xoff='xset dpms force off'

# @libreoffice
alias doc2pdf='libreoffice --headless --convert-to pdf'

# @ImageMagick
alias mgrfy90='mogrify -rotate 90'
alias mgrfy180='mogrify -rotate 180'
alias mgrfy270='mogrify -rotate 270'
alias imgsplith='mkdir -p splits && mogrify -path splits -format png -crop 100%x50% +repage'
alias imgsplitv='mkdir -p splits && mogrify -path splits -format png -crop 50%x100% +repage'

# @produTeX
function _mk_ptex_doc {
    local lang=${1:?'arg #1 missing: language code'}
    local doct=${2:?'arg #2 missing: doc type'}
    local docf=${3:?'arg #3 missing: file path'}
    local outd=$4

    # produTeX doesn't like extension, plus the path must be local to
    # "sources" subdir
    docf=${docf%%.$lang.tex}
    docf=${docf##sources/}

    local opts="PLANG=${lang} DOC_TYPE=${doct} custom_name=${docf}"
    [ "$outd" ] && opts+=" JOB_OUTDIR=${outd}"
    # defs with blanks are better handled explictly
    make DEF_MYSELF='Marco\ Emilio\ Poleggi' $opts custom
}
alias mkcl-en="_mk_ptex_doc en Cover_Letter"
alias mkcv-en="_mk_ptex_doc en CV"
alias mkcl-fr="_mk_ptex_doc fr Lettre_de_Motivation"
alias mkcv-fr="_mk_ptex_doc fr CV"
alias mkrp-it="_mk_ptex_doc it rep"
alias mklt-fr="_mk_ptex_doc fr Lettre"
alias mklt-it="_mk_ptex_doc it Lettera"

### @python
alias venvwrap-init="source virtualenvwrapper.sh"
# @Emacs/RealGUD
alias epc='emacspclient'
alias ipdb-sp='ipython --simple-prompt -i -c "%run -d $@"'

alias ilocate='locate -i'

### @PDF tricks
function _pdfshrink {
    local img_res=${1:?'arg #1 missing: output image resolution'}
    local in_file=${2:?'arg #2 missing: input file'}
    local out_file=$3

    pdfinfo $in_file > /dev/null 2>&1 || {
        log_error "${in_file}: not a PDF file"
        return 1
    }

    [[ "$out_file" ]] || {
        local bname=${in_file%.pdf}
        out_file="${bname}.shrinked.pdf"
    }

    gs -q -dNOPAUSE -dBATCH -dSAFER \
       -dPDFSETTINGS=/ebook \
       -dAutoRotatePages=/None \
       -sDEVICE=pdfwrite \
       -dCompatibilityLevel=1.4 \
       -dPDFSETTINGS=/screen \
       -dEmbedAllFonts=true \
       -dSubsetFonts=true \
       -dCompressFonts=true \
       -dDetectDuplicateImages=true \
       -dConvertCMYKImagesToRGB=true \
       -dColorImageDownsampleType=/Bicubic \
       -dColorImageResolution=${img_res} \
       -dGrayImageDownsampleType=/Bicubic \
       -dGrayImageResolution=${img_res} \
       -dMonoImageDownsampleType=/Bicubic \
       -dMonoImageResolution=${img_res} \
       -sOutputFile=${out_file} \
       ${in_file}
}
alias pdfshrink100='_pdfshrink 100'
alias pdfshrink200='_pdfshrink 200'

function pdfrotate {
    local angle=${1:?"arg #1 missing: rotation angle"}
    shift

    pdfjam --suffix rotated${angle} --angle ${angle} --fitpaper true --rotateoversize true "$@"
}
alias pdf90='pdfrotate 90'
alias pdf180='pdfrotate 180'
alias pdf270='pdfrotate 270'
alias pdfnup='pdfxup'
alias pdfrflw='k2pdfopt -ui- -x -rt 0 -om 5p  -wrap+ -o %s.rflw'

function _pdfkrop {
    local cboxs=${1:?'arg #1 missing: cbox specs [l,t,w,h]'}
    local ifile=${2:?'arg #2 missing: input PDF file name'}
    local kopts=$3

    kopts="-ui- -x -mode crop ${kopts}"
    # k2pdfopt -ui- -x -wrap- -col 1 -vb -2 -w 1t -h 1t -t- -rt 0 -c -f2p -2  -cbox ${cboxs} -om 0 -pad 0 -mc- -n- -o %s.crop $ifile
    k2pdfopt -ui -x -cbox ${cboxs} -o %s.crop $ifile
}
alias pdfkrop='_pdfkrop'
alias pdfcrpscn='mpdfcrop "-20 0 0 -165"'
function _pdf_gs_fix() {
    local in_file=${1:?'arg #1 input PDF file'}

    local bfname=${in_file%.pdf}

    gs -o ${bfname}.fix.pdf -dPDFSETTINGS=/prepress -sDEVICE=pdfwrite ${in_file}
}
alias pdf-fix=_pdf_gs_fix
alias pdf-repair=_pdf_gs_fix


function pdf2paper {
    local psize=${1:?'arg #1 missing: paper size'}
    local ifile=${2:?'arg #2 missing: input PDF file name'}
    local ofile=$3

    [ "$ofile" ] || ofile=${ifile%%.*}-${psize}.pdf

    gs -o $ofile \
        -sDEVICE=pdfwrite \
        -sPAPERSIZE=$psize \
        -dFIXEDMEDIA \
        -dPDFFitPage \
        -dCompatibilityLevel=1.4 \
        $ifile

    # TO-DO: use something like this to recenter a trimmed 555 x 699 pts doc (size diff is 595-555=40 and 842-699=143 divided by 2)
    # gs -o default-a4.pdf -sDEVICE=pdfwrite -g5950x8420 -c "<</Install { 20
    #                70 translate}>> setpagedevice" -f default.pdf
}
alias pdf2a4='pdf2paper a4'
alias pdfjoin='pdfunite'
alias pdfsplit='pdfseparate'

# @media play etc.
alias ffq='ffplay -loglevel quiet'
alias ffq-12dB='ffplay -loglevel quiet -af volume=volume=12dB'
alias ff-rten="ffq ${rturl_en}"
alias ff-rtfr="ffq ${rturl_fr}"
alias ff-rt=ff-rten
alias ff-rainews="ffq ${rainewsurl}"
alias ff-raistoria='ffq mmsh://212.162.68.163/rai_storia'
alias ff-f24="ffq ${f24url}"
alias ff-tv5="ffq ${tv5url}"
# mpv
mpv_btdev='--audio-device=alsa/bluealsa'
mpv_eqdev='--audio-device=alsa/equal'
alias mpb="mpv ${mpv_btdev}"
alias mpe="mpv ${mpv_eqdev}"
alias m-rten="mpv ${rturl_en}"
alias m-rtfr="mpv ${rturl_fr}"
alias m-rt=m-rten
alias m-rainews="mpv ${rainewsurl}"
alias m-raistoria='mpv mmsh://212.162.68.163/rai_storia'
# format is for quality, see <https://github.com/jgreco/mpv-youtube-quality>
alias m-f24en="mpv --ytdl-format=95 ${f24url_en}"
alias m-f24fr="mpv --ytdl-format=95 ${f24url_fr}"
alias m-tv5-av="mpv '${tv5url_v}' --audio-file='${tv5url_a}'"
alias m-tv5="mpv --vid=4 '${tv5url}'"
alias m-arte="mpv ${arteurl}"
alias m-rvostok="mpv ${rvostok_url}"
alias mpvcam='mpv av://v4l2:/dev/video0 --profile=low-latency --untimed'


function _image_cast() {
    local device=${1:?'arg #1 missing: video device'}
    local image=${2:?'arg #2 missing: image to cast'}
    local xfmopts=${3} # extra ffmpeg options

    [[ ${device} ]] || {
        log_error "${device}: video device not found. Did you run '# modprobe v4l2loopback'?"
        return 1
    }
    ffmpeg -re -loop 1 -i ${image} -vf format=yuv420p -f v4l2 ${device} ${xfmopts}
}

function _screen_cast() {
    local device=${1:?'arg #1 missing: video device'}
    local vsize=${2:?'arg #2 missing: video size'}
    local xfmopts=${3} # extra ffmpeg options

    [[ ${device} ]] || {
        log_error "${device}: video device not found. Did you run '# modprobe v4l2loopback'?"
        return 1
    }
    ffmpeg -f x11grab -framerate 25 -video_size ${vsize} -i ${DISPLAY} -f v4l2 ${device} ${xfmopts}
}

v4l2lodev=$(v4l2-ctl --list-devices | sed -nr '/Dummy/{n;p}')
alias img-cast="_image_cast ${v4l2lodev}"
alias screen-cast="_screen_cast ${v4l2lodev}"
alias screen-cast-fs='screen-cast 1920x1080'

alias qrcam='zbarcam --oneshot'
alias qrimg='zbarimg'
alias qrcam-lo="qrcam ${v4l2lodev}"

alias prename=perl-rename
alias pmv=prename


# misc
alias iotop='sudo iotop'
alias nettop='sudo nethogs'
alias wnettop='sudo nethogs wlan0'

# @gnucash
alias gnucashdbg='gnucash --debug --log "gnc=debug"'
alias gnucashfr='LANGUAGE=fr_CH.UTF-8 LANG=fr_CH.UTF-8 gnucash'
alias qifhdr-uc="qifhdr ${qifhdr_uc}"
alias qifnormalize="sed -nr 's/\s+/ /g; s/^(\w)\s*/\1/g; /^\s*$/ !p'"

function _mans () {
    local pages string
    if [[ -n $2 ]]; then
        pages=(${@:2})
        string="$1"
    else
        pages=$1
    fi
    man ${2:+--pager="less -p '$string' -G"} ${pages[@]}
}
alias mans='_mans'


# https://trac.torproject.org/projects/tor/ticket/10607
alias torb='export XDG_DATA_HOME=/tmp/tor/; ./start-tor-browser.desktop'
alias atorb='export XDG_DATA_HOME=/tmp/tor/; apulse ./start-tor-browser.desktop'

# connection to Infomaniak are sensitive to sleep/hibernate...
alias clvps-net-restart="ssh myvps \"ssh clvps 'sudo systemctl restart  netfilter-persistent.service'\""

# WM tricks. Use `xprop | grep "CLASS"` and click window to be
# indentified. Depending on the app, class might not be needed
function _dockify {
    local name=${1:?'arg #1 missing: app window name'}
    local xypos=${2:?'arg #2 missing: X,Y position'}
    local class=$3

    # local appwin=$name
    [ "$class" ] && name+=",${class}"

    props="sticky skip_taskbar skip_pager above save_position"
    for p in $props; do
        wmctrl -xr $name -b add,$p
    done

    # place close to upper/right corner. TO-DO: probe screen size
    wmctrl -xr $name -e "0,${xypos},-1,-1"
}

alias dwmclck='wmclockmon -bw'
alias dwmclock='dwmclck & sleep 1 && _dockify wmclockmon 1820,50'

# session tricks
function _killnrrun {
    local appp=${1:?'arg #1 missing: app process name'}
    local sdly=${2:-1}   #2 sleep delay before restart'}
    local args=$3        #3 app arguments

    pkill $appp
    sleep $sdly
    $appp $args &
}
alias rwmclock='_killnrrun wmclockmon 1 -bw && sleep 1 && _dockify wmclockmon 1800,950'

alias xsessionsave='dbus-send --session --dest=org.xfce.SessionManager --print-reply /org/xfce/SessionManager org.xfce.Session.Manager.Checkpoint string:""'


# dev stuff
function _dlist_changes {
    # set -x
    # list different files between 2 directories with opt diff-patch output
    local cmnd=${1:?'arg #1 missing: command [list,diff]'}
    local srcd=${2:?'arg #2 missing: source directory'}
    local dstd=${3:?'arg #3 missing: destination directory'}

    local dodf=
    case $cmnd in
        list)
            # noop
            ;;
        diff)
            dodf=1
            ;;
        *)
            echo >&2 "[error] ${cmnd}: unsupported command"
            return  1
    esac

    fchngd=$(rsync -Cani $srcd/ $dstd/ | sed -nr 's/^[><].+?\s+(.+)$/\1/ p' | sort) || {
        echo >&2 "[ERROR] can't get list of changed files"
        return 1
    }

    [ "$fchngd" ] || {
        echo >&2 '[info] Nothing changed ^-^'
        return
    }

    if [ "$dodf" ]; then
        # can't diff a list of files... :-(
        for f in $fchngd; do
            diff -u ${srcd}$f ${dstd}$f
            echo
        done
    else
        echo "$fchngd"
    fi
}
alias difflist='_dlist_changes list'
alias diffpatch='_dlist_changes diff'


# @audio stuff
alias oggrip='abcde -G -o vorbis:"-b 192"'
alias opusrip='abcde -G -o opus:"--bitrate 192"'
alias flacrip='abcde -G -1 -o flac:"--best" -a default,cue'

alias moggrec='oggrec 1 8000'

function _audioxtract {
    local in_file=${1:?'arg #1 missing: input file'}
    local out_fmt=$2

    local out_fext=
    local out_file=

    local ffm_opts='-vn -ac 2'
    if [ "$out_fmt" ]; then
        ffm_opts+=" -f ${out_fmt}"
        out_fext=$out_fmt
    else
        # detect the first (?) audio streams
        out_fext=$(ffprobe -show_streams -select_streams a -show_entries 'stream=codec_name' -print_format 'csv' $in_file 2>/dev/null | cut -d, -f2)
        # f*** it: need the real ext file type
        [ $out_fext == 'vorbis' ] && out_fext='ogg'
    fi

    out_file=${in_file%.*}.${out_fext}
    ffmpeg -i $in_file $ffm_opts $out_file
}

alias audioxtract='_audioxtract'

alias tagger=org.nickvision.tagger

alias am=alsamixer
alias amhdmi='am -c0'
alias ampch='am -c1'

# @bluez
alias taplay='xfce4-terminal -T "aplay" -x aplay'
alias ambluez='am -D bluealsa'
alias tambluez='xfce4-terminal -T "AlsaMixer BlueZ" -x /usr/bin/alsamixer -D bluealsa'
alias btd=bluez-test-device
alias btconnect='btd connect'
alias btdisconnect='btd disconnect'
function _btreconnect {
    local btaddr=${1:?'arg #1 missing: BT device address'}

    bluez-test-device disconnect $btaddr && bluez-test-device connect $btaddr
}

alias btre_beoplay="_btreconnect ${btaddr_beoplay}"
alias btre_wmcube="_btreconnect ${btaddr_wmcube}"


# localization
function _localize() {
    local lang=${1:?'arg #1 missing: language code'}
    local appl=${2:?'arg #2 missing: application name (command)'}

    export LANGUAGE=$lang LANG=$lang
    exec $appl
}
alias lc-fr='_localize fr'

# wordpress dev
alias wph='wp --path=./'

# @youtube_dl. Base config from ~/.config/yt-dlp/config
# TO-DO: --postprocessor-args="Metadata:-metadata Genre='...'"
# ytdl=youtube-dl
ytdl=yt-dlp
ytdl_outptrn='%(playlist_title)s/%(playlist_index)s.%(title)s.%(ext)s'
alias ytdl-audio="${ytdl} -x --restrict-filenames"
alias ytdl-opus='ytdl-audio --audio-format=opus'
alias ytdl-vorbis='ytdl-audio --audio-format=vorbis'
alias ytdl-pl-opus="ytdl-opus --output='${ytdl_outptrn}'"
alias ytdl-pl-vorbis="ytdl-vorbis --output='${ytdl_outptrn}'"

alias foliate=/usr/bin/com.github.johnfactotum.Foliate

function _genlop_day {
    local ndays=${1:?'arg #1 missing: number of days'}
    local sdate=${2:?'arg #1 missing: start ISO date'}

    [[ $sdate =~ ^([[:digit:]]{4})-([[:digit:]]{2})-([[:digit:]]{2})$ ]]
    sday=${BASH_REMATCH[3]}
    sday=${sday##0}
    eday=$((sday + ${ndays}))
    edate="${BASH_REMATCH[1]}-${BASH_REMATCH[2]}-${eday}"

    genlop --date ${sdate} --date ${edate}  --list --nocolor | \
        sed -nr 's/^.+?>>> (.+?)$/\1/ p' | sort
}
alias genlop-day='_genlop_day'
alias genlop-1-day='_genlop_day 1'

# SSH tricks
function bassh() {
    local host=${1:?'arg #1 missing: remote host'}
    shift
    local command="$@"
    local usage="bassh REMOTE_HOST COMMAND"

    [ "$command" ] || {
        echo -e >&2 "[error] no command provided\nUsage: ${usage}"
        return 1
    }

    ssh ${host} -t bash -ic "'${command}'"
}

# @HTML tricks
function _xtract_mail_addr {
    local infile=${1:?'arg #1 missing: input HTML file | URL'}
    local outfile=${2-?'addresses.html'}

    if [[ ${infile} =~ ^[[:blank:]]*http[s]*:// ]]; then
        # we got an URL. We want to keep the output
        curl -o ${outfile} ${infile}
        infile=${outfile}
    fi
    sed -ri 's%(^.+?)s*style="font-family\s*:\s*LucidaGrande;\s*font-size:\s*11pt"(.+?)$%\1\2%g; s%^(.+?)width\s*=\s*".+?"(.+?)$%\1\2%g' $infile
    sed -nr 's/^.+?href="?mailto:([^><"]+?)".+$/\1/p' $infile | sort -u
}
function _xtract_mail_subj {
    local infile=${1:?'arg #1 missing: input HTML file | URL'}

    local ccmd="cat ${infile}"
    if [[ ${infile} =~ ^[[:blank:]]*http[s]*:// ]]; then
        # we got an URL
        ccmd="curl ${infile}"
    fi
    ${ccmd} | sed -nr 's%^.+<h1>(.+?)</h1>.+$%\1%p'
}

alias xtract-mail-addr=_xtract_mail_addr

function _tb_compose {
    local frmt=${1:?'arg #1 missing: format (html | text)'}
    local auth=${2:?'arg #2 missing: sender -- "From:" field'}
    local subj=${3:?'arg #3 missing: subject -- "Subject:" field'}
    local body=${4:?'arg #4 missing: body'}
    shift 4
    local rcpt=${@:?'need at least one recipient -- "To:" field'}

    rcpt=$(echo $rcpt | tr -s '[:blank:]\n,' ';')

    # don't know why: if author identity exists, need <>
    thunderbird -compose "format=${frmt},from='<${auth}>',to='${rcpt}',subject='${subj}',body='${body}'"
}

alias tb-newmail=_tb_compose
alias tb-write=tb-newmail
alias tb-write-text='tb-newmail text'
alias tb-write-html='tb-newmail html'

# @CAS-GE utilz
function _casge_course_plist_url() {
    local course_id=${1:?'arg #1 missing: course ID'}

    echo "${casge_www_root}/${casge_www_clstpart_path}?idCourse=${course_id}"
}

function _casge_course_url() {
    local course_id=${1:?'arg #1 missing: course ID'}

    echo "${casge_www_root}/${casge_www_course_path}?idCourse=${course_id}"
}

declare -A casge_email_tpls=(
    [html]=~/docs/jobs/training/CAS/templates/email_course.html
    [text]=~/docs/jobs/training/CAS/templates/email_course.txt
)

function _casge_course_email() {
    local from=${1:?'arg 1 missing: sender -- "From:" field'}
    local mstyle=${2:?'arg #2 missing: e-mail style [html|text]'}
    local course_id=${3:?'arg #3 missing: course ID'}

    local addr_file='addresses.html'
    local body_src=${casge_email_tpls[${mstyle}]}
    local body=''

    if [[ -r ${body_src} ]]; then
        body=$(cat ${body_src})
    else
        log_warn "${body_src}: body source file not readable"
    fi

    declare -A tpl_tags=(
        [course_id]=$course_id
    )
    # expands tags
    for tk in ${!tpl_tags[@]}; do
        tv=${tpl_tags[${tk}]}
        body=${body//@${tk}@/${tv}}
    done

    local subj=$(_xtract_mail_subj $(_casge_course_url ${course_id}))
    local email_addrs=$(_xtract_mail_addr \
                            $(_casge_course_plist_url ${course_id}) ${addr_file} | \
                            sed -r "/${from}/ d")

    declare -A body_addresses=(
        [text]=$(links -dump ${addr_file})
        [html]=$(<${addr_file})
    )
    body+=$(cat <<EOF


----------------------------------------8<--------------------------------------
${body_addresses[${mstyle}]}
---------------------------------------->8--------------------------------------


EOF
         )

    _tb_compose ${mstyle} ${from} "[CAS-GE] course '${subj}'" "${body}" ${email_addrs}
}

my_email=${MY_EMAIL:-'please.set.MY_EMAIL@in.your.ENV'}
alias casge-course-emailh="_casge_course_email ${my_email} html"
alias casge-course-emailt="_casge_course_email ${my_email} text"


# net tricks
alias mypubip='dig +short myip.opendns.com @resolver1.opendns.com'

# @lpr tricks
function _lpr_fake_duplex() {
    local in_file=${1:?'arg 1 missing: file to print'}

    echo >&2 "[1/3] Printing even pages..."
    lpr -o page-set=even ${in_file}

    echo >&2 "[2/3] Printing even pages. Flip (short side) the printed even pages and refill the drawer."
    read -p "Hit any key when ready. "
    echo >&2 "[3/3] printing even pages..."
    lpr -o page-set=odd -o outputorder=reverse ${in_file}
}
alias lpr-fake-duplex=_lpr_fake_duplex

# @proj-mgmt
# defaults in file ${HOME}//.config/proj-mgmt/config
alias proj-md-toc="proj-md ${proj_md_toc_opts}"
alias proj-md-cl-fr="proj-md ${proj_md_cl_fr_opts}"
alias proj-md-toc-cl-fr="proj-md-toc ${proj_md_cl_fr_opts}"

alias lsyncw='lsync-watch -b ~/.bashrc-xps1 .lsync-watch/lsyncd.conf.lua'


# bash tricks
function setprompt {
    # set -x
    ps1=${1:?'arg 1 missing: prompt code'}
    PS1=${ps1}
}
export -f setprompt
alias setps1=setprompt


# @PHP
alias gcomposer='composer global'
alias composerg=gcomposer
alias php-pretty=pretty-php


# @eslint
# So, some veeery well-opinionated dev-folks have decided that you <epic>shall
# no longer use global setings, etc.</epic> Mind that
# '--resolve-plugins-relative-to' has been removed in v 9.x., plus, symlinks
# to config (flat.mjs) won't work. F**k'em!
eslint_config=${XDG_CONFIG_HOME}/eslint/eslint.config.mjs
eslint_mod_dir=${HOME}/.local/lib/npm/eslint/node_modules
function _eslint_prepare() {
    local config=${1:?'arg missing: config file'}
    local mod_dir=${2:?'arg missing: mod dir'}

    # config must be copied, 'cause if symlinked, "import" would look from the
    # original file location :-(
    [[ -f $(basename $config) ]] || {
        unalias cp
        cp $config .
    }
    ln -sf ${mod_dir}
}
alias eslint-prepare="_eslint_prepare ${eslint_config} ${eslint_mod_dir}"
