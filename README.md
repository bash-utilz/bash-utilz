# Bash-utilz #

A collection of utility mostly written in BASH.


## Installation

(FIX ME!)

Only files listed in `MANIFEST` will be installed.


## Directory info ##

`_attic`
: Old stuff mostly obsolete, kept there... you never know.

`audio-video`
: AV utilities.

`btrfs`
: BTRFS cron scripts.

`dot-rc-collection`
: My `.rc` files

`dovecot`
: Bootstrap scripts and sieve utilities for Dovecot.

`git`
: Git custom scripts. Mostly dangerous stuff ;-)

`ldap`
: (L)LDAP user init scripts.

`letsencrypt`
: Certbot wrappers and hooks.

`lib`
: My BASH lib with goodies for arg parsing, dry-run command eval and other magics.

`misc`
: Other un-categorized things. Only `lsync-watch` is really worth of
  consideration.

`roundcube`
: A custom RC plugin installer.

`sysadm`
: Some administration tools.


## License

All programs in this repository, even when not explicitly mentioned, are
released under the terms specified in the `LICENSE` file.
