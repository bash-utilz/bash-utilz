################################################################################
# Dovecot Pigeonhole's filter example with spam and virust test, short version.
#
# <https://doc.dovecot.org/configuration_manual/sieve/examples/#filtering-using-the-spamtest-and-virustest-extensions>
################################################################################
# Copyright 2018-2024 Marco Emilio Poleggi
#
# This file is part of bash-utilz.
#
# bash-utilz is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License Foundation version 3 as
# published by the Free Software Foundation.
#
# bash-utilz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with bash-utilz.  If not, see <http://www.gnu.org/licenses/>.
################################################################################
require [
  "include",
  "spamtestplus",
  "fileinto",
  "relational",
  "comparator-i;ascii-numeric",
  "virustest",
  "imap4flags",
  "regex",
  # "vnd.dovecot.debug",
  "vnd.dovecot.environment",
  "envelope",
  "variables"
];

if envelope :matches "from" "*@*" {
  set "their_domain" "${2}";
  set "from" "${1}@${their_domain}";
}

# rule:[accept-internal--do-not-touch]
# For this to work, make sure you have configured 'sieve_env_our_domain =
# <domain.tld>' in ${dovecot-conf.d}/XX-sieve.conf
# See <https://github.com/dovecot/pigeonhole/blob/main/doc/extensions/vnd.dovecot.environment.txt>
if string :is "${their_domain}" "${env.vnd.dovecot.config.our_domain}" {
  # accept *everything* coming from ourselves -- make sure that your MTA's
  # *_restrictions arec configured to prevent spoofing!
  # debug_log "Message from ourselves ;-)";
  stop;
}

# rule:[SPAM-subj--do-not-touch]
elsif header :regex "subject" ["\\**\s*\\[?SPAM\\]?\s*\\**"] {
  setflag "\\seen";
  fileinto "Junk";
  stop;
}

# rule:[maybe-SPAM--do-not-touch]
if spamtest :value "ge" :comparator "i;ascii-numeric" "5" {
  # spam score range: 1-15 (0 = not ranked)
  fileinto "Junk";
  stop;
}
# rule:[surely-SPAM--do-not-touch]
elsif spamtest :value "gt" :comparator "i;ascii-numeric" :percent "95" {
  discard;
# } elsif virustest :value "ge" :comparator "i;ascii-numeric" "5" {
#   # virus score range: 1-5 (0: not ranked, 1: clean, >=2: infected)
#   discard;
}
# rule:[virus--do-not-touch]
elsif virustest :value "ge" :comparator "i;ascii-numeric" "2" {
  fileinto "Quarantine";
  stop;
}
