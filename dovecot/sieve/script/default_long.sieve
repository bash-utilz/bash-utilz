################################################################################
# Dovecot Pigeonhole's filter example with spam and virust test, long version.
#
# <https://doc.dovecot.org/configuration_manual/sieve/examples/#filtering-using-the-spamtest-and-virustest-extensions>
################################################################################
# Copyright 2018-2024 Marco Emilio Poleggi
#
# This file is part of bash-utilz.
#
# bash-utilz is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License Foundation version 3 as
# published by the Free Software Foundation.
#
# bash-utilz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with bash-utilz.  If not, see <http://www.gnu.org/licenses/>.
################################################################################
require [
         "spamtestplus",
         "fileinto",
         "relational",
         "comparator-i;ascii-numeric",
         "virustest",
         "imap4flags",
         "regex"
];

if header :contains "From" "cinelux.ch" {
  # accept *everything* coming from ourselves -- postfix *_restrictions should
  # prevent spooing
  stop;
} elsif header :regex "subject" ["\\**\s*\\[?SPAM\\]?\s*\\**"] {
  setflag "\\seen";
  fileinto "Junk";
  stop;
}

# if spamtest :value "eq" :comparator "i;ascii-numeric" "0" {
#   # spamtest fails...
#   fileinto "Unclassified";
#   stop;
# }
if spamtest :value "ge" :comparator "i;ascii-numeric" "5" {
  # spam score range: 1-15 (0 = not ranked)
  fileinto "Junk";
  stop;
} elsif spamtest :value "gt" :comparator "i;ascii-numeric" :percent "95" {
  discard;
# In principle, we scan *everything* on receipt
# } elsif virustest :value "eq" :comparator "i;ascii-numeric" "0" {
#   fileinto "Unscanned";
#   stop;
} elsif virustest :value "ge" :comparator "i;ascii-numeric" "2" {
  # virus score range: 1-5 (0 = not ranked)
  fileinto "Quarantine";
  stop;
} # elsif virustest :value "eq" :comparator "i;ascii-numeric" "5" {
#   # Definitely infected
#   discard;
# }
