################################################################################
# Dovecot Pigeonhole's report spam filter example.
#
# <https://doc.dovecot.org/configuration_manual/sieve/examples/#vacation-auto-reply>
################################################################################
# Copyright 2018-2024 Marco Emilio Poleggi
#
# This file is part of bash-utilz.
#
# bash-utilz is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License Foundation version 3 as
# published by the Free Software Foundation.
#
# bash-utilz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with bash-utilz.  If not, see <http://www.gnu.org/licenses/>.
################################################################################
require [
  "include",
  "variables",
  "vacation"
];

# SPAM & viruses handled there. This should disable the "default", if
# configured in Dovecot.
# Better off configuring the global default in Dovecot's 'sieve_before' option
# and mask it (empty 'sieve_default').
#  include :global "default";

# rule:[extract-subject]
if header :matches "Subject" "*" {
  set "subjwas" "${1}";
}
# rule:[vacation]
if header :contains ["accept-language", "content-language"] "fr" {
  vacation
  :days 1
  :subject "Réponse automatique: ${subjwas}"
  "Je suis en vacances :-)

  Cordialement";
} else {
  vacation
  :days 1
  :subject "Out of office auto-reply: ${subjwas}"
  "I'm on vacation :-)

  Cheers";
}
