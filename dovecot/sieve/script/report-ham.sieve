################################################################################
# Dovecot Pigeonhole's report ham filter example.
#
# <https://doc.dovecot.org/configuration_manual/spam_reporting/>
################################################################################
# Copyright 2018-2024 Marco Emilio Poleggi
#
# This file is part of bash-utilz.
#
# bash-utilz is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License Foundation version 3 as
# published by the Free Software Foundation.
#
# bash-utilz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with bash-utilz.  If not, see <http://www.gnu.org/licenses/>.
################################################################################
require ["vnd.dovecot.debug", "vnd.dovecot.pipe", "copy", "imapsieve", "environment", "variables"];

if environment :matches "imap.mailbox" "*" {
  set "mailbox" "${1}";
}

# The same DBs are used for all virtual users. We keep this here just for debugging
if environment :matches "imap.user" "*" {
  set "username" "${1}";
}

if string "${mailbox}" "Trash" {
  stop;
}

debug_log "report-ham: ${mailbox} ${username}";

pipe :copy "sa-learn-sieve" [ "${username}", "ham" ];
