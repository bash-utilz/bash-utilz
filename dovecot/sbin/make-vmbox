#!/usr/bin/env bash
################################################################################
# make-vmbox: make a minimal Dovecot's FS hierarchy of virtual mailboxes for a
# given user. Namespace details are extracted by quering via `doveconf`
# <https://wiki.dovecot.org/Plugins/Virtual>
#
# TO-DO:
# * package with dovecot-utilz?
# * should we really stop/start dovecot? FS is touched, though...
################################################################################
# Copyright 2018-2024 Marco Emilio Poleggi
#
# This file is part of bash-utilz.
#
# bash-utilz is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License Foundation version 3 as
# published by the Free Software Foundation.
#
# bash-utilz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with bash-utilz.  If not, see <http://www.gnu.org/licenses/>.
################################################################################
# meta
__version=0.2.0
declare -A __dependencies=(
    [bash]='>=5.1.16'
    [peval]='>=0.3.7'
    [dovecot]='>=2.3'
)
################################################################################
peval_lib_path=${PEVAL_LIB_PATH:-'~.local/lib/bash-utilz/peval.sh'}
source $peval_lib_path || {
    echo "[fatal] unable to load shell library '${peval_lib_path}'" >&2
    exit -1
}
# think of these as a sort of "import as"
for aname in log_debug log_info log_warn log_error log_fatal; do
    alias ${aname}=pvl__${aname}
done
alias peval=pvl__eval

myself=$(basename $0)
status=0

################################################################################

usage_msg=$(cat <<EOF
  $myself [OPTIONS] USER

where:

  USER is something like "foo@bar.quux". Wildcards are allowed and will cause
       iteration over all the matching users (provided you're using the LDAP
       driver for your userdb)
EOF
)

# options' defaults
declare -A my_opt_defs=(
    [conf_ext]='.conf'
    [mdata_dir]='/etc/dovecot/virtual-mailbox'
    [sieve_gdir]='/var/lib/dovecot/sieve/script'
    [sieve_snames]='vacation'    # comma-separated
    [mdir_perms]='u=rwx,g=rx,o='
)

# let's introduce ourselves
declare -A my_data=(
    [name]=$myself
    [path]=$0
    [version]=$__version
    [dependencies]='__dependencies'     # @nameref
    [usage_msg]="$usage_msg"
    [short_opts]='c:g:M:s:'
    [long_opts]='conf-ext:,,sieve-gdir:,mdata-dir:,sieve-scripts:'
)

# $my_env is passed as nameref to `pvl__init()`
declare -A my_env=(
    [cl_opts]=''
    [lib_env]=''
)
pvl__init my_env my_data || {
    pvl__usage
    exit $PVL_OS_ERR
}
[[ ${my_env[lib_env]} ]] && eval export ${my_env[lib_env]}
[[ ${my_env[cl_opts]} ]] && eval set -- ${my_env[cl_opts]}

# holy laziness! just for backward compatibility ;-)
for opt in ${!my_opt_defs[@]}; do
    val=${my_opt_defs[$opt]}
    declare $opt=$val
done

# Our mandatory arguments will be instantiated at run time
declare -a argsn=(
    user
)

while true; do
    case "$1" in
        -c|--conf-ext)      #@USAGE: str (${my_opt_defs[conf_ext]}). Extension for metadata conf files
            conf_ext=$2
            shift 2
            ;;
        -g|--sieve-gdir)    #@USAGE: str (${my_opt_defs[sieve_gdir]}). Global sieve scripts directory
            sieve_gdir=$2
            shift 2
            ;;
        -M|--mdata-dir)     #@USAGE: str (${my_opt_defs[mdata_dir]}). Metadata directory
            mdata_dir=$2
            shift 2
            ;;
        -s|--sieve-snames)  #@USAGE: str (${my_opt_defs[sieve_snames]}). List of non-default global sieve script names (without extension) to copy to user repository
            sieve_snames=$2
            shift 2
            ;;
        --)
            pvl__ingest_posargs argsn $@ || {
                log_error "Can't ingest my positional arguments"
                pvl__usage
                exit $PVL_OS_ERR
            }
            break
            ;;
        *)
            pvl__log_fatal "[BUG] $1: unexpected command line option" >&2
            exit $PVL_OS_ERR
    esac
done


################################################################################

# Make sure all namespaces use the same sep and layout. We query by user just
# to know the correct mailbox details -- mailbox type and ns layout are the same for
# everyone
function process_user() {
    local user=${1:?'arg #1 missing: user to process'}

    log_info "$user: start processing..."

    # get the needed info for a single user: "maildir:/srv/vmail/cinelux.ch/dummy"
    umail_info=$(doveadm user -f mail $user) || \
        log_fatal "can't lookup user '$user'"

    # extract bits to reconstruct later location paths
    user_n=${user%@*}           # ~ dovecot '%n'
    user_d=${user#*@}           # ~ dovecot '%d'
    umbox_type=${umail_info%:*}
    umbox_path=${umail_info#*:}


    dc_ug=$(doveconf -h mail_gid mail_uid) || \
        log_fatal "can't query conf 'mail_gid', 'mail_uid'"
    dc_ug=${dc_ug/$'\n'/:}

    inbox_ns_sep=$(doveconf -xh namespace/inbox/separator) || \
        log_fatal " can't query conf 'namespace/inbox/separator'"

    new_ns_sep=$inbox_ns_sep
    if [ -z $new_ns_sep ]; then
       # defaults depend on the mailbox type
       case $umbox_type in
           maildir)
               new_ns_sep=.
               ;;
           *)
               # FS layout
               new_ns_sep=/
               ;;
       esac
    fi


    # query virtual namespaces, i.e., those having 'location=virtual:...'. We need
    # the key, as it holds the ns name
    all_ns_locs=$(doveconf -xS namespace | sed -nr '/location=virtual:/ p') || \
        log_fatal "can't query 'namespace' conf"

    [ -z $all_ns_locs ] && {
        log_info "no virtual namespace found. Bye!"
        exit 0
    }


    # ops below *will* modify the file system!
    # pvl__service dovecot stop || \
    #     log_fatal "can't stop dovecot"

    umask 'u=rw,g=r,o='
    # template:
    #   namespace/<name>/location=virtual:<path-with-%-vars>[:LAYOUT=...]
    for nsloc in $all_ns_locs; do
        [[ $nsloc =~ ^namespace/(.+?)/location=virtual:(.+?):LAYOUT=(.+)$ ]] || \
            # no LAYOUT specified. Don't now why, can't manage to use '(:LAYOUT=)?'...
            [[ $nsloc =~ ^namespace/(.+?)/location=virtual:(.+)$ ]]

        name=${BASH_REMATCH[1]}
        path=${BASH_REMATCH[2]}
        type=${BASH_REMATCH[3]}     # 'fs' by default, if empty

        log_info "processing namespace '${name}'..."

        # path may hold %-vars. Only %d and %n are expanded
        path=${path/\%n/$user_n}
        path=${path/\%d/$user_d}
        [[ "$path" =~ % ]] && {
            log_error "[bug] ${path}: not handling further %- expansion in namespace path"
            ((status++))
            continue
        }

        sep=$(doveconf -xh namespace/${name}/separator) || {
            log_error "${name}: can't query 'separator'"
            ((status++))
            continue
        }
        # unset == default
        test "$sep" -a "$sep" != $new_ns_sep && \
            log_warn "${name}: separator mismatch: ${sep} <> ${new_ns_sep} from 'inbox' namespace"

        # get mailboxes
        mboxes=$(doveconf -xS namespace/${name}/mailbox | \
                     sed -nr 's/^.+mailbox=(.+)$/\1/ p') || {
            log_error "${name}: can't query mailboxes"
            ((status++))
            continue
        }

        for mbox in $mboxes; do
            log_info "${name}: processing mailbox '${mbox}'..."

            mbox_path=${path}/${new_ns_sep}${mbox}
            cmd="mkdir -m $mdir_perms -p ${mbox_path}"
            peval "$cmd" || {
                log_error "$name: $mbox: can't create mailbox. Failing command: '$cmd'"
                ((status++))
                continue
            }

            mdata_file=${mdata_dir}/${name}/${mbox}${conf_ext}
            cmd="cp ${mdata_file} ${mbox_path}/dovecot-virtual"
            peval "$cmd" || {
                log_error "$name: $mbox: can't prepare metadata file. Failing command: '$cmd'"
                ((status++))
                continue
            }
        done
    done

    usieve_dir="${umbox_path}/sieve"
    cmd="mkdir -m $mdir_perms -p ${usieve_dir}"
    peval "$cmd" || {
        log_error "$usieve_dir: can't create user sieve dir. Failing command: '$cmd'"
        ((status++))
    }

    if [[ $sieve_snames ]]; then
        sscripts=''
        for s in ${sieve_snames//,/ }; do
            s+='.sieve'
            sscripts+="${s},"
        done
        sscripts=${sscripts%,}
        [[ "$sscripts" =~ , ]] && sscripts="{$sscripts}"

        cmd="cp ${sieve_gdir}/${sscripts} ${usieve_dir}"
        peval "$cmd" || {
            log_error "can't copy sieve scripts. Failing command: '$cmd'"
            ((status++))
        }
    fi

    cmd="chown -R ${dc_ug} ${umbox_path}"
    peval "$cmd" || {
        log_error "$umbox_path: can't fix ownership. Failing command: '$cmd'"
        ((status++))
    }
    cmd="find ${umbox_path} -type d -exec chmod $mdir_perms {} \;"
    peval "$cmd" || {
        log_error "$umbox_path: can't fix permissions. Failing command: '$cmd'"
        ((status++))
    }

} # process_user()

################################################################################
# main
################################################################################
if (( $EUID != 0 )); then
    [[ "$PVL_PRETEND" ]] || \
        log_fatal 'Please run as root'
fi

################################################################################

users=$user

if [[ $user =~ '*' ]]; then
    # query matching users as specified by your dovecot's LDAP 'iterate_attrs'
    # option
    users=$(doveadm user $user) || \
        log_fatal "can't lookup users '$user'"
fi

for user in $users; do
    process_user $user || {
        log_error "${user}: can't process user"
        ((status++))
        continue
    }
done

# pvl__service dovecot start || \
#     log_fatal "can't start dovecot"

exit $status
