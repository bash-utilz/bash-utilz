#!/bin/sh
################################################################################
# quota-warning: a quota warning script for Dovecot
################################################################################
# Copyright 2018-2024 Marco Emilio Poleggi
#
# This file is part of bash-utilz.
#
# bash-utilz is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License Foundation version 3 as
# published by the Free Software Foundation.
#
# bash-utilz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with bash-utilz.  If not, see <http://www.gnu.org/licenses/>.
################################################################################
PERCENT=${1:?'arg#1 missing: percent'}
RCPT=${2:?'arg#2 missing: recipient'}
OPTS=$3

domain=$(cat /etc/mailname || echo "no.domain")
dvclda=/usr/lib/dovecot/dovecot-lda

cat << EOF | $dvclda -d $RCPT -o "plugin/quota=maildir:User quota:noenforcing" $OPTS
From: postmaster@$domain
Subject: mailbox quota warning!

Whoa $RCPT!

Your mailbox is getting short of spaaaace, as it is now

   $PERCENT% full

We'd all feel better after a little clean-up ^_^

Yours faithfully,

   _postmaster_
EOF
