#!/bin/bash
################################################################################
# lfsync: emulates rsync via lftp
################################################################################
# Copyright 2024 Marco Emilio Poleggi
#
# lfsync is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# msievec is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with lfsync.  If not, see <http://www.gnu.org/licenses/>.
################################################################################
__version=0.1.1
declare -A __dependencies=(
    [bash]='>=5.1.16'
    [peval]='>=0.3.5'
    [lftp]='>=4.9.2'
    [sed]='>=4.9'
)
# export __dependencies
################################################################################
peval_lib_path=${PEVAL_LIB_PATH:-'~.local/lib/bash-utilz/peval.sh'}
source $peval_lib_path || {
    echo "[fatal] unable to load shell library '${peval_lib_path}'" >&2
    exit -1
}
# think of these as a sort of "import as"
for aname in log_debug log_info log_warn log_error log_fatal; do
    alias ${aname}=pvl__${aname}
done
alias peval=pvl__eval

myself=$(basename $0)
status=0

# defaults
delete=
justprint=
reverse=
verbose=0
dflt_excl_globs='
.*
.*/
*.{bak,tmp,swp}
*~
'
mirr_opts='
    --continue
    --max-errors=1
    --no-empty-dirs
    --parallel=8
    --loop
'
# --only-newer

declare -a conf_file_opts_mndt=(
    HOST
    LOCAL_DIR
    REMOTE_DIR
    USER
)

declare -a conf_file_opts_optl=(
    EXCL_GLOBS
    INCL_GLOBS
    LOGFILE
    PASS
)


################################################################################

usage_msg=$(cat <<EOT
  $myself [OPTIONS] SOURCE DESTINATION

N.B. In order to emulate rsync, the lftp "mirror" command used here works in
"reverse" (push) mode, that is:

  "lftp mirror --reverse SOURCE DESTINATION" ~= "rsync SOURCE DESTINATION"

Thus, to use the original lftp "pull" mode, you have to specifiy the
"--no-reverse" option.
EOT
)


parse_conf() {
    local conf_file=${1:?'arg#1 missing: path to configuration file'}
    local status=0

    source $conf_file || return 1

    for copt in ${conf_file_opts_mndt[@]}; do
        eval "test \$$copt" || {
            echo "[ERROR] $copt: mandatory option missing" >&2
            let status++;
        }
    done

    return $status
}


################################################################################

# let's introduce ourselves
declare -A caller_data=(
    [name]=$myself
    [version]=$__version
    [path]=$0
    [dependencies]='__dependencies'  # intended as nameref
    [usage_msg]="$usage_msg"
    [short_opts]='c:dhjrv:'
    [long_opts]='conf-file:,delete,help,just-print,reverse,verbose:'
    [local_conf_file]=$local_conf_file
)

# $caller_env is passed as nameref to `pvl__init()`
declare -A caller_env=(
    [cl_opts]=''
    [lib_env]=''
)
pvl__init caller_env caller_data || {
    pvl__usage
    exit $PVL_OS_ERR
}
[[ ${caller_env[lib_env]} ]] && eval export ${caller_env[lib_env]}
[[ ${caller_env[cl_opts]} ]] && eval set -- ${caller_env[cl_opts]}

# Our mandatory arguments will be instantiated at run time
declare -a argsn=(
    target_dir_or_conf_file
)

@@@


while true; do
    case "$1" in
        -c|--conf-file)     #@USAGE: str (). Configuration file
            parse_conf $2 || {
                echo "[FATAL] $2: problem with config file" >&2
                exit 1
            }
            shift 2
            ;;
        -j|--just-print)    #@USAGE: pretend execution (dry-run)
            mirr_opts+=' --just-print'
            justprint=1
            shift
            ;;
        -d|--delete)        #@USAGE: delete those files at the target that don't exist at the source
            mirr_opts+=' --delete'
            delete=1
            shift
            ;;
        -h|--help)          #@USAGE: guess what ;-)
            usage
            exit
            ;;
        -r|--reverse)       #@USAGE: switch target and source (push mode)
            reverse=1
            mirr_opts+=' --reverse'
            shift
            ;;
        -v|--verbose)       #@USAGE: verbosity level [1-3]
            verbose=$2
            [[ $verbose =~ ^[1-3]$ ]] || {
                echo "[error] option 'verbose': expected integer in [1-3] but got '$verbose'" >&2
                exit 1
            }
            mirr_opts+=" --verbose=$verbose"
            shift 2
            ;;
        --)
            shift
            # we're in a loop ;-)
            break
            ;;
        *)
            echo "[BUG] $1: unexpected command line option" >&2
            exit 1
    esac
done

ftpurl="ftp://$USER:$PASS@$HOST"

[ "$justprint" ] && echo "$dry_run_msg" >&2


set -o noglob
for xglob in $INCL_GLOBS; do
    mirr_opts+=" --include-glob='${xglob}'"
done

for xglob in $EXCL_GLOBS $dflt_excl_globs; do
    mirr_opts+=" --exclude-glob='${xglob}'"
done
set +o noglob

mirr_opts=$(echo -e "$mirr_opts" | sed -r 's/^[[:blank:]]*//' | sort -u)
mirr_opts=${mirr_opts//$'\n'/ }

# set ftp:list-options -a;
lftp -c "
    set cmd:fail-exit yes;
    open $ftpurl;
    lcd $LOCAL_DIR;
    cd $REMOTE_DIR;
    mirror $mirr_opts --log=$LOGFILE
"
# lftp -c "
#     set cmd:fail-exit yes;
#     open $ftpurl;
#     lcd $LOCAL_DIR;
#     mirror $mirr_opts --log=$LOGFILE $REMOTE_DIR
# "
lstatus=$?

[ "$justprint" ] && echo "$dry_run_msg" >&2

exit $lstatus
