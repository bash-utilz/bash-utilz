# Sendmail-style
From: ${mail_from}
Subject: Ton compte a été supprimé
Content-Type: text/plain; charset=UTF-8

Salut ${curr_user[givenname]},

Ton compte a été supprimé.

Cordialement,

   Ton Webmestre
