# Sendmail-style
From: ${mail_from}
Subject: Ton compte a été ajouté
Content-Type: text/plain; charset=UTF-8

Salut ${curr_user[givenname]},

Ton compte a été ajouté, Voici tes codes d'accés:

  u: ${curr_user[uid]}
  p: ${curr_user[password]}

Cordialement,

   Ton Webmestre
