#!/usr/bin/env bats
################################################################################
# Copyright 2024 Marco Emilio Poleggi
#
# This file is part of bash-utilz.
#
# bash-utilz is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License Foundation version 3 as
# published by the Free Software Foundation.
#
# bash-utilz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with bash-utilz.  If not, see <http://www.gnu.org/licenses/>.
################################################################################

setup_file() {
    export \
        PVL_DEBUG=1 \
        csv_input_file=test/input.csv
}

setup() {
    bats_require_minimum_version 1.5.0
    load '/usr/share/bats-support/load'
    load '/usr/share/bats-assert/load'

    tmp_csv_input_file=$(mktemp -q)
    tmp_tpl_input_file=$(mktemp -q)
    tmp_tpl_dir=$(mktemp -qd)

    sender="${USER}@${HOSTNAME}"
    my_mail_from='Quux Gee <quux@gee.net>'
}

teardown() {
    rm -rf $tmp_csv_input_file $tmp_tpl_input_file $tmp_tpl_dir
}

@test 'check env vars' {
    run : ${PEVAL_LIB_PATH:?"Need to set PEVAL_LIB_PATH"}
    assert_success
}

# bats test_tags=cli
@test 'command line -- missing 1st arg' {
    run ! ./lldap-user-init

    assert_output --partial "action: mandatory caller's argument missing."
}

# bats test_tags=cli,add
@test 'command line -- missing 2nd arg' {
    run ! ./lldap-user-init add

    assert_output --partial "csv_input_file: mandatory caller's argument missing."
}

# bats test_tags=cli
@test 'command line -- bad 1st arg' {
    run ! ./lldap-user-init whatever1 whatever2

    assert_output --partial "whatever1: unsupported action."
}

# bats test_tags=cli,add
@test 'command line -- non-readable 2nd arg' {
    run ! ./lldap-user-init add non-readable-file

    assert_output --partial "non-readable-file: can't read input CSV file."
}


# bats test_tags=cli,dry,add
@test 'pretend add' {
    # we use an aux line var because in CSV you don't need to escape the $
    csv_l='foo,foo@bar.net,Foo Baz,Bar,Foo Bar,quite$Not!Secret,g1 g2'
    cat >$tmp_csv_input_file <<EOF
uid,mail,givenname,sn,cn,password,groups
${csv_l}
EOF
    run ./lldap-user-init -p add $tmp_csv_input_file
    assert_output --partial '[pretend] lldap-cli user add foo foo@bar.net -f Foo Baz -l Bar -d Foo Bar -p quite\$Not!Secret'
    assert_output --partial '[pretend] lldap-cli user group add foo g1'
    assert_output --partial '[pretend] lldap-cli user group add foo g2'
}

# bats test_tags=cli,dry,del
@test 'pretend del' {
    csv_l='foo,foo@bar.net,Foo Baz,Bar,Foo Bar,quite$Not!Secret,g1 g2'
    cat >$tmp_csv_input_file <<EOF
uid,mail,givenname,sn,cn,password,groups
${csv_l}
EOF
    run ./lldap-user-init -p del $tmp_csv_input_file
    assert_output --partial '[pretend] lldap-cli user del foo'
}

# bats test_tags=cli,dry,del,ignore
@test 'pretend del, ignore' {
    csv_l='foo,foo@bar.net,Foo Baz,Bar,Foo Bar,quite$Not!Secret,g1 g2'
    cat >$tmp_csv_input_file <<EOF
uid,mail,givenname,sn,cn,password,groups
${csv_l}
EOF
    run ./lldap-user-init -pi del $tmp_csv_input_file
    assert_output --partial '[pretend] lldap-cli user del foo'
}

# bats test_tags=cli,dry,add,cn
@test 'pretend add, tmp input, cn' {
    csv_l1='foo,foo@bar.net,Foo Baz,Bar,Foo Bar,quite$Not!Secret,g1 g2'
    cat >$tmp_csv_input_file <<EOF
uid,mail,givenname,sn,cn,password,groups
${csv_l1}
EOF
    run ./lldap-user-init -p add $tmp_csv_input_file
    assert_output --partial '[pretend] lldap-cli user add foo foo@bar.net -f Foo Baz -l Bar -d Foo Bar -p quite\$Not!Secret'
}

# bats test_tags=cli,dry,add,no-cn
@test 'pretend add, tmp input, no-cn' {
    csv_l2='baz,baz@gee.net,Baz,Quux,,$zup3r_zkrt,g1 g2'
    cat >$tmp_csv_input_file <<EOF
uid,mail,givenname,sn,cn,password,groups
${csv_l2}
EOF
    run ./lldap-user-init -p add $tmp_csv_input_file
    assert_output --partial '[pretend] lldap-cli user add baz baz@gee.net -f Baz -l Quux -p \$zup3r_zkrt'
}

# bats test_tags=cli,dry,add,cn,ignore
@test 'pretend add, tmp input, cn, ignore-cn' {
    csv_l1='foo,foo@bar.net,Foo Baz,Bar,Foo Bar,quite$Not!Secret,g1 g2'
    cat >$tmp_csv_input_file <<EOF
uid,mail,givenname,sn,cn,password,groups
${csv_l1}
EOF
    run ./lldap-user-init -p -i add $tmp_csv_input_file
    assert_output --partial '[pretend] lldap-cli user add foo foo@bar.net -f Foo Baz -l Bar -p quite\$Not!Secret -d Foo Baz Bar'
}

# bats test_tags=cli,dry,add,no-cn,ignore
@test 'pretend add, tmp input, no-cn, ignore-cn' {
    csv_l2='baz,baz@gee.net,Baz,Quux,,$zup3r_zkrt,g1 g2'
    cat >$tmp_csv_input_file <<EOF
uid,mail,givenname,sn,cn,password,groups
${csv_l2}
EOF
    run ./lldap-user-init -p -i add $tmp_csv_input_file
    assert_output --partial '[pretend] lldap-cli user add baz baz@gee.net -f Baz -l Quux -p \$zup3r_zkrt -d Baz Quux'
}

# Problematic characters in the password pass in dry mode but break in real mode
# bats test_tags=add,password
@test 'add, tmp input, password with problematic chars' {
    export LLDAP_CLI='echo fake-lldap-cli $@'
    for c in \$ \( \) \; \> \<; do
        pw="pas${c}word"
        csv_l="baz,baz@gee.net,Baz,Quux,,${pw},g1 g2"
        cat >$tmp_csv_input_file <<EOF
uid,mail,givenname,sn,cn,password,groups
${csv_l}
EOF
        echo "# trying: ${pw}" >&3
        # ./lldap-user-init add $tmp_csv_input_file >&3
        # won't fail, so check the output
        run ./lldap-user-init add $tmp_csv_input_file
        regex="fake-lldap-cli .+ -p pas${c}word"
        assert_output --regexp $regex
    done
}

# bats test_tags=add,mail,def-tpl,def-sender
@test 'add + mail, default template, default sender' {
    export LLDAP_CLI='echo fake-lldap-cli $@'
    export MAIL_CLIENT='echo fake-mail-client $@'

     csv_l2='baz,baz@gee.net,Baz,Quux,,$zup3r_zkrt,g1 g2'
    cat >$tmp_csv_input_file <<EOF
uid,mail,givenname,sn,cn,password,groups
${csv_l2}
EOF

    run ./lldap-user-init --send-mail --debug add $tmp_csv_input_file
    assert_output --partial "From: ${sender}"
    assert_output --partial 'Hi Baz'
    assert_output --partial 'Your account has been added'
    assert_output --partial 'u: baz'
    assert_output --partial 'p: $zup3r_zkrt'
}

# bats test_tags=add,mail,def-tpl,mail-from
@test 'add + mail, default template, mail-from' {
    export LLDAP_CLI='echo fake-lldap-cli $@'
    export MAIL_CLIENT='echo fake-mail-client $@'

     csv_l2='baz,baz@gee.net,Baz,Quux,,$zup3r_zkrt,g1 g2'
    cat >$tmp_csv_input_file <<EOF
uid,mail,givenname,sn,cn,password,groups
${csv_l2}
EOF

    run ./lldap-user-init --send-mail --mail-from="'${my_mail_from}'" --debug add $tmp_csv_input_file
    assert_output --partial "From: ${my_mail_from}"
    assert_output --partial 'Hi Baz'
    assert_output --partial 'Your account has been added'
    assert_output --partial 'u: baz'
    assert_output --partial 'p: $zup3r_zkrt'
}

# bats test_tags=del,mail,def-tpl
@test 'del + mail, default template' {
    export LLDAP_CLI='echo fake-lldap-cli $@'
    export MAIL_CLIENT='echo fake-mail-client $@'

     csv_l2='baz,baz@gee.net,Baz,Quux,,$zup3r_zkrt,g1 g2'
    cat >$tmp_csv_input_file <<EOF
uid,mail,givenname,sn,cn,password,groups
${csv_l2}
EOF

    run ./lldap-user-init --send-mail --debug del $tmp_csv_input_file
    assert_output --partial 'Hi Baz'
    assert_output --partial 'Your account has been deleted'
}

# bats test_tags=add,mail,dir-tpl
# The template name depends on the action (add, etc.)
@test 'add + mail, dir template' {
    export LLDAP_CLI='echo fake-lldap-cli $@'
    export MAIL_CLIENT='echo fake-mail-client $@'

     csv_l2='baz,baz@gee.net,Baz,Quux,,$zup3r_zkrt,g1 g2'
    cat >$tmp_csv_input_file <<EOF
uid,mail,givenname,sn,cn,password,groups
${csv_l2}
EOF

    cat >"${tmp_tpl_dir}/add.tpl" <<'EOF'
From: ${mail_from}
Subject: Your account has been frobzed
Content-Type: text/plain; charset=UTF-8

Dear ${curr_user[givenname]},
Your credentials are:

  user: ${curr_user[uid]}
  password: ${curr_user[password]}
EOF

    run ./lldap-user-init --send-mail --debug --mail-template-dir=$tmp_tpl_dir add $tmp_csv_input_file
    assert_output --partial 'Your account has been frobzed'
    assert_output --partial 'Dear Baz'
    assert_output --partial 'user: baz'
    assert_output --partial 'password: $zup3r_zkrt'
}

# bats test_tags=del,mail,dir-tpl
@test 'del + mail, dir template' {
    export LLDAP_CLI='echo fake-lldap-cli $@'
    export MAIL_CLIENT='echo fake-mail-client $@'

     csv_l2='baz,baz@gee.net,Baz,Quux,,$zup3r_zkrt,g1 g2'
    cat >$tmp_csv_input_file <<EOF
uid,mail,givenname,sn,cn,password,groups
${csv_l2}
EOF

    cat >"${tmp_tpl_dir}/del.tpl" <<'EOF'
From: ${mail_from}
Subject: Your account has been defrobzed
Content-Type: text/plain; charset=UTF-8

Dear ${curr_user[givenname]},
EOF

    run ./lldap-user-init --send-mail --debug --mail-template-dir=$tmp_tpl_dir del $tmp_csv_input_file
    assert_output --partial 'Your account has been defrobzed'
    assert_output --partial 'Dear Baz'
}

# bats test_tags=add,mail,custom-tpl
# The template is unrelated to the action (add, etc.)
@test 'add + mail, custom template' {
    export LLDAP_CLI='echo fake-lldap-cli $@'
    export MAIL_CLIENT='echo fake-mail-client $@'

     csv_l2='baz,baz@gee.net,Baz,Quux,,$zup3r_zkrt,g1 g2'
    cat >$tmp_csv_input_file <<EOF
uid,mail,givenname,sn,cn,password,groups
${csv_l2}
EOF

    cat >$tmp_tpl_input_file <<'EOF'
From: ${mail_from}
Subject: Your account has been frobzed
Content-Type: text/plain; charset=UTF-8

Dear ${curr_user[givenname]},
Your credentials are:

  user: ${curr_user[uid]}
  password: ${curr_user[password]}
EOF

    run ./lldap-user-init --send-mail --debug --mail-template=$tmp_tpl_input_file add $tmp_csv_input_file
    assert_output --partial 'Your account has been frobzed'
    assert_output --partial 'Dear Baz'
    assert_output --partial 'user: baz'
    assert_output --partial 'password: $zup3r_zkrt'
}
