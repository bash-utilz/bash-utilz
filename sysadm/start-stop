#!/usr/bin/env sh
################################################################################
# start-stop: a prototype micro service management script for limited
# virtualized instances such as shared hosting VMs, containers, etc.
################################################################################
# Motivation: need for managing programs running as pseudo-services on shared
# hosting VM instances.
#
# Warning! This is still work-in-progress *beta stage*. Use with caution.
################################################################################
# Copyright 2024 Marco Emilio Poleggi
#
# This file is part of bash-utilz.
#
# bash-utilz is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License Foundation version 3 as
# published by the Free Software Foundation.
#
# bash-utilz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with bash-utilz.  If not, see <http://www.gnu.org/licenses/>.
################################################################################
# To-do:
# - add a configuration file
# - support explicit service's stop command
# - support per-service sleep time spec
# - convert manual from MD -> roff man pages with ronn
#   <https://github.com/apjanke/ronn-ng?tab=readme-ov-file> or pandoc
################################################################################
# Bugs:
# - possibly not fully POSIX-compliant
################################################################################
export POSIXLY_CORRECT=1
set -u

# meta
__version=0.2.1
__dependencies=$(cat <<EOF
foobar: >=1.2.3 ;-) Please FIX-ME
EOF
)
################################################################################
# Unknown to POSIX...
# set -o posix pipefail
# shopt -s expand_aliases

debug=''
# STARTSTOP_TRACE=${STARTSTOP_TRACE-}
[ "${STARTSTOP_DEBUG-}" ] && debug=1
[ "${STARTSTOP_TRACE-}" ] && set -x

# exit statuses
STATUS_ERR=1
STATUS_NIY=127 # not implemented yet
STATUS_OK=0

# booleans
FALSE=0
TRUE=1

# service conditions
SRV_CRASHED=2   # not running but PID file exists
SRV_STARTED=0   # (supposedly) running, PID file exists
SRV_STOPPED=1   # (supposedly) not running, PID file doesn't exist

_status=$STATUS_OK

myself=$(basename $0)

user_home=${HOME-}
# default option values
def_sleep_time=${STARTSTOP_SLEEP_TIME:-1}
def_system_root_d=${STARTSTOP_SYSTEM_ROOT:-"${user_home}/.local"}
# these are subdirs
def_system_etc_sd="etc/${myself}"
def_system_run_sd="var/run/${myself}"
def_system_log_sd="var/log/${myself}"


# service specs. Mandatory ones are *-prefixed
spec_names="*name description *path start stop restart reload background cdhome"
srv_name=''
srv_path=''
srv_background=''   # == false
srv_cdhome=''       # == false
srv_start=''
srv_stop=''
srv_restart=''
srv_reload=''

# option validation: '<BRE>==<description>'
opt_re_digit='^[[:digit:]]\+$==must be an integer'
opt_re_path='^[[:alnum:]/._-]\+$==must be a POSIX file path'

try_msg="Try '${myself} --help' for more information."

help_msg=$(cat <<EOF
Usage: ${myself} [OPTION...] SERVICE {status|start|stop|restart|zap}

  -h, --help          show this message
  -m, --manual        show the manual page
  -r, --system-root=DIRECTORY[=${def_system_root_d}]
                      use this directory as system root, which must
                      exist. Tilde expansion is not suported
  -s, --sleeptime=INTEGER[=${def_sleep_time}]
                      sleep time (seconds) between operations sensitive to
                      possible race conditions

${myself} v${__version}
EOF
)

# This is markdown. TO-DO: generate roff man pages with ronn.
manual_msg=$(cat <<EOF
# ${myself}(1) -- a micro  service manager standalone script | Version ${__version}

## Synopsis

    ${myself} [OPTION...] SERVICE {status|start|stop|restart|zap}

## Description

${myself} is a (hopefully) POSIX-compliant SysV-inspired service-management
shell script tailored to resource-constrained virtualized server environments,
such as shared hosting VMs, containers, etc. It is totally standalone, has no
shell dependencies and uses standard utlity commands shipped with most Unices
and GNU/Linuces.

### Capabilities

${myself} can start, stop and (partially) track a running program.


### Limitations

${myself} cannot automatically manage a service across reboots or run-level
changes because it has no init-like mechanism. Indeed, in a shared hosting
envrionment, the init subsystem (and cron too) is usually not exposed to the
users; thus, en external check-and-do mechanism has to be employed, e.g., some
sort of web-cron subsystem.

${myself} is not supposed to replace full-fledged RC systems and will never
attempt to do so. Hence, its file system structure, though canonical, is
expected to be rooted in the user's home directory -- see option
*--system-root* . Also, it should not run as a privileged user.


## Actions

The supported *actions* (a.k.a. commands -- the 2^nd parameter) are:

*restart*
: Either call service's own spec action or do a *stop* followed by a
  *start*.

*start*

: On success, a PID file
  (*${def_system_root_d}/${def_system_run_sd}/var/run/\${service}.pid*) is
  written and the service's output is redirected to a log file
  ((*${def_system_root_d}/${def_system_run_sd}/var/log/\${service}.log*)). The
  service is sent to background.

*status*
: Report about the service status: running (show PID), stopped or crashed.

*stop*
: Terminate the service by either calling its spec command or by explicit
  kill. The process is looked up by its recorded PID. The PID file is removed
  on success or if the service wasn't running.

*zap*
: Terminate the service by explicit kill without resorting to the PID
  file. The PID file is *always* removed. **Use with caution.**


## Options

*-h, --help*
: show the usage message

*-m, --manual*
:  show the manual message

*-r, --system-root=DIRECTORY[=${def_system_root_d}]*
: use this directory as system root which must exist. Tilde expansion is not
  supported. **Warning! DO NOT SET this to '/'!**

*-s, --sleeptime=INTEGER[=${def_sleep_time}]*
: sleep time (seconds) between operations sensitive to possible race
conditions


## Services

A service is any installed program that can be started from the command
line. It does not need to be a real "daemon", as ${myself} can take care of
running it in the background.

In order to be handled by ${myself}, a service has to be specified in a
single, er... *[iu]nit-like* file. We call it the service's
[*spec*](#spec-files) file.


### File system structure

${myself} expects and stores all files in the file system under the directory
spcified by the option *--system-root* (default *${def_system_root_d}*). Its
structure is quite canonical:


    {system-root}
    ├── etc
    │   └── ${myself}
    └── var
        ├── log
        │   └── ${myself}
        └── run
            └── ${myself}


By default:

- Specs file are looked up in *${def_system_root_d}/${def_system_etc_sd}/*.
- PID files are stored in *${def_system_root_d}/${def_system_run_sd}/*.
- Log files are stored in *${def_system_root_d}/${def_system_log_sd}/*.


### Spec files

Spec files are simple shell-like flat assignments, one per line. A full spec
file might be (**Warning:** NIY means that the feature is not implemented
yet):

    # Hash-prefixed lines are skipped ;-)
    name=my-service
    description="My Service"
    path=/to/the/service/executable
    start=run
    cdhome=yes
    #stop=''        # NIY
    # restart=''    # NIY
    # reload=''     # NIY
    # background='' # NIY

Apart from the evident meaning of specs *name* and *description*, here are the
other supported ones:

*path=FILEPATH[='']*
: Full (absolute) path to the service executable.

*start=STRING[='']*
: The service (sub-)command to start it, if any.

*cdhome=PSEUDOBOOL[='']*
: Optional flag (set to whatever to enable) to cd to the service's "home",
  i.e., the directory that contains the service executable. If, e.g.,
  *path='/to/the/service/executable'*, the home is '/to/the/service'. **Tip:**
  set this for services that need modules to be loaded from their home.
EOF
)

################################################################################
# functions
################################################################################
log_any() {
    tag=${1:?'arg #1 missing: priority tag'}
    shift

    echo >&2 "$(date +'[%F %T]') [${tag}] $*"
}

log_fatal() {
    log_any 'error' "$*"

    _status=$STATUS_ERR
    exit $_status
}

log_bug() {
    log_any 'error' "[BUG] $*: feature not implemented yet, sorry."

    _status=$STATUS_NIY
    exit $_status
}


alias log_debug=:
[ "$debug" ] && alias log_debug='log_any debug'
alias log_die='log_fatal'
alias log_error='log_any error'
alias log_info='log_any info'
alias log_warn='log_any warning'

# say_exit: print an optional header error message than the main message and exit
# with status code.
# *Parameters*
#   $1: string(). Main message.
#   $2: integer(). Status code to exit with.
#   $3: string(). Optional header message.
say_exit() {
    main_msg=${1:?'arg #1 missing: main message'}
    status=${2:?'arg #2 missing: status code'}
    header=${3-}

    [ -n "$header" ] && log_error "${header}"

    echo >&2 "$main_msg"

    # set global for correct exit trapping
    _status=$status
    exit $status
}

# say_help: print an optional diagnostic message than the 'help' text and exit
# OK.
alias say_help='say_exit "${help_msg}" ${STATUS_OK}'

# try_help: print an optional diagnostic message than the 'try' text and exit
# KO.
alias say_try='say_exit "${try_msg}" ${STATUS_ERR}'

alias manual='echo >&2 "$manual_msg"; _status=$STATUS_OK exit $_status'

# check_optarg: check that opt has a valid optarg.
# *Parameter:*
#   $1: string(). Option's name
#   $2: string(). Validation regex (BRE) + description
#   $3: string(). Optional option's argument
check_optarg() {
    opt=${1:?'arg #1 missing: option name'}
    bre_descr=${2:?'arg #2 missing: validation regex'}
    optarg=${3}

    opt_prfx='-'
    [ 1 -lt $(expr "$opt" : '.\+') ] && opt_prfx='--'

    header="${myself}: option '${opt_prfx}${opt}'"

    [ "$optarg" ] ||
        # msg consistent with common tools (no leading -)
        say_try "${header} requires an argument"

    bre=${bre_descr%%==*}
    descr=${bre_descr#*==}

    expr "$optarg" : $bre >/dev/null ||
        say_try "${header} ${descr} -- doesn't match '${bre}'"

    return $STATUS_OK
}


# psgrep: echo the PID of $process_path, if any, looking it up as a (full)
# path via 'ps'.
# *Parameters:*
#   $1: string(). Process path
# *Return* 0, if PID was found, >0 otherwise or on error.
psgrep() {
    process_path=${1:?'arg #1 missing: process path'}

    ourselves=$(realpath $0)

    pid=$(ps -ef | sed -nr "
                 \%${ourselves}%d; # discard the caller process
                 \%sed%d;          # discard any sed filter process... mmh, fragile :-/
                 \%${process_path}%p
             " | tr -s ' ' | cut -d' ' -f2) || return 1

    [ "$pid" ] && {
        echo $pid
        return 0
    }
    return 1
}

# get_srv_specs: retrieve service specs from file and instantiates global vars
# with the corresponding values. Valid spec names come from global
# '$spec_names'. E.g.: spec 'foo=bar' (from the specs file) gets instantiated
# as global var 'srv_foo=bar'.
# *Parameters.*
#   $1: string(). Service name.
#   $2: string(). System etc path.
# *Return* 0 if all specs are valid, >0 otherwise or on error.
get_srv_specs() {
    srvname=${1:?'arg #1 missing: service name'}
    sysetcd=${2:?'arg #2 missing: system etc path'}

    forbidden_chars='~|=(){}$;:<>'
    specs_file=${sysetcd}/${srvname}
    [ -r $specs_file ] || {
        log_error "specs file not found/readable: ${specs_file}"
        return 1
    }

    status=0

    mandatory_specs=''
    for spec in $spec_names; do
        mspec=''
        # ^-anchoring should be implicit with POSIX... you never know.
        mspec=$(expr "$spec" : '^*\(.*\)$') &&
            mandatory_specs="${mandatory_specs} ${mspec}"
    done
    log_debug "Mandatory specs: ${mandatory_specs}"

    line=''
    while read -r line; do
        # expected structure:
        #   spec_name=spec_value[=possibly-another-value]

        # skip comments
        expr "$line" : '[[:blank:]]*#' >/dev/null && continue

        s_name=''
        s_value=''
        s_name=${line%%=*}

        echo "${spec_names}" | grep -qw "\b${s_name}\b" || {
            log_error "${s_name}: invalid spec name."
            status=$((status+1))
            continue
        }

        # shortest -- match "name=val=other-val"
        s_value=${line#*=}
        echo "${s_value}" | grep -qE "[${forbidden_chars}]" && {
            log_error "${s_name}: forbidden character(s) found in spec value"
            status=$((status+1))
            continue
        }

        # assign global
        log_debug "srv_${s_name}=${s_value}"
        # single-quoting of right-side only is essential to avoid code
        # injection attacks
        eval srv_${s_name}='${s_value}'

        # remove from mandatory specs
        mandatory_specs=$(echo "$mandatory_specs" | sed -E "s/^(.*?)${s_name}(.*?)$/\1\2/")

    done < $specs_file || return 1

    mandatory_specs=$(echo ${mandatory_specs} | sed -E 's/^[[:space:]]+//; s/[[:space:]]+$//')
    log_debug "Mandatory specs: '${mandatory_specs}'"
    [ "${mandatory_specs}" ] && {
        log_error "Missing mandatory specs: ${mandatory_specs}"
        status=$((status+1))
    }

    return $status
}

# is_running: check if a service process is running by looking up its PID via
# 'ps'.
# *Parameters*:
#   $1: string(). Service name.
#   $2: string(). PID file path.
# *Return* SRV_{CRASHED | STARTED | STOPPED}.
is_running() {
    srvname=${1:?'arg #1 missing: service name'}
    pidfile=${2:?'arg #2 missing: PID file path'}

    rpid=$(cat $srv_pidfile 2>/dev/null) || {
        log_debug "${srvname}: PID file not found/readable."
        return $SRV_STOPPED
    }

    ps -q $rpid >/dev/null && {
        echo $rpid
        return $SRV_STARTED
    }

    return $SRV_CRASHED
}

# start_srv: start a service by calling its path (possibly) with a 'start'
# action. If the process starts, its PID is recorded in a file and output is
# redirected to a log file.
# *Parameters*
#   $1: string(). Service name.
#   $2: string(). Service path.
#   $3: string(). PID file path.
#   $4: string(). Log file path.
#   $5: string(). Optional service start command.
# *Return* 0 if the process started, 0 otherwise or on error.
start_srv() {
    srvname=${1:?'arg #1 missing: service name'}
    srvpath=${2:?'arg #2 missing: service path'}
    pidfile=${3:?'arg #3 missing: service PID file path'}
    logfile=${4:?'arg #4 missing: service log file path'}
    srvstart=${5:-''}
    cdhome=${6:-''}

    rpid=''
    srvhome=''

    rpid=$(is_running $srvname $pidfile) && {
        log_error "${srvname}: already started (PID=${rpid})"
        return 1
    }

    # beware of shorthand redirection '&>': not POSIX-compliant
    cmd="${srvpath} ${srvstart} >>$logfile 2>&1"

    [ "$cdhome" ] && {
        # longest '*/' prefix
        srvhome=${srvpath%/*}
        cd $srvhome || {
            log_error "${srvname}: can't cd to home: ${srvhome}"
            return 1
        }
    }
    [ "$srv_background" ] || cmd="nohup ${cmd} &"
    # otherwise the service process is supposed to go background by itself.
    log_debug "going to run: ${cmd}"
    eval "$cmd" ||
        # KO: eval/nohup
        return 1

    pid=$!

    sleep ${sleep_time}

    # useless to check/report with 'jobs' as this is not an interactive shell
    ps -q $pid >/dev/null ||
        # KO: process
        return 1

    # OK
    echo $pid > $srv_pidfile
    log_info "${srvname}: started. PID: $(cat ${pidfile})"
    log_info "${srvname}: logging to '${logfile}'"

    return 0
}

# stop_srv: look up a service process by recorded PID (using 'is_running()')
# and kill it. The PID file is removed on success or if the service wasn't
# running.
# *Parameters*
#   $1: string(). Service name.
#   $2: string(). PID file path.
#   $3: string(). Optional service stop command. NOT IMPLEMENTED YET!
#   $4: string(). Optional service path -- must be supplied if $3 is also
#                 set. NOT IMPLEMENTED YET!
# *Return* 0 if the process started, 0 otherwise or on error.
stop_srv() {
    srvname=${1:?'arg #1 missing: service name'}
    pidfile=${2:?'arg #2 missing: PID file path'}

    rpid=''
    status=0

    rpid=$(is_running $srvname $pidfile) || {
        log_error "${srvname}: not running."
        rm -f $pidfile
        return 1
    }

    kill $rpid
    status=$?
    [ $status -eq 0 ] && {
        sleep ${sleep_time}
        rm -f $pidfile
        log_info "${srvname}: stopped."
    }

    return $status
}


# zap_srv: look up a service process by path via 'psgrep()' (without using the
# PID file) and kill it. The PID file is *always* removed.
# wasn't running.
# *Caveats* No guarantee of correct result.
# *Parameters*
#   $1: string(). Service name.
#   $2: string(). Service path
#   $3: string(). PID file path.
# *Return* 0 if the process started, 0 otherwise or on error.
zap_srv() {
    srvname=${1:?'arg #1 missing: service name'}
    srvpath=${2:?'arg #2 missing: service name'}
    pidfile=${3:?'arg #3 missing: PID file path'}

    rpid=''
    status=0

    # N.B. pgrep would be handy but is unreliable for scripts using
    # '#!/usr/bin/env ...' shebangs.

    rpid=$(psgrep $srvpath) || {
        log_error "${srvname}: not running."
        rm -f $pidfile
        return 1
    }

    kill $rpid
    status=$?
    sleep ${sleep_time}
    # whatever happened...
    rm -f $pidfile

    log_info "${srvname}: zapped."

    return $status
}


cleanup() {
    trap - EXIT INT TERM

    [ $_status -eq $STATUS_OK ] ||
        log_debug "Something went wrong. See the service log (if any): ${srv_logfile-[none]}"

    exit $_status
}

# EXIT raised on Ctrl+D in an interactive shell
trap cleanup EXIT INT TERM


################################################################################
# Main
################################################################################
[ ${EUID:-$(id -u)} -eq 0 ] &&
    log_fatal 'This program is not supposed to run as user "root".'

# TO-DO: get also from configuration file?
system_root_d=${def_system_root_d}
sleep_time=${def_sleep_time}

# first ':' silences getotps
while getopts ':hmr:s:-:' OPT; do
  # support long options: https://stackoverflow.com/a/28466267/519360
  if [ "$OPT" = "-" ]; then   # long option: reformulate OPT and OPTARG
    OPT="${OPTARG%%=*}"       # extract long option name
    OPTARG="${OPTARG#"$OPT"}" # extract long option argument (may be empty)
    OPTARG="${OPTARG#=}"      # if long option argument, remove assigning `=`
  fi
  case "$OPT" in
      \?)
          # msg consistent with common tools (no leading -)
          say_try "${myself}: unrecognized option -- '${OPTARG}'."
          ;;
      :)
          # possibly useless if check_optarg does the job...
          say_try "${myself}: option requires an argument -- '${OPTARG}'."
          ;;
      h | help)
          say_help
          ;;
      m | man)
          manual
          ;;
      r | system-root)
          check_optarg ${OPT} "$opt_re_path" "$OPTARG"
          system_root_d="${OPTARG:-${system_root_d}}"
          ;;
      s | sleep-time)
          check_optarg ${OPT} "$opt_re_digit" "$OPTARG"
          sleep_time="${OPTARG:-${sleep_time}}"
          ;;
      *)
          # bad long option
          # msg consistent with common tools (two leading -)
          say_try "${myself}: unrecognized option -- --${OPT}"
          ;;
  esac
done
shift $((OPTIND-1))

# command's non-opt arguments
service=${1-}
[ -n "${service}" ] || say_try "Arg #1 missing: service name."

action=${2-}
[ -n "${action}" ] || say_try "Arg #2 missing: action."

shift 2
[ -n "$*" ] && say_try "Extra arguments found: '$*'"

# sanity check
[ -d "${system_root_d}" ] ||
    say_try "system root is not a directory: ${system_root_d}."

[ "${system_root_d}" = '/' ] &&
    say_try "stubbornly refusing to use the real system root."


system_etc_d=${system_root_d}/${def_system_etc_sd}
system_run_d=${system_root_d}/${def_system_run_sd}
system_log_d=${system_root_d}/${def_system_log_sd}

mkdir -p $system_log_d $system_run_d

srv_logfile=${system_log_d}/${service}.log
srv_pidfile=${system_run_d}/${service}.pid

get_srv_specs $service $system_etc_d ||
    log_fatal "${service}: can't get service's specs."

# Sanity checks
[ "$srv_background" ] && log_bug "Spec 'background'"
[ "$srv_reload" ] && log_bug "Spec 'reload'"
[ "$srv_restart" ] && log_bug "Spec 'restart'"
[ "$srv_stop" ] && log_bug "Spec 'stop'"

# now global vars 'srv_*' should be set
# we're looking for a command
srv_path=$(realpath -s "$srv_path")

srv_pid=''
case $action in
    status)
        srv_pid=$(is_running $service $srv_pidfile)
        case $? in
            $SRV_STARTED)
                log_info "${service}: started (PID: ${srv_pid})."
                ;;
            $SRV_STOPPED)
                log_info "${service}: stopped."
                ;;
            $SRV_CRASHED)
                log_info "${service}: crashed."
                ;;
            *)
                log_bug "Service status: $?"
        esac
        ;;
    start)
        start_srv $service $srv_path $srv_pidfile $srv_logfile $srv_start $srv_cdhome || {
            log_error "${service}: ${action} failed."
            _status=$STATUS_ERR
        }
        ;;
    stop)
        stop_srv $service $srv_pidfile || {
            log_error "${service}: ${action} failed."
            _status=$STATUS_ERR
        }
        ;;
    restart)
        stop_srv $service $srv_pidfile ||
            log_warn "${service}: stop failed."
        sleep ${sleep_time}
        start_srv $service $srv_path $srv_pidfile $srv_logfile $srv_start $srv_cdhome || {
            log_error "${service}: start failed."
            _status=$STATUS_ERR
        }
        ;;
    zap)
        zap_srv $service $srv_path $srv_pidfile || {
            log_error "${service}: ${action} failed."
            _status=$STATUS_ERR
        }
        ;;
    *)
        log_error "${action}: unsupported action."
        _status=$STATUS_ERR
esac

exit $_status
