#!/usr/bin/env bats
# -*- mode: bats -*-
################################################################################
# Copyright 2024 Marco Emilio Poleggi
#
# This file is part of bash-utilz.
#
# bash-utilz is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License Foundation version 3 as
# published by the Free Software Foundation.
#
# bash-utilz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with bash-utilz.  If not, see <http://www.gnu.org/licenses/>.
################################################################################
# aliases are problematic <https://github.com/bats-core/bats-core/issues/259>
# shopt -s expand_aliases
set -u

service_cli='start-stop'

# aux functions
function assert_p_bug() {
    assert_output --partial "[error] [BUG] ${*}"
}

function assert_p_error() {
    assert_output --partial "[error] ${*}"
}

function assert_p_error() {
    assert_output --partial "[error] ${*}"
}

function assert_p_info() {
    assert_output --partial "[info] ${*}"
}
function assert_p_warn() {
    assert_output --partial "[warning] ${*}"
}


_srv_pid() {
    srv_output=${1:?'arg #1 missing: service output'}

    pid=$(echo "$srv_output" |
              sed -nr 's/^(.+?)PID:[[:blank:]]+([[:digit:]]+)(.+?)$/\2/p')
    # echo "# PID: ${pid}" >&3
    echo $pid
}

################################################################################

setup_file() {
    srv_root_d=$(mktemp -qd)
    srv_etc_d=${srv_root_d}/etc/${service_cli}
    srv_log_d=${srv_root_d}/var/log/${service_cli}
    srv_run_d=${srv_root_d}/var/run/${service_cli}
    srv_path_symlnk=${srv_root_d}/quuxbaz

    for d in $srv_etc_d $srv_log_d $srv_run_d; do
        mkdir -p $d
    done

    dummy_srv_bin=$(mktemp -q --tmpdir=$srv_root_d)
    output_log=$(mktemp -q --tmpdir=$srv_root_d)

    cat test/dummy_service > $dummy_srv_bin

    chmod +x $dummy_srv_bin

    # Watch out! Race conditions ahead.
    # This should be lower than STARTSTOP_SLEEP_TIME
    DUMMYSERVICE_SLEEP_TIME=1
    # This should be higher than DUMMYSERVICE_SLEEP_TIME
    STARTSTOP_SLEEP_TIME=1
    STARTSTOP_SYSTEM_ROOT=$srv_root_d

    export STARTSTOP_SYSTEM_ROOT STARTSTOP_SLEEP_TIME DUMMYSERVICE_SLEEP_TIME STARTSTOP_DEBUG=1 srv_root_d srv_etc_d srv_log_d srv_run_d dummy_srv_bin output_log srv_path_symlnk

    echo "*** Warning! If tests fail randomly, try lowering DUMMYSERVICE_SLEEP_TIME (${DUMMYSERVICE_SLEEP_TIME}) and/or increasing STARTSTOP_SLEEP_TIME (${STARTSTOP_SLEEP_TIME}) ***" >&3
}

setup() {
    bats_require_minimum_version 1.5.0
    load '/usr/share/bats-support/load'
    load '/usr/share/bats-assert/load'
}

teardown() {
    pkill -f ${dummy_srv_bin} || : # ignore failures
    pkill -f ${srv_path_symlnk} || : # ignore failures
    # avoid race conditions
    sleep 2
}

teardown_file() {
    [[ -d "$srv_root_d" && $srv_root_d != '/' ]] || {
        # you never know...
        echo "# *** Your 'srv_root_d' (='$srv_root_d') is screwed! Teardwon skipped ***" >&3
        skip
    }

    rm -rf $srv_root_d
}

################################################################################

# bats test_tags=args,none
@test 'args -- none' {
    run ! ./${service_cli}
    assert_p_error "Arg #1 missing: service name."
    assert_output --partial "Try '${service_cli} --help'"
}

# bats test_tags=args,arg2,missing
@test 'args -- arg2, missing' {
    run ! ./${service_cli} foobar
    assert_p_error "Arg #2 missing: action."
    assert_output --partial "Try '${service_cli} --help'"
}

# bats test_tags=args,opt,short,invalid
@test 'args -- opt, short, invalid' {
    run ! ./${service_cli} -X
    assert_p_error "${service_cli}: unrecognized option -- 'X'"
    assert_output --partial "Try '${service_cli} --help'"
}

# bats test_tags=args,opt,long,invalid
@test 'args -- opt, long, invalid' {
    run ! ./${service_cli} --Xinvalid
    assert_p_error "${service_cli}: unrecognized option -- --Xinvalid"
    assert_output --partial "Try '${service_cli} --help'"
}

# bats test_tags=args,opt,h
@test 'args -- opt, h' {
    run ./${service_cli} -h whatever happens...
    assert_output --partial "Usage: ${service_cli}"

    run ./${service_cli} --help whatever happens...
    assert_output --partial "Usage: ${service_cli}"
}

# bats test_tags=args,opt,m
@test 'args -- opt, m' {
    run ./${service_cli} -m whatever happens...
    # quite fuzzy
    assert_output --partial "${service_cli}(1)"

    run ./${service_cli} --man whatever happens...
    assert_output --partial "${service_cli}(1)"
}

# bats test_tags=args,opt,all,unset
@test 'args -- opt, all, unset' {
    declare -A opts=(
        [r]='system-root'
        [s]='sleep-time'
    )

    for opt in ${!opts[*]}; do
        run ! ./${service_cli} -${opt}
        # as per getopts error handling
        assert_p_error "${service_cli}: option requires an argument -- '${opt}'"
        assert_output --partial "Try '${service_cli} --help'"

        lopt=${opts[${opt}]}
        run ! ./${service_cli} --${lopt}
        assert_p_error "${service_cli}: option '--${lopt}' requires an argument"
        assert_output --partial "Try '${service_cli} --help'"
    done
}

# bats test_tags=args,opt,s,invalid
@test 'args -- opt, s, invalid' {
    run ! ./${service_cli} -s whatever foo bar
    assert_p_error "${service_cli}: option '-s' must be an integer"

    run ! ./${service_cli} --sleep-time=whatever foo bar
    assert_p_error "${service_cli}: option '--sleep-time' must be an integer"
}

# bats test_tags=args,opt,r,valid
@test 'args -- opt, s, valid' {
    run ! ./${service_cli} -r /tmp foo bar
    assert_p_error "foo: can't get service's specs."

    run ! ./${service_cli} --system-root=/tmp foo bar
    assert_p_error "foo: can't get service's specs."
}

# bats test_tags=args,opt,r,invalid
@test 'args -- opt, r, invalid' {
    # opt should be checked before args -- POSIX path
    run ! ./${service_cli} -r /wha%teve/+123 foo bar
    assert_p_error "${service_cli}: option '-r' must be a POSIX file path"

    run ! ./${service_cli} --system-root=/wha%teve/+123 foo bar
    assert_p_error "${service_cli}: option '--system-root' must be a POSIX file path"
}

# bats test_tags=args,opt,r,nonexistent
@test 'args -- opt, r, nonexistent' {
    # opt should be checked before args
    run ! ./${service_cli} -r whatever foo bar
    assert_p_error "system root is not a directory: whatever"

    run ! ./${service_cli} --system-root=whatever foo bar
    assert_p_error "system root is not a directory: whatever"
}

# bats test_tags=args,opt,r,valid
@test 'args -- opt, r, valid' {
    # opt should be checked before args
    run ! ./${service_cli} -r /tmp foo bar
    assert_p_error "foo: can't get service's specs."

    run ! ./${service_cli} --system-root=/tmp foo bar
    assert_p_error "foo: can't get service's specs."
}

# bats test_tags=args,opt,r,dangerous
@test 'args -- opt, r, dangerous' {
    # opt should be checked before args
    run ! ./${service_cli} -r / foo bar
    assert_p_error "stubbornly refusing to use the real system root"

    run ! ./${service_cli} --system-root=/ foo bar
    assert_p_error "stubbornly refusing to use the real system root"
}

# bats test_tags=status,specs,nonexistent
@test 'status -- specs,nonexistent' {
    service=nonexistent

    run ! ./${service_cli} $service status
    assert_p_error "${service}: can't get service's specs."
}

# bats test_tags=status,specs,niy
@test 'status -- specs, niy' {
    service=foobar

    for spec in stop reload restart background; do
        cat > ${srv_etc_d}/${service} <<EOF
name=${service}
path=${dummy_srv_bin}
${spec}=whatever
EOF
        run ! ./${service_cli} $service status
        assert_p_bug  "Spec '${spec}': feature not implemented yet, sorry."
    done
}

# bats test_tags=status,specs,missing
@test 'status -- specs, missing' {
    service=foobar

    :> ${srv_etc_d}/${service}
    # cat > ${srv_etc_d}/${service} <<EOF
# name=${service}
# path=${dummy_srv_bin}
    # EOF

    run ! ./${service_cli} $service status

    assert_p_error "Missing mandatory specs: name path"
}

# bats test_tags=status,specs,invalid
@test 'status -- specs, invalid' {
    service=foobar

    cat > ${srv_etc_d}/${service} <<EOF
foo=bar
baz=quux
whatever
EOF

    run ! ./${service_cli} $service status
    assert_p_error  "foo: invalid spec name."
    assert_p_error  "baz: invalid spec name."
    assert_p_error  "whatever: invalid spec name."
    assert_p_error  "${service}: can't get service's specs."
}

# bats test_tags=status,specs,missing
@test 'status -- specs missing' {
    service=foobar

    cat > ${srv_etc_d}/${service} <<EOF
foo=bar
baz=quux
whatever
EOF

    run ! ./${service_cli} $service status
    assert_p_error  "foo: invalid spec name."
    assert_p_error  "baz: invalid spec name."
    assert_p_error  "whatever: invalid spec name."
    assert_p_error  "${service}: can't get service's specs."
}

# bats test_tags=action,invalid
@test 'action -- invalid' {
    service=foobar

    cat > ${srv_etc_d}/${service} <<EOF
# path must NOT be quoted
name=${service}
# the real executable
path=${dummy_srv_bin}
EOF

    run ! ./${service_cli} $service whatever
    assert_p_error  "whatever: unsupported action."
}

# bats test_tags=status,stopped
@test 'status -- stopped' {
    service=foobar

    cat > ${srv_etc_d}/${service} <<EOF
# path must NOT be quoted
name=${service}
# the real executable
path=${dummy_srv_bin}
EOF

    run ./${service_cli} $service status
    assert_p_info "${service}: stopped."

    ! [ -f "${srv_run_d}/${service}.pid" ]
}

# bats test_tags=status,crashed
@test 'status -- crashed' {
    service=foobar

    cat > ${srv_etc_d}/${service} <<EOF
# path must NOT be quoted
name=${service}
# the real executable
path=${dummy_srv_bin}
EOF

    # simulate crash with PID file left over
    touch ${srv_run_d}/${service}.pid

    run ./${service_cli} $service status
    assert_p_info "${service}: crashed."

    [ -f "${srv_run_d}/${service}.pid" ]
}

# bats test_tags=stop,stopped
@test 'stop -- stopped' {
    service=foobar

    cat > ${srv_etc_d}/${service} <<EOF
# path must NOT be quoted
name=${service}
# the real executable
path=${dummy_srv_bin}
EOF

    run ! ./${service_cli} $service stop
    assert_p_error  "${service}: not running."
    assert_p_error  "${service}: stop failed."

    ! [ -f "${srv_run_d}/${service}.pid" ]
}

# bats test_tags=zap,stopped
@test 'zap -- stopped' {
    service=foobar

    cat > ${srv_etc_d}/${service} <<EOF
# path must NOT be quoted
name=${service}
# the real executable
path=${dummy_srv_bin}
EOF

    # never started => no process
    run ! ./${service_cli} $service zap
    assert_p_error  "${service}: zap failed."

    ! [ -f "${srv_run_d}/${service}.pid" ]
}

# bats test_tags=start,stopped
@test 'start -- stopped' {
    service=foobar

    cat > ${srv_etc_d}/${service} <<EOF
# simlink to "foobar". path must NOT be quoted
name=${service}
# the real executable
path=${dummy_srv_bin}
start='start'
EOF

    # can't use 'run' here, nor capture via '$()' because the backgrounded
    # process would hang the test
    ./${service_cli} $service start 3>&- &>${output_log}

    output=$(cat $output_log)
    pid=$(_srv_pid "$output")

    assert_p_info  "${service}: started. PID: ${pid}"
    assert_p_info  "${service}: logging to '${srv_log_d}/${service}.log'"

    [ -f "${srv_run_d}/${service}.pid" ]
    [ $pid == $(cat "${srv_run_d}/${service}.pid") ]

    run ./${service_cli} $service status

    assert_p_info "${service}: started (PID"

    [ -f "${srv_run_d}/${service}.pid" ]
    [ $pid == $(cat "${srv_run_d}/${service}.pid") ]
}

# bats test_tags=start,stopped,cdhome
@test 'start -- stopped, cdhome' {
    service=foobar

    cat > ${srv_etc_d}/${service} <<EOF
# simlink to "foobar". path must NOT be quoted
name=${service}
# the real executable
path=${dummy_srv_bin}
start='start'
cdhome=whatever
EOF

    # can't use 'run' here, nor capture via '$()' because the backgrounded
    # process would hang the test
    ./${service_cli} $service start 3>&- &>${output_log}

    output=$(cat $output_log)
    pid=$(_srv_pid "$output")

    assert_p_info  "${service}: started. PID: ${pid}"
    assert_p_info  "${service}: logging to '${srv_log_d}/${service}.log'"

    [ -f "${srv_run_d}/${service}.pid" ]
    [ $pid == $(cat "${srv_run_d}/${service}.pid") ]

    run ./${service_cli} $service status

    assert_p_info "${service}: started (PID"

    [ -f "${srv_run_d}/${service}.pid" ]
    [ $pid == $(cat "${srv_run_d}/${service}.pid") ]
}

# bats test_tags=start,started
@test 'start -- started' {
    service=foobar

    cat > ${srv_etc_d}/${service} <<EOF
# simlink to "foobar". path must NOT be quoted
name=${service}
# the real executable
path=${dummy_srv_bin}
start='start'
EOF

    ./${service_cli} $service start 3>&- &>${output_log}

    output=$(cat $output_log)
    pid=$(_srv_pid "$output")

    # echo "Service output:" >&3
    # cat ${srv_log_d}/${service}.log >&3

    assert_p_info  "${service}: started. PID: ${pid}"
    assert_p_info  "${service}: logging to '${srv_log_d}/${service}.log'"

    run ! ./${service_cli} $service start

    assert_p_error  "${service}: already started (PID=${pid})"
    assert_p_error  "${service}: start failed"

    [ -f "${srv_run_d}/${service}.pid" ]
    [ $pid == $(cat "${srv_run_d}/${service}.pid") ]
}

# bats test_tags=start,symlink
@test 'start -- symlink' {
    service=foobar

    cat > ${srv_etc_d}/${service} <<EOF
# simlink to "foobar". path must NOT be quoted
name=${service}
# the real executable; it can be a symlink
path=${srv_path_symlnk}
start='start'
EOF

    ln -sf ${dummy_srv_bin} ${srv_path_symlnk}

    ./${service_cli} $service start 3>&- &>${output_log}

    output=$(cat $output_log)
    pid=$(_srv_pid "$output")

    assert_p_info  "${service}: started. PID: ${pid}"
    assert_p_info  "${service}: logging to '${srv_log_d}/${service}.log'"

    [ -f "${srv_run_d}/${service}.pid" ]
    [ $pid == $(cat "${srv_run_d}/${service}.pid") ]
}

# bats test_tags=stopped,restart
@test 'start -- stopped, restart' {
    service=foobar

    cat > ${srv_etc_d}/${service} <<EOF
# simlink to "foobar". path must NOT be quoted
name=${service}
# the real executable
path=${dummy_srv_bin}
start='start'
EOF

    ./${service_cli} $service restart 3>&- &>${output_log}

    output=$(cat $output_log)
    pid=$(_srv_pid "$output")

    assert_p_error  "${service}: not running."
    assert_p_warn  "${service}: stop failed."
    assert_p_info  "${service}: started. PID: ${pid}"
    assert_p_info  "${service}: logging to '${srv_log_d}/${service}.log'"

    [ -f "${srv_run_d}/${service}.pid" ]
    [ $pid == $(cat "${srv_run_d}/${service}.pid") ]
}

# bats test_tags=start,zap
@test 'start -- start, zap' {
    service=foobar

    cat > ${srv_etc_d}/${service} <<EOF
# simlink to "foobar". path must NOT be quoted
name=${service}
# the real executable
path=${dummy_srv_bin}
start='start'
EOF

    ./${service_cli} $service start 3>&- &>${output_log}

    output=$(cat $output_log)

    pid=$(_srv_pid "$output")

    assert_p_info  "${service}: started. PID: ${pid}"
    assert_p_info  "${service}: logging to '${srv_log_d}/${service}.log'"

    run ./${service_cli} $service zap

    assert_p_info  "${service}: zapped"

    ! [ -f "${srv_run_d}/${service}.pid" ]
    ! pgrep -f ${dummy_srv_bin}
}

# bats test_tags=start,crash,stop
@test 'start -- start, crash, stop' {
    service=foobar

    cat > ${srv_etc_d}/${service} <<EOF
# simlink to "foobar". path must NOT be quoted
name=${service}
# the real executable
path=${dummy_srv_bin}
start='start'
EOF

    ./${service_cli} $service start 3>&- &>${output_log}

    output=$(cat $output_log)
    pid=$(_srv_pid "$output")

    assert_p_info  "${service}: started. PID: ${pid}"
    assert_p_info  "${service}: logging to '${srv_log_d}/${service}.log'"

    # crash it
    pkill -f ${dummy_srv_bin}
    sleep $STARTSTOP_SLEEP_TIME

    run ! ./${service_cli} $service stop

    assert_p_error  "${service}: not running"
    assert_p_error  "${service}: stop failed"

    ! [ -f "${srv_run_d}/${service}.pid" ]
}

# bats test_tags=start,crash,zap
@test 'start -- start, crash, zap' {
    service=foobar

    cat > ${srv_etc_d}/${service} <<EOF
# simlink to "foobar". path must NOT be quoted
name=${service}
# the real executable
path=${dummy_srv_bin}
start='start'
EOF

    ./${service_cli} $service start 3>&- &>${output_log}

    output=$(cat $output_log)
    pid=$(_srv_pid "$output")

    assert_p_info  "${service}: started. PID: ${pid}"
    assert_p_info  "${service}: logging to '${srv_log_d}/${service}.log'"

    # crash it
    pkill -f ${dummy_srv_bin}
    sleep $STARTSTOP_SLEEP_TIME

    run ! ./${service_cli} $service zap

    assert_p_error  "${service}: not running"
    assert_p_error  "${service}: zap failed"

    ! [ -f "${srv_run_d}/${service}.pid" ]
}

# bats test_tags=start,lost,stop
@test 'start -- start, lost, stop' {
    service=foobar

    cat > ${srv_etc_d}/${service} <<EOF
# simlink to "foobar". path must NOT be quoted
name=${service}
# the real executable
path=${dummy_srv_bin}
start='start'
EOF

    ./${service_cli} $service start 3>&- &>${output_log}

    output=$(cat $output_log)

    pid=$(_srv_pid "$output")

    assert_p_info  "${service}: started. PID: ${pid}"
    assert_p_info  "${service}: logging to '${srv_log_d}/${service}.log'"

    # simulate lost tracking
    rm "${srv_run_d}/${service}.pid"

    run ./${service_cli} $service stop

    assert_p_error  "${service}: stop failed"

    ! [ -f "${srv_run_d}/${service}.pid" ]

    # It should be still running 'cause stop just checks the PID file (test
    # somewhat unreliable...)
    pgrep -f ${dummy_srv_bin}
}

# bats test_tags=start,lost,zap
@test 'start -- start, lost, zap' {
    service=foobar

    cat > ${srv_etc_d}/${service} <<EOF
# simlink to "foobar". path must NOT be quoted
name=${service}
# the real executable
path=${dummy_srv_bin}
start='start'
EOF

    ./${service_cli} $service start 3>&- &>${output_log}

    output=$(cat $output_log)

    pid=$(_srv_pid "$output")

    assert_p_info  "${service}: started. PID: ${pid}"
    assert_p_info  "${service}: logging to '${srv_log_d}/${service}.log'"

    # simulate lost tracking
    rm "${srv_run_d}/${service}.pid"

    run ./${service_cli} $service zap

    assert_p_info  "${service}: zapped"

    ! [ -f "${srv_run_d}/${service}.pid" ]
    ! pgrep -f ${dummy_srv_bin}
}

# TO-DO
# * test with no/custom 'start' action
# * test with custom 'stop' action
